package com.orbo.isu_customer.activity.home;

public class HomeContract {
    public interface View {
        void fetchedHomeDashboardResponse(boolean status, String response);
    }

    public interface UserInteraction{
        void getHomeDashboardResponse(String base_url);
    }
}
