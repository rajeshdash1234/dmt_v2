package com.orbo.isu_customer.activity.login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.isu.dmt.R;
import com.orbo.isu_customer.activity.home.ISUHomeActivity;
import com.orbo.isu_customer.activity.SessionManager;
import com.orbo.isu_customer.utility.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements LoginContract.View {

    EditText user_name, password;
    LoginPresenter loginPresenter;
    Button submit;
    SessionManager session;
    String _user_name,_password;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        user_name = findViewById(R.id.user_name);
        password = findViewById(R.id.password);
        submit = findViewById(R.id.submit);
        progressBar = findViewById(R.id.progressBar);

        loginPresenter = new LoginPresenter(this);

        // Session Manager
        session = new SessionManager(getApplicationContext());

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                _user_name = user_name.getText().toString();
                _password = password.getText().toString();

                //loginPresenter.getV1Response("https://itpl.iserveu.tech");

                progressBar.setVisibility(View.VISIBLE);

                loadLogin("http://34.93.39.198:8080/getlogintoken.json");
            }
        });
    }

    private void loadV1() {
        String tag_json_obj = "json_obj_req";
        String url = "https://itpl.iserveu.tech/generate/v1";

//        final ProgressDialog pDialog = new ProgressDialog(this);
//        pDialog.setMessage("Loading...");
//        pDialog.show();

        StringRequest jsonObjRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(response);
                        } catch (Throwable t) {
                            t.printStackTrace();
                        }
                        try {
                            String _hello = obj.getString("hello");

                            // Receiving side
                            byte[] data = Base64.decode(_hello, Base64.DEFAULT);
                            String base64 = new String(data, "UTF-8");

                            Log.i("base64 :::::: ", base64);

                            Toast.makeText(LoginActivity.this, base64, Toast.LENGTH_SHORT).show();

                            loadLogin(base64);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("volley", "Error: " + error.getMessage());
                error.printStackTrace();
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonObjRequest, tag_json_obj);
    }

    private void loadLogin(String base64) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("username", _user_name);
        params.put("password", _password);

        RequestQueue mQueue = Volley.newRequestQueue(getApplicationContext());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(base64, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.i("response :: ",""+response);
                            String _token = response.getString("token");
                            String _admin = response.getString("adminName");

                            // Use user real data
                            session.createLoginSession(_token, _admin);

                            progressBar.setVisibility(View.GONE);

                            Intent i = new Intent(getApplicationContext(), ISUHomeActivity.class);
                            startActivity(i);
                            finish();

                        } catch (Exception e) {
                            e.printStackTrace();
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("TAG", error.getMessage(), error);
            }
        }) { //no semicolon or coma
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };

        mQueue.add(jsonObjectRequest);

    }

    @Override
    public void fetchedV1Response(boolean status, String response) {
        if(response != null){
            loadLogin(response);
        }

    }
}
