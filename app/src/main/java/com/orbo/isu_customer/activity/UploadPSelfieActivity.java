package com.orbo.isu_customer.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;


import com.isu.dmt.R;
import com.orbo.isu_customer.utility.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static android.content.Intent.FLAG_GRANT_READ_URI_PERMISSION;
import static android.content.Intent.FLAG_GRANT_WRITE_URI_PERMISSION;

public class UploadPSelfieActivity extends AppCompatActivity {

    ImageView pancard_image;
    Button pan_upload;
    EditText name,dob;

    protected static final int CAMERA_REQUEST = 100;
    protected static final int GALLERY_PICTURE = 101;
    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 123;

    File captureMediaFile;
    byte[] bytesDocumentsTypePicture = null;

   // private StorageReference mStorageRef;

    Uri final_imagePath;

    String TAG = "UploadPSelfieActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_upload_pselfie);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("KYC Verification");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        pancard_image = (ImageView) findViewById(R.id.pancard_image);
        pan_upload = (Button) findViewById(R.id.pan_upload);

        requestStoragePermission();


        pancard_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                load();
            }
        });

        name =  findViewById(R.id.name);
        dob = findViewById(R.id.dob);

      //  mStorageRef = FirebaseStorage.getInstance().getReference();

        pan_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitImage(final_imagePath);
            }
        });
    }

    private void load() {
        AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(UploadPSelfieActivity.this);
        myAlertDialog.setTitle("Upload PAN Card");
        myAlertDialog.setMessage("How do you want to set your picture?");

        myAlertDialog.setPositiveButton("Gallery", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, GALLERY_PICTURE);
            }
        });

        myAlertDialog.setNegativeButton("Camera", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface arg0, int arg1) {

                captureMediaFile = getOutputMediaFile(getApplicationContext());

                if (captureMediaFile != null) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    //intent.putExtra("android.intent.extras.CAMERA_FACING", 1);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                        intent.putExtra("android.intent.extras.LENS_FACING_FRONT", 1);
                    } else {
                        intent.putExtra("android.intent.extras.CAMERA_FACING", 1);
                    }

                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                        intent.addFlags(FLAG_GRANT_READ_URI_PERMISSION | FLAG_GRANT_WRITE_URI_PERMISSION);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, captureMediaFile);
                    } else {
                        Uri photoUri = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".provider", captureMediaFile);
                        intent.addFlags(FLAG_GRANT_READ_URI_PERMISSION);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                    }

                    if (intent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(intent, CAMERA_REQUEST);
                    } else {
                        // new AlertDialogMessage(getApplicationContext()).showAToast(R.string.camera_unavailable);
                    }
                } else {
                    // new AlertDialogMessage(getApplicationContext()).showAToast(R.string.file_save_error);
                }
            }
        });

        myAlertDialog.show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == CAMERA_REQUEST) {
            Bitmap bitmap;
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                if (data != null) {
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
                        pancard_image.setImageBitmap(bitmap);
                        bytesDocumentsTypePicture = getBytesFromBitmap(bitmap);
                        // submitImage(data.getData());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), " some_error_while_uploading  ", Toast.LENGTH_SHORT).show();
                }
            } else {
                bitmap = BitmapFactory.decodeFile(captureMediaFile.getAbsolutePath());
                bytesDocumentsTypePicture = getBytesFromBitmap(bitmap);

                final_imagePath = Uri.parse(captureMediaFile.getAbsolutePath());

                //submitImage(final_imagePath);
            }

        } else if (resultCode == RESULT_OK && requestCode == GALLERY_PICTURE) {
            if (data != null) {
                Uri selectedImage = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
                    pancard_image.setImageBitmap(bitmap);
                    bytesDocumentsTypePicture = getBytesFromBitmap(bitmap);

                    Log.i("selectedImage :::: ", "" + selectedImage);

                    Uri file = selectedImage;

                    // Uri.fromFile(new File(selectedImage));

                    final_imagePath = file;
                    // submitImage(final_imagePath);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(), " some_error_while_uploading  ", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), " some_error_while_uploading  ", Toast.LENGTH_SHORT).show();
        }
    }

    private void submitImage(Uri final_imagePath) {
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();

        String date = new SimpleDateFormat("yyyy_MM_dd", Locale.getDefault()).format(new Date());

    /*    final StorageReference riversRef = mStorageRef.child("aadhar_" + date);

        riversRef.putFile(final_imagePath)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Task<Uri> urlTask = taskSnapshot.getStorage().getDownloadUrl();
                        while (!urlTask.isSuccessful()) ;
                        Uri downloadUrl = urlTask.getResult();

                        Log.i("taskSnapshot ::::: ", "" + downloadUrl);

                        pDialog.dismiss();

                        submitPanCard(downloadUrl.toString());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        exception.printStackTrace();
                    }
                });*/
     }

    private void submitPanCard(final String pic) {
        String tag_json_obj = "json_obj_req";
        String url = "http://34.67.102.42:8083/compare";

        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();

        StringRequest jsonObjRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(response);
                        } catch (Throwable t) {
                        }
                        try {
                            String status = obj.getString("status");
                            String message = obj.getString("message");

                            if (status.equals("1")) {
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                                finish();
                            } else {
                                Snackbar snackbar = Snackbar.make(pancard_image, message, Snackbar.LENGTH_LONG);
                                snackbar.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("volley", "Error: " + error.getMessage());
                error.printStackTrace();
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("aadhar_uri","https://firebasestorage.googleapis.com/v0/b/isu-customerapp.appspot.com/o/aadhar_2019_11_06?alt=media&token=43370991-19fe-4e57-898d-4f57d4dd702c");
                params.put("target", pic);
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonObjRequest, tag_json_obj);
    }

    public byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 25, stream);
        return stream.toByteArray();
    }

    public File getOutputMediaFile(Context context) {

        File mediaStorageDir;

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            mediaStorageDir = new File(String.valueOf(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)));
        } else {
            mediaStorageDir = new File(String.valueOf(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)));
        }

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                //Log.d(“GKMIT”, "Oops! Failed create " + “GKMIT”+ " directory");
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
        return mediaFile;
    }

    //Requesting permission
    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
            return;
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);
    }


    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //Checking the request code of our request
        if (requestCode == MY_PERMISSIONS_REQUEST_CAMERA) {
            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
                Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
    }
}
