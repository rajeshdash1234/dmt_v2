package com.orbo.isu_customer.activity.insurance;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;

import com.isu.dmt.R;

public class InsuranceWabviewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insurance_wabview);


        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Webview");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        String url = getIntent().getStringExtra("url");

        WebView webView = findViewById(R.id.webview);
        webView.loadUrl(url);

        webView.getSettings().setJavaScriptEnabled(true);

    }
}
