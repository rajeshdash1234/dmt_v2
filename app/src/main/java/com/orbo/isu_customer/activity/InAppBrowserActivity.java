package com.orbo.isu_customer.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.webkit.WebView;

import com.isu.dmt.R;

public class InAppBrowserActivity extends AppCompatActivity {
    WebView webv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_in_app_browser);
        webv = findViewById(R.id.webv);
    }
}
