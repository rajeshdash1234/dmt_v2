package com.orbo.isu_customer.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.isu.dmt.R;
import com.orbo.isu_customer.activity.login.LoginActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class LanguageListAdapter extends RecyclerView.Adapter<LanguageListAdapter.LanguageViewHolder> {

    public ArrayList<HashMap<String, String>> mapslanguage;
    Context activity;
    SharedPreferences sharedpreferences;


    public class LanguageViewHolder extends RecyclerView.ViewHolder {
        public ImageView select_btn;
        public TextView language_name;

        public LanguageViewHolder(View view) {
            super(view);
            language_name = view.findViewById(R.id.language_name);
            select_btn = view.findViewById(R.id.select_btn);

            sharedpreferences = activity.getSharedPreferences("language", Context.MODE_PRIVATE);

        }
    }

    public LanguageListAdapter(Context activity, ArrayList<HashMap<String, String>> mapslanguage) {
        this.mapslanguage = mapslanguage;
        this.activity = activity;
    }

    @Override
    public LanguageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_lang, parent, false);
        return new LanguageViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final LanguageViewHolder holder, final int position) {

        sharedpreferences = activity.getSharedPreferences("language", Context.MODE_PRIVATE);
        String lang  = sharedpreferences.getString("lang","en");

        if(lang.equals(mapslanguage.get(position).get("key"))){
            holder.select_btn.setImageDrawable(activity.getDrawable(R.drawable.checkmark));
        }

        holder.language_name.setText(mapslanguage.get(position).get("name"));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //

                String lang = mapslanguage.get(position).get("key");

                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString("lang", lang);
                editor.apply();

                Locale locale = new Locale(lang);
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                activity.getResources().updateConfiguration(config, activity.getResources().getDisplayMetrics());

                // ((Activity) activity).finish();

                Intent i = new Intent(activity, LoginActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                activity.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mapslanguage.size();
    }
}
