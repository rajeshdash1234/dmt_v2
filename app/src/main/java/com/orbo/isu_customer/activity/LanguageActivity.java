package com.orbo.isu_customer.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import com.isu.dmt.R;

import java.util.ArrayList;
import java.util.HashMap;

public class LanguageActivity extends AppCompatActivity {

    private RecyclerView languageRecyclerView;
    private LanguageListAdapter languageAdapter;

    ArrayList<HashMap<String, String>> mapslanguage = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.languahe));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        HashMap<String, String> lang_eng = new HashMap<>();
        lang_eng.put("name", "English");
        lang_eng.put("key", "en");

        HashMap<String, String> lang_hin = new HashMap<>();
        lang_hin.put("name", "Hindi");
        lang_hin.put("key", "hi");

        HashMap<String, String> lang__odia = new HashMap<>();
        lang__odia.put("name", "Odia");
        lang__odia.put("key", "or");

        mapslanguage.add(lang_eng);
        mapslanguage.add(lang_hin);
        mapslanguage.add(lang__odia);

        languageRecyclerView = findViewById(R.id.language);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        languageRecyclerView.setLayoutManager(mLayoutManager);
        languageRecyclerView.setItemAnimator(new DefaultItemAnimator());

        languageAdapter = new LanguageListAdapter(LanguageActivity.this, mapslanguage);
        languageRecyclerView.setAdapter(languageAdapter);

    }
}
