package com.orbo.isu_customer.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.isu.dmt.R;
import com.orbo.isu_customer.activity.retrofitService.DMTRetrofitInstance;
import com.orbo.isu_customer.activity.retrofitService.GetDataService;
import com.orbo.isu_customer.utility.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class ProfileActivity extends AppCompatActivity {

    TextView user_name, user_email, user_mobile, balance, add_money, kyc, logout, addbene;
    SessionManager session;
    String _token, _admin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_profile);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.profile));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        user_name = findViewById(R.id.user_name);
        user_email = findViewById(R.id.user_email);
        user_mobile = findViewById(R.id.user_mobile);
        balance = findViewById(R.id.balance);
        add_money = findViewById(R.id.add_money);
        logout = findViewById(R.id.logout);
        addbene = findViewById(R.id.addbene);

        kyc = findViewById(R.id.kyc);

        session = new SessionManager(getApplicationContext());
        HashMap<String, String> user = session.getUserSession();
        String _userName = user.get(session.userName);
        String _userType = user.get(session.userType);
        String _userBalance = user.get(session.userBalance);
        String _adminName = user.get(session.adminName);
        String _mposNumber = user.get(session.mposNumber);


        HashMap<String, String> _user = session.getUserDetails();
        _token = _user.get(SessionManager.KEY_TOKEN);
        _admin = _user.get(SessionManager.KEY_ADMIN);

//        Log.i("_userName",_userName);
//        Log.i("_userType",_userType);
//        Log.i("_userBalance","_userBalance");
//        Log.i("_adminName","_adminName");
//        Log.i("_mposNumber","_mposNumber");

        user_name.setText(_userName);
        balance.setText(_userBalance);


        addbene.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadWallet("https://itpl.iserveu.tech/generate/v2");
            }
        });

        kyc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), PersonalDetailsActivity.class));
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                session.logoutUser();
            }
        });

    }

    private void loadWallet(String load_wallet) {
        String tag_json_obj = "json_obj_req";

        StringRequest jsonObjRequest = new StringRequest(Request.Method.GET, load_wallet,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(response);
                        } catch (Throwable t) {
                            t.printStackTrace();
                        }
                        try {
                            String _hello = obj.getString("hello");

                            // Receiving side
                            byte[] data = Base64.decode(_hello, Base64.DEFAULT);
                            String base64 = new String(data, "UTF-8");

                            Log.i("base64 ::",""+base64);

                            loadActualWallet(base64);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("volley", "Error: " + error.getMessage());
                error.printStackTrace();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonObjRequest, tag_json_obj);
    }

    private void loadActualWallet(String base64) {
        Log.i("base64 v20 ::::", "" + base64);


        StringRequest request = new StringRequest(Request.Method.GET, base64, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("loadWallet ::::", "" + response);
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error is ", "" + error);
            }
        }) {
            //This is for Headers If You Needed
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", _token);
                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(request);
    }

    private void transferReq(String base64) {
        String tag_json_obj = "json_obj_req";

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("beneId", "168");
        params.put("coustomerId", "82");
        params.put("amount", "100");
        params.put("pipeNo", "1");
        params.put("uid", "foo");
        params.put("transactionMode", "IMPS");

        HashMap<String, String> user = session.getUserDetails();
        final String _token = user.get(SessionManager.KEY_TOKEN);


        JsonObjectRequest request_json = new JsonObjectRequest(base64, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.i(" response :: ", "" + response);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
            }
        }) {
            //This is for Headers If You Needed
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
//                params.put("Authorization", _token);
                return params;
            }

            //Pass Your Parameters here
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }
        };

        // add the request object to the queue to be executed
        AppController.getInstance().addToRequestQueue(request_json);

    }

    private void load() {
        GetDataService getDataService = DMTRetrofitInstance.getretrofitInstance("https://dmt.iserveu.tech").create(GetDataService.class);
        getDataService.getV5().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                if (response.isSuccessful()) {

                    try {
                        String res = response.body().string();
                        Log.d("v2 res ::::::", res);

                        JSONObject jsonObject = new JSONObject(res);
                        String url = jsonObject.getString("hello");

                        byte[] data = Base64.decode(url, Base64.DEFAULT);
                        String base64 = new String(data, "UTF-8");

                        Log.d("v2 base64 ::::::", base64);
                        transferReq(base64);
                    } catch (Exception exc) {
                        exc.getMessage();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.i(" ResponseBody v2 ::::::", ":::::::: ResponseBody v2 :::::");
            }
        });

    }

}
