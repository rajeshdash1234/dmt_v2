package com.orbo.isu_customer.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.android.material.snackbar.Snackbar;
import com.isu.dmt.R;
import com.orbo.isu_customer.utility.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class LandingkardLoanActivity extends AppCompatActivity {

    EditText fname, lname, email, mobile, business_revenue, business_age, pan, pincode, address, city, state, loan_amount, ifsc_code, business_address,
            business_city, business_pincode, business_state, business_company_name, business_company_email, business_company_mobile, business_company_pincode, business_company_city,
            business_company_state, business_company_address, business_company_pancard, business_company_gst_number, personal_aadhar_number, directiry_fname, directiry_lname,
            directiry_email, directiry_mobile, directiry_pancard, directiry_ownership, directiry_address, directiry_city, directiry_pincode, directiry_state, directiry_uniqueid,
            directiry_additional_id, directiry_another_additional_id, niyo_partner_company_id;

    EditText dob, directorsdob;

    Spinner registered_as, business_run_by, bank_code, nature_of_business, product_category, purpose_of_loan, loan_time, type_id, sector_type, sector_sub_type;

    Button submit;

    String _fname, _lname, _email, _mobile, _business_revenue, _business_age, _dob, _pan, _pincode, _address, _city, _state, _loan_amount, _ifsc_code, _business_address,
            _business_city, _business_pincode, _business_state, _business_company_name, _business_company_email, _business_company_mobile, _business_company_pincode, _business_company_city,
            _business_company_state, _business_company_address, _business_company_pancard, _business_company_gst_number, _personal_aadhar_number, _directiry_fname, _directiry_lname,
            _directiry_email, _directiry_mobile, _directiry_pancard, _directiry_ownership, _directiry_address, _directiry_city, _directiry_pincode, _directiry_state, _directiry_uniqueid,
            _directiry_additional_id, _directiry_another_additional_id, _directorsdob, _niyo_partner_company_id;

    String _registered_as, _business_run_by, _bank_code, _nature_of_business, _product_category, _purpose_of_loan, _loan_time, _type_id, _sector_type, _sector_sub_type;

    RadioGroup cibil, gender, ownership, bank_account_type, netbanking, business_ownership, business_company_ownership;

    String _cibil, _gender, _ownership, _bank_account_type, _netbanking, _business_ownership, _business_company_ownership;

    ArrayList<String> businessConstitutionName = new ArrayList<>();
    ArrayList<String> businessConstitutionId = new ArrayList<>();

    ArrayList<String> sectorTypeName = new ArrayList<>();
    ArrayList<String> sectorTypeId = new ArrayList<>();

    ArrayList<String> subSectorTypeName = new ArrayList<>();
    ArrayList<String> subSectorTypeId = new ArrayList<>();

    ArrayList<String> bankCodeKey = new ArrayList<>();
    ArrayList<String> bankCodeName = new ArrayList<>();

    ArrayList<String> registeredAsArrayList = new ArrayList<>();
    ArrayList<String> businessRunByNameArrayList = new ArrayList<>();
    ArrayList<String> natureOfBusinessforArrayList = new ArrayList<>();
    ArrayList<String> productcategoryArrayList = new ArrayList<>();
    ArrayList<String> purposeofloanArrayList = new ArrayList<>();
    ArrayList<String> loanNeedTimeArrayList = new ArrayList<>();


    DatePickerDialog datePickerDialog;
    int year, month, dayOfMonth;
    Calendar calendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_landingkard_loan);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Customer Details");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        fname = findViewById(R.id.fname);
        lname = findViewById(R.id.lname);
        email = findViewById(R.id.email);
        mobile = findViewById(R.id.mobile);
        business_revenue = findViewById(R.id.business_revenue);
        business_age = findViewById(R.id.business_age);
        dob = findViewById(R.id.dob);
        pan = findViewById(R.id.pan);
        pincode = findViewById(R.id.pincode);
        registered_as = findViewById(R.id.registered_as);
        address = findViewById(R.id.address);
        city = findViewById(R.id.city);
        state = findViewById(R.id.state);
        loan_amount = findViewById(R.id.loan_amount);
        ifsc_code = findViewById(R.id.ifsc_code);

        business_address = findViewById(R.id.business_address);
        business_city = findViewById(R.id.business_city);
        business_pincode = findViewById(R.id.business_pincode);
        business_state = findViewById(R.id.business_state);
        business_company_name = findViewById(R.id.business_company_name);
        business_company_email = findViewById(R.id.business_company_email);
        business_company_mobile = findViewById(R.id.business_company_mobile);
        business_company_pincode = findViewById(R.id.business_company_pincode);
        business_company_city = findViewById(R.id.business_company_city);
        business_company_state = findViewById(R.id.business_company_state);
        business_company_address = findViewById(R.id.business_company_address);
        business_company_pancard = findViewById(R.id.business_company_pancard);
        business_company_gst_number = findViewById(R.id.business_company_gst_number);
        personal_aadhar_number = findViewById(R.id.personal_aadhar_number);

        directiry_fname = findViewById(R.id.directiry_fname);
        directiry_lname = findViewById(R.id.directiry_lname);
        directiry_email = findViewById(R.id.directiry_email);
        directiry_mobile = findViewById(R.id.directiry_mobile);
        directiry_pancard = findViewById(R.id.directiry_pancard);
        directiry_ownership = findViewById(R.id.directiry_ownership);
        directiry_address = findViewById(R.id.directiry_address);
        directiry_city = findViewById(R.id.directiry_city);
        directiry_pincode = findViewById(R.id.directiry_pincode);
        directiry_state = findViewById(R.id.directiry_state);
        directiry_uniqueid = findViewById(R.id.directiry_uniqueid);
        directiry_additional_id = findViewById(R.id.directiry_additional_id);
        directiry_another_additional_id = findViewById(R.id.directiry_another_additional_id);
        directorsdob = findViewById(R.id.directorsdob);


        registered_as = findViewById(R.id.registered_as);
        business_run_by = findViewById(R.id.business_run_by);
        bank_code = findViewById(R.id.bank_code);
        nature_of_business = findViewById(R.id.nature_of_business);
        product_category = findViewById(R.id.product_category);
        purpose_of_loan = findViewById(R.id.purpose_of_loan);
        loan_time = findViewById(R.id.loan_time);

        type_id = findViewById(R.id.type_id);
        niyo_partner_company_id = findViewById(R.id.niyo_partner_company_id);
        sector_type = findViewById(R.id.sector_type);
        sector_sub_type = findViewById(R.id.sector_sub_type);


        cibil = findViewById(R.id.cibil);
        gender = findViewById(R.id.gender);

        ownership = findViewById(R.id.ownership);
        bank_account_type = findViewById(R.id.bank_account_type);
        netbanking = findViewById(R.id.netbanking);
        business_ownership = findViewById(R.id.business_ownership);
        business_company_ownership = findViewById(R.id.business_company_ownership);

        cibil.clearCheck();
        gender.clearCheck();
        ownership.clearCheck();
        bank_account_type.clearCheck();
        netbanking.clearCheck();
        business_ownership.clearCheck();
        business_company_ownership.clearCheck();

        cibil.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = group.findViewById(checkedId);
                _cibil = rb.getText().toString();
            }
        });


        gender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = group.findViewById(checkedId);
                _gender = rb.getText().toString();
            }
        });


        ownership.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = group.findViewById(checkedId);
                _ownership = rb.getText().toString();
            }
        });


        bank_account_type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = group.findViewById(checkedId);
                _bank_account_type = rb.getText().toString();
            }
        });



       /* directorsdob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar = Calendar.getInstance();
                year = calendar.get(Calendar.YEAR);
                month = calendar.get(Calendar.MONTH);
                dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
                datePickerDialog = new DatePickerDialog(LandingkardLoanActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                                directorsdob.setText(year  + "/" + (month + 1) + "/" + day);
                            }
                        }, year, month, dayOfMonth);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });


        dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar = Calendar.getInstance();
                year = calendar.get(Calendar.YEAR);
                month = calendar.get(Calendar.MONTH);
                dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
                datePickerDialog = new DatePickerDialog(LandingkardLoanActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                                dob.setText(year  + "/" + (month + 1) + "/" + day);
                            }
                        }, year, month, dayOfMonth);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });

*/

        netbanking.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = group.findViewById(checkedId);
                String netbanking = rb.getText().toString();

                if(netbanking.equals("true")){
                    _netbanking = "true";
                }else{
                    _netbanking = "no";
                }
            }
        });


        business_ownership.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = group.findViewById(checkedId);
                _business_ownership = rb.getText().toString();
            }
        });


        business_company_ownership.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = group.findViewById(checkedId);
                _business_company_ownership = rb.getText().toString();
            }
        });


        submit = findViewById(R.id.submit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _fname = fname.getText().toString();
                _lname = lname.getText().toString();
                _email = email.getText().toString();
                _mobile = mobile.getText().toString();
                _business_revenue = business_revenue.getText().toString();
                _business_age = business_age.getText().toString();
                _dob = dob.getText().toString();
                _pan = pan.getText().toString();
                _pincode = pincode.getText().toString();
                _address = address.getText().toString();
                _city = city.getText().toString();
                _state = state.getText().toString();
                _loan_amount = loan_amount.getText().toString();
                _ifsc_code = ifsc_code.getText().toString();
                _business_address = business_address.getText().toString();

                _business_city = business_city.getText().toString();
                _business_pincode = business_pincode.getText().toString();
                _business_state = business_state.getText().toString();
                _business_company_name = business_company_name.getText().toString();
                _business_company_email = business_company_email.getText().toString();
                _business_company_mobile = business_company_mobile.getText().toString();
                _business_company_pincode = business_company_pincode.getText().toString();
                _business_company_city = business_company_city.getText().toString();
                _business_company_state = business_company_state.getText().toString();
                _business_company_address = business_company_address.getText().toString();
                _business_company_pancard = business_company_pancard.getText().toString();
                _business_company_gst_number = business_company_gst_number.getText().toString();
                _personal_aadhar_number = personal_aadhar_number.getText().toString();

                _directiry_fname = directiry_fname.getText().toString();
                _directiry_lname = directiry_lname.getText().toString();
                _directorsdob = directorsdob.getText().toString();
                _directiry_email = directiry_email.getText().toString();
                _directiry_mobile = directiry_mobile.getText().toString();
                _directiry_pancard = directiry_pancard.getText().toString();
                _directiry_ownership = directiry_ownership.getText().toString();
                _directiry_address = directiry_address.getText().toString();
                _directiry_city = directiry_city.getText().toString();
                _directiry_pincode = directiry_pincode.getText().toString();
                _directiry_state = directiry_state.getText().toString();
                _directiry_uniqueid = directiry_uniqueid.getText().toString();
                _directiry_additional_id = directiry_additional_id.getText().toString();
                _directiry_another_additional_id = directiry_another_additional_id.getText().toString();
                _niyo_partner_company_id = niyo_partner_company_id.getText().toString();

                String name = "^[\\p{L} .'-]+$";
                String email = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
                String mobile = "^[6-9]{1}[0-9]{9}$";
                String Personal_aadhar_dir = "[0-9]{12}";
                String personalPAN_Dir = "[A-Z]{3}[P]{1}[A-Z]{1}[0-9]{4}[A-Z]{1}";
                String company_gst = "^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[0-9]{1}[Z]{1}[0-9]{1}$";
                String company_pan = "[A-Z]{3}[C,H,A,B,G,J,L,E,F,T]{1}[A-Z]{1}[0-9]{4}[A-Z]{1}";
                String companyServiceTaxNumber = "^[A-Z]{5}[0-9]{4}[A-Z]{1}[S]{1}[D|T]{1}[0-9]{3}$";


                if (!_fname.matches(name)) {
                    Toast.makeText(getApplicationContext(), "Enter First Name", Toast.LENGTH_SHORT).show();
                } else if (!_lname.matches(name)) {
                    Toast.makeText(getApplicationContext(), "Enter Last Name", Toast.LENGTH_SHORT).show();
                } else if (!_email.matches(email)) {
                    Toast.makeText(getApplicationContext(), "Enter valid Email Address", Toast.LENGTH_SHORT).show();
                } else if (!_mobile.matches(mobile)) {
                    Toast.makeText(getApplicationContext(), "Enter valid Mobile Number", Toast.LENGTH_SHORT).show();
                }else if (_business_revenue.equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter business revenue", Toast.LENGTH_SHORT).show();
                }else if (_business_age.equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter business age", Toast.LENGTH_SHORT).show();
                }else if (_dob.equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter DOM", Toast.LENGTH_SHORT).show();
                }else if (_pincode.equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter Pincode", Toast.LENGTH_SHORT).show();
                }else if (_address.equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter Address", Toast.LENGTH_SHORT).show();
                }else if (_city.equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter City", Toast.LENGTH_SHORT).show();
                }else if (_state.equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter State", Toast.LENGTH_SHORT).show();
                }else if (_loan_amount.equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter loan amount", Toast.LENGTH_SHORT).show();
                }else if (_ifsc_code.equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter Bank IFSC code", Toast.LENGTH_SHORT).show();
                }else if (_business_address.equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter business address", Toast.LENGTH_SHORT).show();
                }else if (_business_city.equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter business city", Toast.LENGTH_SHORT).show();
                }else if (_business_pincode.equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter business pincode", Toast.LENGTH_SHORT).show();
                }else if (_business_state.equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter business state", Toast.LENGTH_SHORT).show();
                }else if (_business_company_name.equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter company name", Toast.LENGTH_SHORT).show();
                }else if (_business_company_email.equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter company email", Toast.LENGTH_SHORT).show();
                }else if (_business_company_mobile.equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter company mobile", Toast.LENGTH_SHORT).show();
                }else if (_business_company_pincode.equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter company pincode", Toast.LENGTH_SHORT).show();
                }else if (_business_company_city.equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter company city", Toast.LENGTH_SHORT).show();
                }else if (_business_company_state.equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter company state", Toast.LENGTH_SHORT).show();
                }else if (_business_company_address.equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter company address", Toast.LENGTH_SHORT).show();
                } else if (!_pan.matches(personalPAN_Dir)) {
                    Toast.makeText(getApplicationContext(), "Enter valid PAN Detail", Toast.LENGTH_SHORT).show();
                } else if (!_business_company_email.matches(email)) {
                    Toast.makeText(getApplicationContext(), "Enter valid Company Email Address", Toast.LENGTH_SHORT).show();
                } else if (!_business_company_pancard.matches(company_pan)) {
                    Toast.makeText(getApplicationContext(), "Enter valid Company PAN Detail", Toast.LENGTH_SHORT).show();
                } else if (!_business_company_gst_number.matches(company_gst)) {
                    Toast.makeText(getApplicationContext(), "Enter valid GST Number", Toast.LENGTH_SHORT).show();
                } else if (!_personal_aadhar_number.matches(Personal_aadhar_dir)) {
                    Toast.makeText(getApplicationContext(), "Enter valid Aadhar Number", Toast.LENGTH_SHORT).show();
                } else if (!_directiry_email.matches(email)) {
                    Toast.makeText(getApplicationContext(), "Enter valid director email address", Toast.LENGTH_SHORT).show();
                } else if (!_directiry_mobile.matches(mobile)) {
                    Toast.makeText(getApplicationContext(), "Enter valid director mobile number", Toast.LENGTH_SHORT).show();
                } else if (!_directiry_pancard.matches(personalPAN_Dir)) {
                    Toast.makeText(getApplicationContext(), "Enter valid director PAN number", Toast.LENGTH_SHORT).show();
                } else {

                    HashMap<String, String> _personalAddress = new HashMap<>();
                    _personalAddress.put("pincode", _pincode);
                    _personalAddress.put("ownership", _ownership);
                    _personalAddress.put("address", _address);
                    _personalAddress.put("city", _city);
                    _personalAddress.put("state", _state);

                    HashMap<String, String> _business = new HashMap<>();
                    _business.put("ownership", _business_ownership);
                    _business.put("address", _business_company_address);
                    _business.put("city", _business_city);
                    _business.put("pincode", _business_pincode);
                    _business.put("state", _business_state);

                    ArrayList<String> arrayListOthernature_of_business = new ArrayList<>();
                    arrayListOthernature_of_business.add(_nature_of_business);

                    HashMap<String, String> _otherFields = new HashMap<>();
                    _otherFields.put("testfield1", _directiry_additional_id);
                    _otherFields.put("testfield2", _directiry_another_additional_id);
                    _otherFields.put("testfield3", "123");

                    JSONObject jsonObject = new JSONObject();

                    try {
                        jsonObject.put("firstName", _fname);
                        jsonObject.put("lastName", _lname);
                        jsonObject.put("email", _email);
                        jsonObject.put("mobile", _mobile);
                        jsonObject.put("businessAge", _business_age);
                        jsonObject.put("businessRevenue", _business_revenue);
                        jsonObject.put("registeredAs", _registered_as);
                        jsonObject.put("personalDob", _dob);
                        jsonObject.put("personalPAN", _pan);
                        jsonObject.put("gender", _gender);
                        jsonObject.put("cibilConsentForLK", _cibil);
                        jsonObject.put("mobileNoVerified", "true");
                        jsonObject.put("personalAddress", new JSONObject(_personalAddress));
                        jsonObject.put("businessRunBy", _business_run_by);
                        jsonObject.put("loanAmount", _loan_amount);


                        JSONObject _bankDetails = new JSONObject();
                        _bankDetails.put("bankCode", _bank_code);
                        _bankDetails.put("ifsc", _ifsc_code);
                        _bankDetails.put("typeOfBankAccount", _bank_account_type);
                        _bankDetails.put("customerUsesNetBanking", _netbanking);


                        jsonObject.put("bankDetails", new JSONArray(_bankDetails));
                        jsonObject.put("bankOthers", "");
                        jsonObject.put("businessAddress", new JSONObject(_business));
                        jsonObject.put("productCategory", _product_category);
                        jsonObject.put("personalAadhaar", _personal_aadhar_number);
                        jsonObject.put("companyEmail", _email);
                        jsonObject.put("companyGstNumber", _business_company_gst_number);
                        jsonObject.put("companyMobile", _business_company_mobile);
                        jsonObject.put("companyName", _business_company_name);
                        jsonObject.put("companyPAN", _business_company_pancard);


                        jsonObject.put("natureOfBusiness", arrayListOthernature_of_business);
                        jsonObject.put("natureOfBusinessOthers", "");
                        jsonObject.put("purposeOfLoan", _purpose_of_loan);
                        jsonObject.put("purposeOfLoanOthers", "");


                        JSONObject directiryaddress = new JSONObject();
                        directiryaddress.put("ownership", _directiry_ownership);
                        directiryaddress.put("address", _directiry_address);
                        directiryaddress.put("city", _directiry_city);
                        directiryaddress.put("pincode", _directiry_pincode);
                        directiryaddress.put("state", _directiry_state);


                        JSONObject _directors = new JSONObject();
                        _directors.put("dob", _directorsdob);
                        _directors.put("email", _directiry_email);
                        _directors.put("firstName", _directiry_fname);
                        _directors.put("lastName", _directiry_lname);
                        _directors.put("mobile", _directiry_mobile);
                        _directors.put("pan", _directiry_pancard);
                        _directors.put("residentialAddress", directiryaddress);

                        JSONArray jsonArray = new JSONArray(_directors);

                        jsonObject.put("otherDirectors", jsonArray);
                        jsonObject.put("language", "en");
                        jsonObject.put("loanNeedTime", _loan_time);
                        jsonObject.put("uniqueId", _directiry_uniqueid);

                        jsonObject.put("otherFields", new JSONObject(_otherFields));

                        jsonObject.put("companyemail", _business_company_email);
                        jsonObject.put("mobilenumber", _business_company_mobile);
                        jsonObject.put("typeid", _type_id);
                        jsonObject.put("companyid", _niyo_partner_company_id);
                        jsonObject.put("sectortype", _sector_type);
                        jsonObject.put("subsectortype", _sector_sub_type);
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    loadcreate(jsonObject);
                }
            }
        });

        registered_as.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    _registered_as = registeredAsArrayList.get(position);

                } catch (Exception exc) {
                    exc.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        business_run_by.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    _business_run_by = businessRunByNameArrayList.get(position);
                } catch (Exception exc) {
                    exc.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        bank_code.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    String bank_key = bankCodeName.get(position);
                    int pos = bankCodeName.indexOf(bank_key);
                    _bank_code = bankCodeKey.get(pos);
                } catch (Exception exc) {
                    exc.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        nature_of_business.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    _nature_of_business = natureOfBusinessforArrayList.get(position);
                } catch (Exception exc) {
                    exc.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        product_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    _product_category = productcategoryArrayList.get(position);
                } catch (Exception exc) {
                    exc.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        purpose_of_loan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    _purpose_of_loan = purposeofloanArrayList.get(position);
                } catch (Exception exc) {
                    exc.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        loan_time.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    _loan_time = loanNeedTimeArrayList.get(position);
                } catch (Exception exc) {
                    exc.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        type_id.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                try {
                    String type_name = businessConstitutionName.get(position);
                    int pos = businessConstitutionName.indexOf(type_name);
                    _type_id = businessConstitutionId.get(pos);
                } catch (Exception exc) {
                    exc.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sector_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                try {
                    String sector_name = sectorTypeName.get(position);
                    int pos = sectorTypeName.indexOf(sector_name);
                    _sector_type = sectorTypeId.get(pos);
                } catch (Exception exc) {
                    exc.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sector_sub_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                try {
                    String sub_sector_name = subSectorTypeName.get(position);
                    int pos = subSectorTypeName.indexOf(sub_sector_name);
                    _sector_sub_type = subSectorTypeId.get(pos);
                } catch (Exception exc) {
                    exc.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        loadInfo();

        loadLandingKartData();
    }

    private void loadLandingKartData() {

        String tag_json_obj = "json_obj_req";
        String url = "https://us-central1-creditapp-29bf2.cloudfunctions.net/api/application/lendkartdata";

        StringRequest jsonObjRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject obj = null;
                        try {

                            obj = new JSONObject(response);
                            JSONArray bankCode = obj.getJSONArray("bankCode");
                            JSONArray registeredAsformobile = obj.getJSONArray("registeredAsformobile");
                            JSONArray businessRunByformobile = obj.getJSONArray("businessRunByformobile");
                            JSONArray natureOfBusinessformobile = obj.getJSONArray("natureOfBusinessformobile");
                            JSONArray productcategoryformobile = obj.getJSONArray("productcategoryformobile");
                            JSONArray purposeOfLoanformobile = obj.getJSONArray("purposeOfLoanformobile");
                            JSONArray loanNeedTimeformobile = obj.getJSONArray("loanNeedTimeformobile");


                            for (int k = 0; k < registeredAsformobile.length(); k++) {
                                JSONObject jsonObject = registeredAsformobile.getJSONObject(k);
                                registeredAsArrayList.add(jsonObject.getString("name"));
                            }


                            for (int k = 0; k < businessRunByformobile.length(); k++) {
                                JSONObject jsonObject = businessRunByformobile.getJSONObject(k);
                                businessRunByNameArrayList.add(jsonObject.getString("name"));
                            }


                            for (int k = 0; k < natureOfBusinessformobile.length(); k++) {
                                JSONObject jsonObject = natureOfBusinessformobile.getJSONObject(k);
                                natureOfBusinessforArrayList.add(jsonObject.getString("name"));
                            }


                            for (int k = 0; k < bankCode.length(); k++) {
                                JSONObject jsonObject = bankCode.getJSONObject(k);
                                bankCodeKey.add(jsonObject.getString("key"));
                                bankCodeName.add(jsonObject.getString("name"));
                            }

                            for (int k = 0; k < productcategoryformobile.length(); k++) {
                                JSONObject jsonObject = productcategoryformobile.getJSONObject(k);
                                productcategoryArrayList.add(jsonObject.getString("name"));
                            }

                            for (int k = 0; k < purposeOfLoanformobile.length(); k++) {
                                JSONObject jsonObject = purposeOfLoanformobile.getJSONObject(k);
                                purposeofloanArrayList.add(jsonObject.getString("name"));
                            }

                            for (int k = 0; k < loanNeedTimeformobile.length(); k++) {
                                JSONObject jsonObject = loanNeedTimeformobile.getJSONObject(k);
                                loanNeedTimeArrayList.add(jsonObject.getString("name"));
                            }


                            ArrayAdapter registered_as_adapter = new ArrayAdapter<String>(LandingkardLoanActivity.this, android.R.layout.simple_spinner_item, registeredAsArrayList);
                            registered_as_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            registered_as.setAdapter(registered_as_adapter);

                            ArrayAdapter businessRunBy_adapter = new ArrayAdapter<String>(LandingkardLoanActivity.this, android.R.layout.simple_spinner_item, businessRunByNameArrayList);
                            businessRunBy_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            business_run_by.setAdapter(businessRunBy_adapter);

                            ArrayAdapter natureOfBusiness_adapter = new ArrayAdapter<String>(LandingkardLoanActivity.this, android.R.layout.simple_spinner_item, natureOfBusinessforArrayList);
                            natureOfBusiness_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            nature_of_business.setAdapter(natureOfBusiness_adapter);

                            ArrayAdapter productcategory_adapter = new ArrayAdapter<String>(LandingkardLoanActivity.this, android.R.layout.simple_spinner_item, productcategoryArrayList);
                            natureOfBusiness_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            product_category.setAdapter(productcategory_adapter);

                            ArrayAdapter purposeofloan_adapter = new ArrayAdapter<String>(LandingkardLoanActivity.this, android.R.layout.simple_spinner_item, purposeofloanArrayList);
                            natureOfBusiness_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            purpose_of_loan.setAdapter(purposeofloan_adapter);


                            ArrayAdapter loantime_adapter = new ArrayAdapter<String>(LandingkardLoanActivity.this, android.R.layout.simple_spinner_item, loanNeedTimeArrayList);
                            natureOfBusiness_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            loan_time.setAdapter(loantime_adapter);


                            ArrayAdapter adapter = new ArrayAdapter<String>(LandingkardLoanActivity.this, android.R.layout.simple_spinner_item, bankCodeName);
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            bank_code.setAdapter(adapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (Throwable t) {
                            t.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("volley", "Error: " + error.getMessage());
                error.printStackTrace();
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonObjRequest, tag_json_obj);
    }

    private void loadInfo() {
        String tag_json_obj = "json_obj_req";
        String url = "https://us-central1-creditapp-29bf2.cloudfunctions.net/api/application/getmasterdata";

        //        final ProgressDialog pDialog = new ProgressDialog(this);
//        pDialog.setMessage("Loading...");
//        pDialog.show();

        StringRequest jsonObjRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // pDialog.dismiss();
                        JSONObject obj = null;

                        try {

                            obj = new JSONObject(response);

                            JSONObject data = obj.getJSONObject("data");
                            JSONArray BusinessConstitution = data.getJSONArray("BusinessConstitution");
                            JSONArray Sector = data.getJSONArray("Sector");
                            JSONArray SubSector = data.getJSONArray("SubSector");

                            for (int k = 0; k < BusinessConstitution.length(); k++) {
                                JSONObject jsonObject = BusinessConstitution.getJSONObject(k);
                                String name = jsonObject.getString("text");
                                String id = jsonObject.getString("value");
                                businessConstitutionName.add(name);
                                businessConstitutionId.add(id);
                            }

                            ArrayAdapter adapter = new ArrayAdapter<String>(LandingkardLoanActivity.this, android.R.layout.simple_spinner_item, businessConstitutionName);
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            type_id.setAdapter(adapter);

                            for (int k = 0; k < Sector.length(); k++) {
                                JSONObject jsonObject = Sector.getJSONObject(k);
                                String name = jsonObject.getString("text");
                                String id = jsonObject.getString("value");
                                sectorTypeName.add(name);
                                sectorTypeId.add(id);
                            }

                            ArrayAdapter sector_adapter = new ArrayAdapter<String>(LandingkardLoanActivity.this, android.R.layout.simple_spinner_item, sectorTypeName);
                            sector_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            sector_type.setAdapter(sector_adapter);


                            for (int k = 0; k < SubSector.length(); k++) {
                                JSONObject jsonObject = SubSector.getJSONObject(k);
                                String name = jsonObject.getString("text");
                                String id = jsonObject.getString("value");
                                subSectorTypeName.add(name);
                                subSectorTypeId.add(id);
                            }

                            ArrayAdapter subsector_adapter = new ArrayAdapter<String>(LandingkardLoanActivity.this, android.R.layout.simple_spinner_item, subSectorTypeName);
                            subsector_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            sector_sub_type.setAdapter(subsector_adapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (Throwable t) {
                            t.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("volley", "Error: " + error.getMessage());
                error.printStackTrace();
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonObjRequest, tag_json_obj);
    }

    private void loadcreate(JSONObject obj) {
        String tag_json_obj = "json_obj_req";
        String url = "https://us-central1-creditapp-29bf2.cloudfunctions.net/api/application/create";

        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {

               // Log.i("response 1 ::::::", jsonObject.toString());

                try{
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");
                    pDialog.hide();

                    if (status.equals("1")){
                        String key = jsonObject.getString("key");
                        Toast.makeText(LandingkardLoanActivity.this, "Your request submitted successfully and your id is "+key, Toast.LENGTH_SHORT).show();
                        finish();
                    }else{
                        Toast.makeText(LandingkardLoanActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception exc){
                    exc.printStackTrace();
                    pDialog.hide();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.hide();
                Log.i("response 2 ::::::", "" + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }

        };

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

       /* StringRequest jsonObjRequest = new StringRequest(Request.Method.POST,url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        JSONObject obj = null;

                        try {

                            obj = new JSONObject(response);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (Throwable t) {
                            t.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("volley", "Error: " + error.getMessage());
                error.printStackTrace();
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                String parsed;
                try {
                    parsed = new String(response.data, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return super.getHeaders();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonObjRequest, tag_json_obj);*/
    }

}
