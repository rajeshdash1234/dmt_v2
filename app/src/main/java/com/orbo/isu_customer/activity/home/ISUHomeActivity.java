package com.orbo.isu_customer.activity.home;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.navigation.NavigationView;
import com.isu.dmt.R;
import com.orbo.isu_customer.activity.AddMoneyActivity;
import com.orbo.isu_customer.activity.LanguageActivity;
import com.orbo.isu_customer.activity.ProfileActivity;
import com.orbo.isu_customer.activity.SessionManager;
import com.orbo.isu_customer.activity.Settings.SettingsActivity;
import com.orbo.isu_customer.activity.report.ReportActivity;
import com.orbo.isu_customer.activity.ui.BluetoothConnDevice;
import com.orbo.isu_customer.activity.ui.home.HomeFragment;
import com.orbo.isu_customer.fragment.AEPSFragment;
import com.orbo.isu_customer.fragment.ATMFragment;
import com.orbo.isu_customer.fragment.TransferFragment;
import com.orbo.isu_customer.fundtransferreport.FundTransferReport;
import com.orbo.isu_customer.matmtransaction.MicroAtmReportActivity;
import com.orbo.isu_customer.utility.Constants;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class ISUHomeActivity extends AppCompatActivity implements HomeContract.View, NavigationView.OnNavigationItemSelectedListener {

    // private AppBarConfiguration mAppBarConfiguration;

    TextView das_user_name, das_user_email;

    ImageView menu_nav, go_profile, con_menu,con_notification,add_money;
    LinearLayout nav_host_fragment, transfer, aeps, atm;
    CardView _nav_view;
    SessionManager session;
    HomePresenter homePresenter;

    String _token = "", _admin = "";

    DrawerLayout drawer;

    NavigationView navigationView;
    ProgressBar loadProgress;

    ProgressBar progressBar;

    String deviceSerialNumber = "0";
    String morphodeviceid = "SAGEM SA";
    String mantradeviceid = "MANTRA";
    String morphoe2device = "Morpho";
    SharedPreferences sharedpreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedpreferences = getSharedPreferences("language", Context.MODE_PRIVATE);
        String lang  = sharedpreferences.getString("lang","en");
        Locale locale = new Locale(lang);

        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;

        getBaseContext().getResources().updateConfiguration(config,getBaseContext().getResources().getDisplayMetrics());

        setContentView(R.layout.activity_isuhome);

        transfer = findViewById(R.id.transfer);
        nav_host_fragment = findViewById(R.id.nav_host_fragment);
        aeps = findViewById(R.id.aeps);
        atm = findViewById(R.id.atm);

        _nav_view = findViewById(R.id._nav_mainview);

        progressBar = findViewById(R.id.progressBar);
        con_notification = findViewById(R.id.con_notification);
        add_money = findViewById(R.id.add_money);

        session = new SessionManager(getApplicationContext());

        HashMap<String, String> user = session.getUserDetails();
        _token = user.get(SessionManager.KEY_TOKEN);
        _admin = user.get(SessionManager.KEY_ADMIN);

        loadProgress = new ProgressBar(ISUHomeActivity.this);
        loadProgress.setVisibility(View.VISIBLE);

        homePresenter = new HomePresenter(this);

        homePresenter.getHomeDashboardResponse("https://itpl.iserveu.tech");

        transfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TransferFragment bottomSheetFragment = new TransferFragment(ISUHomeActivity.this);
                bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
            }
        });

        con_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        add_money.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ISUHomeActivity.this, AddMoneyActivity.class);
                startActivity(intent);
            }
        });


        aeps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HashMap<String, String> deviceInfo = session.getDeviceInfoSession();

                try {
                    if (deviceInfo.get("manufacturername").trim().equalsIgnoreCase(mantradeviceid)) {
                       // capture();
                        AEPSFragment bottomSheetFragment = new AEPSFragment(ISUHomeActivity.this);
                        bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());

                    } else if (deviceInfo.get("manufacturername").trim().equalsIgnoreCase(morphoe2device)) {
                        ATMFragment bottomSheetFragment = new ATMFragment(ISUHomeActivity.this);
                        bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());


                    } else {
                        Intent intent = new Intent(ISUHomeActivity.this,SettingsActivity.class);
                        startActivity(intent);
                        Toast.makeText(ISUHomeActivity.this, getResources().getString(R.string.recommended_device), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception exc) {
                    exc.printStackTrace();
                    Intent intent = new Intent(ISUHomeActivity.this,SettingsActivity.class);
                    startActivity(intent);
                    Toast.makeText(ISUHomeActivity.this, getResources().getString(R.string.recommended_device), Toast.LENGTH_SHORT).show();
                }

            }
        });


        atm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ATMFragment bottomSheetFragment = new ATMFragment(ISUHomeActivity.this);
                bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
            }
        });

        menu_nav = findViewById(R.id.menu_nav);


        con_menu = findViewById(R.id.con_menu);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);

        menu_nav.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                drawer.openDrawer(Gravity.START);
            }
        });

        con_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
            }
        });


        _nav_view.setVisibility(View.VISIBLE);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerview = navigationView.getHeaderView(0);
        das_user_name = headerview.findViewById(R.id.das_user_name);
        das_user_email = headerview.findViewById(R.id.das_user_email);
        go_profile = headerview.findViewById(R.id.go_profile);

        go_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
                drawer.closeDrawers();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.isuhome, menu);
        return true;
    }


    @Override
    public void onBackPressed() {
        Constants.BANK_FLAG="";
        super.onBackPressed();
    }

    @Override
    public void fetchedHomeDashboardResponse(boolean status, String response) {
        loadDashboardFeature(response);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        switch (item.getItemId()) {

            case R.id.profile_data: {
             //   startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
                break;
            }

            case R.id.setting: {
                startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
                break;


            }
            case R.id.report_aeps:{
                Intent intent = new Intent(ISUHomeActivity.this, MicroAtmReportActivity.class);
                startActivity(intent);

                break;

            }

            case R.id.report_matm:{
                Intent intent = new Intent(ISUHomeActivity.this, ReportActivity.class);
                startActivity(intent);
                break;
            }

            case R.id.report_dmt:{
                Intent intent = new Intent(ISUHomeActivity.this, FundTransferReport.class);
                startActivity(intent);
                break;
            }

            case R.id.language:{
                Intent intent = new Intent(ISUHomeActivity.this, LanguageActivity.class);
                startActivity(intent);
                break;
            }

        }

        drawer.closeDrawers();

        return true;
    }

    private void loadDashboardFeature(String base64) {

        loadUserDashboard(base64);
    }

    private void loadUserDashboard(String base64) {
        StringRequest request = new StringRequest(Request.Method.GET, base64, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                loadProgress.setVisibility(View.GONE);

                if (!response.equals(null)) {
                    Log.e("Your Array Response", response);
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(response);
                        JSONObject _userInfo = jsonObject.optJSONObject("userInfo");
                        String _userName = _userInfo.optString("userName");
                        String _userType = _userInfo.optString("userType");
                        String _userBalance = _userInfo.optString("userBalance");
                        String _adminName = _userInfo.optString("adminName");
                        String _mposNumber = _userInfo.optString("mposNumber");
                        String _promotionalMessage = _userInfo.optString("promotionalMessage");

                        String _userBrand = _userInfo.optString("userBrand");
                        String _userProfile = String.valueOf(_userInfo.getJSONObject("userProfile"));
                        String _userFeature = String.valueOf(_userInfo.getJSONArray("userFeature"));

                        JSONObject obj = new JSONObject(_userProfile);
                        String fname  = obj.getString("firstName");
                        String lname = obj.getString("lastName");
                        String mobileNo = obj.getString("mobileNumber");
                        String profileName = fname+" "+lname;

                        session.createUserSession(_userName, _userType, _userBalance, _adminName, _mposNumber,_userName,profileName,mobileNo);

                        das_user_name.setText(_userName);
                        das_user_email.setText(_mposNumber);
                        loadProgress.setVisibility(View.GONE);

                        FragmentManager fragmentManager = getSupportFragmentManager();
                        FragmentTransaction tx = fragmentManager.beginTransaction();
                        tx.replace(R.id.nav_host_fragment, new HomeFragment()).commit();

                        progressBar.setVisibility(View.GONE);



                    } catch (JSONException e) {
                        e.printStackTrace();
                        loadProgress.setVisibility(View.GONE);
                    }

                } else {
                    // Log.e("Your Array Response", "Data Null");
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error is ", "" + error);
                loadProgress.setVisibility(View.GONE);
            }
        }) {
            //This is for Headers If You Needed
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", _token);
                return params;
            }

            //Pass Your Parameters here
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                int statusCode = response.statusCode;
                return super.parseNetworkResponse(response);
            }
        };
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(request);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }
}