package com.orbo.isu_customer.activity.transactionstatus;

import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;


import com.isu.dmt.R;
import com.orbo.isu_customer.utility.Constants;
import com.orbo.isu_customer.utility.SharePreferenceClass;


public class TransactionStatusActivity extends AppCompatActivity {
    // Card transaction sucessfully done for the card number . \n \n Reference No : 536645545545 \n Available Balance : 6466.98 \n Transaction Amount : 546.00 \n Transaction Date and TIme : 2018-11-06 13:26:03 \n Terminal ID : IS000004
    LinearLayout successLayout, failureLayout;
    Button okButton, okSuccessButton, printButton;
    TextView detailsTextView, failureDetailTextView, failureTitleTextView;
    SharePreferenceClass sharePreferenceClass;
    String bankName = "N/A";
    String referenceNo = "N/A";
    String balance = "N/A";
    String amount = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_status);
        sharePreferenceClass = new SharePreferenceClass(TransactionStatusActivity.this);
        successLayout = findViewById(R.id.successLayout);
        failureLayout = findViewById(R.id.failureLayout);
        okButton = findViewById(R.id.okButton);
        okSuccessButton = findViewById(R.id.okSuccessButton);
        printButton = findViewById(R.id.printButton);
        detailsTextView = findViewById(R.id.detailsTextView);
        failureTitleTextView = findViewById(R.id.failureTitleTextView);
        failureDetailTextView = findViewById(R.id.failureDetailTextView);

        if (getIntent().getSerializableExtra(Constants.TRANSACTION_STATUS_KEY) == null) {
            failureLayout.setVisibility(View.VISIBLE);
            successLayout.setVisibility(View.GONE);
            failureDetailTextView.setText("Some Exception occured");
        } else {
            TransactionStatusModel transactionStatusModel = (TransactionStatusModel) getIntent().getSerializableExtra(Constants.TRANSACTION_STATUS_KEY);

            if (transactionStatusModel.getStatus().trim().equalsIgnoreCase("0")) {
                failureLayout.setVisibility(View.GONE);
                successLayout.setVisibility(View.VISIBLE);
                String aadharCard = transactionStatusModel.getAadharCard();
                if (transactionStatusModel.getAadharCard() == null) {
                    aadharCard = "N/A";
                } else {
                    if (transactionStatusModel.getAadharCard().equalsIgnoreCase("")) {
                        aadharCard = "N/A";
                    } else {
                        StringBuffer buf = new StringBuffer(aadharCard);
                        buf.replace(0, 10, "XXXX-XXXX-");
                        System.out.println(buf.length());
                        aadharCard = buf.toString();
                    }
                }


                if (transactionStatusModel.getBankName() != null && !transactionStatusModel.getBankName().matches("")) {
                    bankName = transactionStatusModel.getBankName();
                }

                if (transactionStatusModel.getReferenceNo() != null && !transactionStatusModel.getReferenceNo().matches("")) {
                    referenceNo = transactionStatusModel.getReferenceNo();
                }

                if (transactionStatusModel.getBalanceAmount() != null && !transactionStatusModel.getBalanceAmount().matches("")) {
                    balance = transactionStatusModel.getBalanceAmount();
                    if (balance.contains(":")) {
                        String[] separated = balance.split(":");
                        balance = separated[1].trim();
                    }
                }

                if (transactionStatusModel.getTransactionAmount() != null && !transactionStatusModel.getTransactionAmount().matches("")) {
                    amount = transactionStatusModel.getTransactionAmount();
                }

                if (transactionStatusModel.getTransactionType().equalsIgnoreCase("Cash Withdrawal")) {
                    detailsTextView.setText(transactionStatusModel.getTransactionType() + " for customer account linked with aadhar card " + aadharCard + " " + "was successful. \n \n Bank Name : " + bankName + "\n Reference No : " + referenceNo + " \n " + "Account Balance : " + balance + "\n Transaction Amount : " + amount);
                } else if (transactionStatusModel.getTransactionType().equalsIgnoreCase("Balance Enquery")) {
                    detailsTextView.setText(transactionStatusModel.getTransactionType() + " for customer account linked with aadhar card " + aadharCard + " " + "was successful. \n \n Bank Name : " + bankName + "\n Reference No : " + referenceNo + " \n " + "Account Balance : " + balance);
                }
            } else {
                failureLayout.setVisibility(View.VISIBLE);
                successLayout.setVisibility(View.GONE);
                failureDetailTextView.setText(transactionStatusModel.getApiComment());
                failureTitleTextView.setText(transactionStatusModel.getStatusDesc());
            }
        }
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        okSuccessButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        printButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Constants.BRAND_NAME.trim().length() != 0) {

                    BluetoothDevice bluetoothDevice = Constants.bluetoothDevice;
                    if (bluetoothDevice != null) {
                        // bluetoothDevice.createBond();
                        //callBluetoothFunction(bluetoothDevice);
                    } else {
//                        Intent in = new Intent(TransactionStatusActivity.this, MainActivity.class);
//                        startActivity(in);
//
//                        Toast.makeText(TransactionStatusActivity.this, "Please connect the printer", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    showBrandSetAlert();
                }
            }
        });
    }

    private void showBrandSetAlert() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(TransactionStatusActivity.this);
        builder1.setMessage("Unable to download/print the receipt. Please contact admin.");
        builder1.setTitle("Warning!!!");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "GOT IT",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

}
