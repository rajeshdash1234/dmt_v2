package com.orbo.isu_customer.activity.report;



import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;



public interface ReportAPI {
    //"/aeps/txreports"
    @POST()
    Call<ReportResponse> insertUser(@Header("Authorization") String token, @Body ReportRequest body, @Url String url);
}

