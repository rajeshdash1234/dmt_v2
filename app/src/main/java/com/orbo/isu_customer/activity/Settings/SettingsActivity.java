package com.orbo.isu_customer.activity.Settings;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.finopaytech.finosdk.activity.DeviceSettingActivity;
import com.isu.dmt.R;
import com.orbo.isu_customer.activity.SessionManager;
import com.rw.loadingdialog.LoadingView;

import java.util.HashMap;
import java.util.Locale;

import fr.ganfra.materialspinner.MaterialSpinner;

public class SettingsActivity extends AppCompatActivity {

    private MaterialSpinner deviceSpinner, btdeviceSpinner;
    private static final String[] ITEMS = {"Morpho", "Mantra"};
    private static final String[] BT_ITEMS = {"BLUPRINTS"};

    UsbManager musbManager;
    LoadingView loadingView;

    private UsbDevice usbDevice;
    boolean usbconnted = false;
    String deviceSerialNumber = "0";
    String morphodeviceid = "SAGEM SA";
    String mantradeviceid = "MANTRA";
    String morphoe2device = "Morpho";

    SessionManager session;

    TextView bt_device_name, device_name,deviceConfiguration;
    TextView paxdevicepair,paxDeviceName;

    boolean first_run = true;
    boolean from_BT = false, from_RD = false;

    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       /* sharedpreferences = getSharedPreferences("language", Context.MODE_PRIVATE);
        String lang  = sharedpreferences.getString("lang","en");
        Locale locale = new Locale(lang);

        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;

        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
      */
        setContentView(R.layout.activity_settings);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.settings));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        device_name = findViewById(R.id.device_name);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        session = new SessionManager(getApplicationContext());

        deviceSpinner = findViewById(R.id.deviceSpinner);

        paxdevicepair = findViewById(R.id.paxdevicepair);
        paxDeviceName = findViewById(R.id.paxDeviceName);
        deviceConfiguration = findViewById(R.id.deviceConfiguration);
        deviceConfiguration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deviceSpinner.setVisibility(View.VISIBLE);
            }
        });




        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ITEMS);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        deviceSpinner.setAdapter(adapter);



        deviceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    from_BT = false;
                    from_RD = true;

//                    showLoaderader();

                    musbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
                    updateDeviceList();
                }
                if (position == 1) {
                    from_BT = false;
                    from_RD = true;
//                    showLoader();
                    musbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
                    updateDeviceList();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //------------PAX pair----

        paxdevicepair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent settingIntent = new Intent(SettingsActivity.this, DeviceSettingActivity.class);
                settingIntent.putExtra("IS_PAIR_DEVICE",true);
                startActivityForResult(settingIntent,3);

            }
        });

    }

    private void updateDeviceList() {
        HashMap<String, UsbDevice> connectedDevices = musbManager.getDeviceList();
        usbDevice = null;
        //hideLoader();
        if (connectedDevices.isEmpty()) {
            usbconnted = false;
            deviceConnectMessgae();
        } else {
            for (UsbDevice device : connectedDevices.values()) {
                usbconnted = true;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (device != null && device.getManufacturerName() != null) {
                        if (device.getManufacturerName().equalsIgnoreCase(mantradeviceid) || device.getManufacturerName().equalsIgnoreCase(morphodeviceid) || device.getManufacturerName().trim().equalsIgnoreCase(morphoe2device)) {
                            usbDevice = device;
                            deviceSerialNumber = usbDevice.getManufacturerName();

                            Log.i("usbDevice  ::: ", "" + usbDevice.getManufacturerName());
                            Log.i("device Serial ::: ", "" + deviceSerialNumber);

                            // session.setUsbDevice(usbDevice.getManufacturerName());
                            // sharePreferenceClass.setUsbDevice(usbDevice.getManufacturerName());
                            // sharePreferenceClass.setUsbDeviceSerial(deviceSerialNumber);

                            session.createDeviceInfoSession(usbDevice.getManufacturerName(), deviceSerialNumber);
                        }

                        // sharePreferenceClass.setConnectedRD_Device(device.getManufacturerName());

                    }
                }
            }

            devicecheck();
        }
    }

    private void deviceConnectMessgae() {
        // hideLoader();
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setCancelable(false);
        builder.setTitle(getResources().getString(R.string.device_connect))
                .setMessage(getResources().getString(R.string.decive_please_connect))
                .setPositiveButton(getResources().getString(R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                })
                .show();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void devicecheck() {
        if (usbDevice == null) {
            deviceConnectMessgae();
        } else {
            if (deviceSpinner.getSelectedItemPosition() == 1) {
                if (usbDevice.getManufacturerName().trim().equalsIgnoreCase(morphodeviceid) || usbDevice.getManufacturerName().trim().equalsIgnoreCase(morphoe2device)) {
                    morphoinstallcheck();
                } else {
                    deviceConnectMessgae();
                }
            } else if (deviceSpinner.getSelectedItemPosition() == 2) {
                if (usbDevice.getManufacturerName().trim().equalsIgnoreCase(mantradeviceid)) {
                    installcheck();
                } else {
                    deviceConnectMessgae();
                }
            }
        }
    }

    private void morphoinstallcheck() {
        boolean isAppInstalled = appInstalledOrNot("com.scl.rdservice");
        if (isAppInstalled) {
            // This intent will help you to launch if the package is already installed
            Intent intent1 = new Intent();
            intent1.setAction("in.gov.uidai.rdservice.fp.INFO");
            intent1.setPackage("com.scl.rdservice");
            // intent1.addFlags ( Intent.FLAG_ACTIVITY_NEW_TASK );
            startActivityForResult(intent1, 2);
        } else {
            // Do whatever we want to do if application not installed
            // For example, Redirect to play store
            morphoMessage();
        }
    }

    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }
        return false;
    }

    private void morphoMessage() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }

        builder.setCancelable(false);
        builder.setTitle(getResources().getString(R.string.morpho))
                .setMessage(getResources().getString(R.string.install_morpho_message))
                .setPositiveButton(getResources().getString(R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        /*
                         * play store intent
                         */
                        final String appPackageName = "com.scl.rdservice"; // getPackageName() from Context or Activity object
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                })
                .show();
    }

    private void installcheck() {
        boolean isAppInstalled = appInstalledOrNot("com.mantra.clientmanagement");
        boolean serviceAppInstalled = appInstalledOrNot("com.mantra.rdservice");

        if (isAppInstalled) {
            // This intent will help you to launch if the package is already installed
            if (serviceAppInstalled) {
                Intent intent = new Intent("in.gov.uidai.rdservice.fp.INFO");
                intent.setPackage("com.mantra.rdservice");
                startActivityForResult(intent, 1);
            } else {
                rdserviceMessage();
            }
        } else {
            // Do whatever we want to do if application not installed
// For example, Redirect to play store
            mantraMessage();
        }
    }

    private void rdserviceMessage() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setCancelable(false);
        builder.setTitle(getResources().getString(R.string.mantra_install))
                .setMessage(getResources().getString(R.string.mantra_rd_service))
                .setPositiveButton(getResources().getString(R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        final String appPackageName = "com.mantra.rdservice"; // getPackageName() from Context or Activity object
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                })
                .show();
    }

    private void mantraMessage() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setCancelable(false);
        builder.setTitle(getResources().getString(R.string.mantra_client_management_install))
                .setMessage(getResources().getString(R.string.mantra))
                .setPositiveButton(getResources().getString(R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        final String appPackageName = "com.mantra.clientmanagement"; // getPackageName() from Context or Activity object
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                })
                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    try {
                        if (data != null) {
                            String result = data.getStringExtra("DEVICE_INFO");
                            String rdService = data.getStringExtra("RD_SERVICE_INFO");
                            String display = "";
                            if (rdService != null) {
                                //hideLoader();
                                display = "RD Service Info :\n" + rdService + "\n\n";
                                Toast.makeText(this, "Your device is ready for use", Toast.LENGTH_SHORT).show();
                                finish();
                                Intent intent = new Intent(SettingsActivity.this, SettingsActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(intent);
                            }
                            if (result != null) {
//                                Toast.makeText ( DashboardActivity.this, "result"+""+result, Toast.LENGTH_SHORT ).show ();
                            }
                        }
                    } catch (Exception e) {
                        if (loadingView != null) {
                            loadingView.hide();
                        }
                    }
                }
                break;
            case 2:
                if (resultCode == RESULT_OK) {

                    try {
                        if (data != null) {
                            String result = data.getStringExtra("DEVICE_INFO");
                            String rdService = data.getStringExtra("RD_SERVICE_INFO");
                            String display = "";
                            if (rdService != null) {
                                display = "RD Service Info :\n" + rdService + "\n\n";
                                Toast.makeText(this, "Your device is ready for use", Toast.LENGTH_SHORT).show();
                                finish();
                                Intent intent = new Intent(SettingsActivity.this, SettingsActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(intent);

                            }
                            if (result != null) {
//                                Toast.makeText ( DashboardActivity.this, "Decive info check", Toast.LENGTH_SHORT ).show ();
                            }
                        }
                    } catch (Exception e) {
                        if (loadingView != null) {
                            loadingView.hide();
                        }
                    }
                }


                break;
            case 3:
                    String response;
                    if (data.hasExtra("DeviceConnectionDtls")) {
                        response = data.getStringExtra("DeviceConnectionDtls");
                        String[] error_dtls = response.split("\\|");
                        String errorMsg = error_dtls[0];
                        String errorMsg2 = error_dtls[1];

                        Toast.makeText(SettingsActivity.this, errorMsg+"  "+errorMsg2, Toast.LENGTH_SHORT).show();

                    }


        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        Toast.makeText(this, "on resume", Toast.LENGTH_SHORT).show();

//        if(from_BT) {
//
//            if (MainActivity.selected_btdevice != null) {
//                bt_device_name.setVisibility(View.VISIBLE);
//                bt_device_name.setText("Connected Device : " + MainActivity.selected_btdevice.getName());
//
//            } else {
//                bt_device_name.setVisibility(View.INVISIBLE);
//            }
//        }else{
//
//
//            if(sharePreferenceClass.getBluetoothInstance()!=null) {
//                //  blueToothDevice = sharePreferenceClass.getBluetoothInstance();
//                try{
//                    // if(blueToothDevice.getName()==null) {
//                    // Grab the BlueToothAdapter. The first line of most bluetooth programs.
//                    bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
//
//                    // if the BluetoothAdapter.getDefaultAdapter(); returns null then the
//                    // device does not have bluetooth hardware. Currently the emulator
//                    // does not support bluetooth so this will this condition will be true.
//                    // i.e. This code only runs on a hardware device an not on the emulator.
//                    if (bluetoothAdapter == null) {
//                        Log.e(this.toString(), "Bluetooth Not Available.");
//                        return;
//                    }
//                    String address = sharePreferenceClass.getBluetoothInstance();
//                    // This will find the remote device given the bluetooth hardware
//                    // address.
//                    // @todo: Change the address to the your device address
//                    if(address!=null) {
//                        blueToothDevice = bluetoothAdapter.getRemoteDevice(address);
//
//                        for (Integer port = 1; port <= 3; port++) {
//                            simpleComm(Integer.valueOf(port));
//                        }
//
//                        sharePreferenceClass.setBluetoothInstance(blueToothDevice);
//                    }
//                    // }
//                }catch (Exception e){
//                    e.printStackTrace();
//                }
//
//                bt_device_name.setVisibility(View.VISIBLE);
//                bt_device_name.setText("Connected Device : " + blueToothDevice.getName());
//
//            } else {
//                bt_device_name.setVisibility(View.INVISIBLE);
//            }
//        }

        if (from_RD) {
            if (deviceSpinner.getSelectedItemPosition() == 2) {
                devicecheck();
            } else if (deviceSpinner.getSelectedItemPosition() == 1) {
                devicecheck();
            }

            updateDeviceInfo();
        } else {
            updateDeviceInfo();
        }
    }

    private void updateDeviceInfo() {

        HashMap<String, String> deviceInfo = session.getDeviceInfoSession();

        try {
            if (deviceInfo.get("manufacturername").trim().equalsIgnoreCase(mantradeviceid)) {
                device_name.setVisibility(View.VISIBLE);
                device_name.setText("Connected Device : " + mantradeviceid);

            } else if (deviceInfo.get("manufacturername").trim().equalsIgnoreCase(morphoe2device)) {
                device_name.setText("Connected Device : " + morphodeviceid);
            } else {

                device_name.setVisibility(View.GONE);

                //Toast.makeText(getActivity(), getResources().getString(R.string.recommended_device), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception exc) {
            exc.printStackTrace();
        }

    }


}