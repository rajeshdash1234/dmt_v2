package com.orbo.isu_customer.activity.report;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ReportScreenRequest {

    /**
     * request body parameters for login Api
     */
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;



    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}
