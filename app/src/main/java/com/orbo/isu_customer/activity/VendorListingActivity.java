package com.orbo.isu_customer.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.isu.dmt.R;


public class VendorListingActivity extends AppCompatActivity {

    ListView list;

    String animalList[] = {"Dr Lal PathLabs Diagnostic center,Bhubaneswar, Odisha, 094394 00019, Open. Closes 7PM","Dr Abc PathLabs Diagnostic center,Bhubaneswar, Odisha, 094394 00019, Open ⋅ Closes 7PM"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_vendor_listing);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Vendor Listing");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        list = findViewById(R.id.list);

        ArrayAdapter adapter = new ArrayAdapter<String>(this, R.layout.listing,R.id.list, animalList);
        list.setAdapter(adapter);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(getApplicationContext(),VendorDetailsActivity.class));
                finish();
            }
        });

    }
}
