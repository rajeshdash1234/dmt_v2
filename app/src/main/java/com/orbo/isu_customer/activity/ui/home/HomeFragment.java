package com.orbo.isu_customer.activity.ui.home;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaCas;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.isu.dmt.R;
import com.orbo.isu_customer.BBPS.BbpsActivity;
import com.orbo.isu_customer.activity.SessionManager;
import com.orbo.isu_customer.activity.cashcollection.CashCollectionActivity;
import com.orbo.isu_customer.activity.insurance.Insurance;
import com.orbo.isu_customer.datacard.DataCardActivity;
import com.orbo.isu_customer.dth.DthActivity;
import com.orbo.isu_customer.postpaid.PostPaidActivity;
import com.orbo.isu_customer.prepaid.PrepaidActivity;
import com.orbo.isu_customer.special.SpecialActivity;
import com.orbo.isu_customer.utility.Util;
import com.orbo.isu_customer.utility.UtilityActivity;

import java.text.DecimalFormat;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;


public class HomeFragment extends Fragment {

    LinearLayout transfer,trans_layout,main_layout,insurance,recharge,loan;

    TextView userName,amountTxt,amount_paisa;
    CircleImageView profile_image;
    SessionManager session;
    String brandName,amout;
    CardView card_one, card_two, card_three,card5,card6;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        userName = root.findViewById(R.id.userName);
        amountTxt = root.findViewById(R.id.amountTxt);
        amount_paisa = root.findViewById(R.id.amount_paisa);
        card_one = root.findViewById(R.id.card_one);
        card_two = root.findViewById(R.id.card_two);
        card_three = root.findViewById(R.id.card_three);
        card5 = root.findViewById(R.id.card5);
        card6 = root.findViewById(R.id.card6);



        session = new SessionManager(getActivity());
        HashMap<String, String> _user = session.getUserSession();
        brandName = _user.get(SessionManager.mposNumber);
        String profileName = _user.get(SessionManager.profileName);
        profileName = profileName.substring(0,1).toUpperCase() + profileName.substring(1).toLowerCase();

        String _userBalance = _user.get(session.userBalance);
        String bal = Util.amountFormat(_userBalance);//_userBalance

        amout ="₹ "+ bal;

        String laststring = amout.substring(Math.max(amout.length() - 2, 0));
        String fstString = amout.substring(0, amout.length() - 2);


        //String[] arrOfStr = amout.split(".");
       // String str1 = arrOfStr[0];
       // String str2 = arrOfStr[1];

        userName.setText(profileName);
        amountTxt.setText(fstString);
        amount_paisa.setText(laststring);




//        transfer = root.findViewById(R.id.transfer);
//        main_layout = root.findViewById(R.id.main_layout);
//        trans_layout = root.findViewById(R.id.trans_layout);
//
//        transfer.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                FragmentManager fragmentManager = getFragmentManager();
//                FragmentTransaction tx = fragmentManager.beginTransaction();
//                tx.replace( R.id.trans_layout, new TransferFragment() ).addToBackStack( "transfer" ).commit();
//
//            }
//        });

        card_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // custom dialog
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setContentView(R.layout.customdialogrecharge);

                LinearLayout prepaid = dialog.findViewById(R.id.prepaid);
                LinearLayout dth = dialog.findViewById(R.id.dth);
                LinearLayout datacard = dialog.findViewById(R.id.datacard);
                LinearLayout postpaid = dialog.findViewById(R.id.postpaid);
                LinearLayout special = dialog.findViewById(R.id.special);

                dth.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        startActivity(new Intent(getActivity(), DthActivity.class));
                    }
                });

                prepaid.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        startActivity(new Intent(getActivity(), UtilityActivity.class));
                    }
                });

                datacard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        startActivity(new Intent(getActivity(), DataCardActivity.class));
                    }
                });

                postpaid.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        startActivity(new Intent(getActivity(), PostPaidActivity.class));
                    }
                });


                special.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        startActivity(new Intent(getActivity(), SpecialActivity.class));
                    }
                });


                dialog.show();
            }
        });

        card_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // custom dialog
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setContentView(R.layout.customdialogutility);

                LinearLayout utilities = dialog.findViewById(R.id.utilities);
                LinearLayout bbps = dialog.findViewById(R.id.bbps);


                utilities.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        startActivity(new Intent(getActivity(), PrepaidActivity.class));
                    }
                });


                bbps.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        startActivity(new Intent(getActivity(), BbpsActivity.class));
                    }
                });


                dialog.show();
            }
        });
        card_three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setContentView(R.layout.customdialogbbps);

                //LinearLayout utilities = dialog.findViewById(R.id.utilities);
                LinearLayout bbps = dialog.findViewById(R.id.bbps);
                bbps.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        startActivity(new Intent(getActivity(), BbpsActivity.class));
                    }
                });


                dialog.show();
            }
        });
        card5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CashCollectionActivity.class);
                startActivity(intent);

            }
        });
        card6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), Insurance.class);
                startActivity(intent);
            }
        });

        return root;
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
    }
}