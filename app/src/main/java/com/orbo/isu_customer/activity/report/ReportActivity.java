package com.orbo.isu_customer.activity.report;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.isu.dmt.R;
import com.orbo.isu_customer.activity.SessionManager;
import com.orbo.isu_customer.utility.Util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;

public class ReportActivity extends AppCompatActivity implements ReportContract.View, DatePickerDialog.OnDateSetListener {

    private RecyclerView reportRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private ReportPresenter mActionsListener;
    ArrayList<ReportModel> reportResponseArrayList ;
    LinearLayout dateLayout,detailsLayout;

    private ReportRecyclerViewAdapter reportRecyclerviewAdapter;
    TextView noData,totalreport,amount;
    TextView chooseDateRange;
    private static final String TAG = ReportActivity.class.getSimpleName ();

    // LoadingView loadingView;
    // Session session;
    // SearchView searchView;

    private boolean ascending = true;

    private ShimmerFrameLayout mm;

    SessionManager session;
    String _token, _admin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        setToolbar();

        //Toast.makeText(this, "hi", Toast.LENGTH_SHORT).show();

        //session = new Session(ReportActivity.this);

        noData = findViewById ( R.id.noData );
        amount = findViewById ( R.id.amount );
        totalreport = findViewById ( R.id.totalreport );
        detailsLayout = findViewById ( R.id.detailsLayout );
        chooseDateRange = findViewById ( R.id.chooseDateRange );
        dateLayout = findViewById ( R.id.dateLayout );
        reportRecyclerView = findViewById ( R.id.reportRecyclerView );


        mLayoutManager = new LinearLayoutManager(ReportActivity.this);
        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);

        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        reportRecyclerView.setHasFixedSize ( true );
        reportRecyclerView.setLayoutManager(mLayoutManager);

        mActionsListener = new ReportPresenter ( ReportActivity.this );


        session = new SessionManager(getApplicationContext());
        HashMap<String, String> user = session.getUserSession();
        String _userName = user.get(session.userName);
        String _userType = user.get(session.userType);
        String _userBalance = user.get(session.userBalance);
        String _adminName = user.get(session.adminName);
        String _mposNumber = user.get(session.mposNumber);


        HashMap<String, String> _user = session.getUserDetails();
        _token = _user.get(SessionManager.KEY_TOKEN);
        _admin = _user.get(SessionManager.KEY_ADMIN);

        mm = findViewById(R.id.shimmer_view_container1);
        mm.setVisibility(View.GONE);

        dateLayout.setOnClickListener(new View.OnClickListener () {
            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(ReportActivity.this, now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));

                dpd.show(getFragmentManager(), "Datepickerdialog");
                dpd.setMaxDate ( Calendar.getInstance () );
                dpd.setAutoHighlight ( true );
            }
        });
    }



    @Override
    public void reportsReady(ArrayList<ReportModel> reportModelArrayList, String totalAmount) {
        reportResponseArrayList = reportModelArrayList;
        amount.setText(getResources().getString(R.string.toatlamountacitvity)+totalAmount);

    }

    @Override
    public void showReports() {
        if (reportResponseArrayList!=null && reportResponseArrayList.size() > 0) {
            reportRecyclerView.setVisibility(View.VISIBLE);
            detailsLayout.setVisibility(View.VISIBLE);
            noData.setVisibility(View.GONE);
            reportRecyclerviewAdapter = new ReportRecyclerViewAdapter(reportResponseArrayList);
            reportRecyclerView.setAdapter(reportRecyclerviewAdapter);
            totalreport.setText("Found "+reportResponseArrayList.size()+"/"+reportResponseArrayList.size()+" Entries");

            mm.stopShimmerAnimation();
            mm.setVisibility(View.GONE);
        }else{

            mm.stopShimmerAnimation();
            mm.setVisibility(View.GONE);

            reportRecyclerView.setVisibility(View.GONE);
            noData.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void showLoader() {
    }

    @Override
    public void hideLoader() {
    }

    @Override
    public void emptyDates() {
        Toast.makeText(ReportActivity.this,getResources().getString(R.string.empty_date),Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void sortData(boolean asc)
    {
        //SORT ARRAY ASCENDING AND DESCENDING
        if (asc) {
            Collections.reverse(reportResponseArrayList);
        }
        else {
            Collections.reverse(reportResponseArrayList);
        }
        //ADAPTER
        reportRecyclerviewAdapter = new ReportRecyclerViewAdapter(reportResponseArrayList);
        reportRecyclerView.setAdapter(reportRecyclerviewAdapter);
    }

    private void setToolbar() {
        // Inflate the layout for this fragment
        Toolbar mToolbar = findViewById ( R.id.toolbar );
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.report));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        String fromdate = year + "-" + (monthOfYear+1) + "-" + dayOfMonth;
        String todate = yearEnd + "-" + (monthOfYearEnd+1)+ "-" + dayOfMonthEnd;
        String date = dayOfMonth+"/"+(monthOfYear+1)+"/"+year+" To "+dayOfMonthEnd+"/"+(monthOfYearEnd+1)+"/"+yearEnd;

        chooseDateRange.setText ( date );

        if(chooseDateRange.getText () !=null && !chooseDateRange.getText ().toString ().matches ( "" ) && !chooseDateRange.getText ().toString ().trim ().matches ( "Choose Date Range for Reports" )) {

            mm.startShimmerAnimation();
            mm.setVisibility(View.VISIBLE);

            if (!(Util.getDateDiff ( new SimpleDateFormat( "yyyy-MM-dd" ),fromdate, todate ) > 10)) {
                if (Util.compareDates ( fromdate, todate ) == "1") {
                    noData.setVisibility ( View.GONE );
                    reportRecyclerView.setVisibility ( View.VISIBLE );
                    mActionsListener.loadReports ( fromdate, Util.getNextDate (todate), _token );
                } else if (Util.compareDates ( fromdate, todate ) == "2") {
                    noData.setVisibility ( View.VISIBLE );
                    reportRecyclerView.setVisibility ( View.GONE );
                    Toast.makeText ( ReportActivity.this, getResources().getString(R.string.appropriate_dates), Toast.LENGTH_SHORT ).show ();
                } else if(Util.compareDates ( fromdate,todate) == "3" ) {
                    noData.setVisibility ( View.GONE );
                    reportRecyclerView.setVisibility ( View.VISIBLE );
                    mActionsListener.loadReports ( fromdate,Util.getNextDate (todate), _token );
                }
            } else {
                Toast.makeText ( ReportActivity.this, getResources().getString(R.string.dates_limit), Toast.LENGTH_LONG ).show ();
                noData.setVisibility ( View.VISIBLE );
                reportRecyclerView.setVisibility ( View.GONE );
            }
        }else{
            noData.setVisibility ( View.VISIBLE );
            reportRecyclerView.setVisibility ( View.GONE );
        }
    }

}

