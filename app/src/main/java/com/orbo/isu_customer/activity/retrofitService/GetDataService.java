package com.orbo.isu_customer.activity.retrofitService;

import com.orbo.isu_customer.activity.login.User;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface GetDataService {

    @GET("/generate/v1")
    Call<ResponseBody> getV1();

    @GET("/generate/v2")
    Call<ResponseBody> getV2();

    @GET("/generate/v4")
    Call<ResponseBody> getV5();
}
