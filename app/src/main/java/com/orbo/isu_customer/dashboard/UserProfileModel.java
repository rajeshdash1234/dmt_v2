package com.orbo.isu_customer.dashboard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserProfileModel {
    @SerializedName("userBalance")
    @Expose
    private String userBalance;

    @SerializedName("mobileNumber")
    @Expose
    private String mobileNumber;

    @SerializedName("shopName")
    @Expose
    private String shopName;

    public UserProfileModel() {
    }

    public String getUserBalance() {
        return userBalance;
    }

    public void setUserBalance(String userBalance) {
        this.userBalance = userBalance;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }
}
