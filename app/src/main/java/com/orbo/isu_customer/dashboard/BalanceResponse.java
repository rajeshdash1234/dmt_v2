package com.orbo.isu_customer.dashboard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BalanceResponse {
    @SerializedName("userInfo")
    @Expose
    private UserInfoModel userInfoModel;


    public BalanceResponse() {
    }

    public UserInfoModel getUserInfoModel() {
        return userInfoModel;
    }

    public void setUserInfoModel(UserInfoModel userInfoModel) {
        this.userInfoModel = userInfoModel;
    }

}
