package com.orbo.isu_customer.utility;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.isu.dmt.R;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

public class UtilityActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    SearchableSpinner utility_select_operator,utility_select_Spinner;

    String[] operatorUtility = {"MTNL Delhi LandLine Utility","BSNL Landline Utility","Reliance Energy(Mumbai) Utility","MSEDC Limited Utility",
            "Torrent Power Utility","Mahanagar Gas Limited Utility","Tata AIG Life Utility","ICICI Pru Life Utility","BSES Rajdhani","BSES Yamuna",
            "North Delhi Power Limited","Brihan Mumbai Electric Supply and Transport Undertaking","Rajasthan Vidyut Vitran Nigam Limited",
            "Soouthern power Distribution Company Ltd of Andhra Pradesh( APSPDCL)","Bangalore Electricity Supply Company","Madhya Pradesh Madhya Kshetra Vidyut Vitaran Company Limited - Bhopal",
            "Noida Power Company Limited","Madhya Pradesh Paschim Kshetra Vidyut Vitaran Indor","Calcutta Electricity Supply Ltd","Chhattisgarh State Electricity Board","India Power Corporation Limited",
            "Jamshedpur Utilities and Services Company Limited","IGL (Indraprast Gas Limited)","Gujarat Gas company Limited","ADANI GAS",
            "Tikona Postpaid","Hathway Broadband Retail"};

    String[] serviceType = {"Landline Individual","Landline Corporate"};

    LinearLayout spinner_linear;
    Button utility_button,utility_fetch_bill_button;

    EditText utility_mobileno,utility_mobilenoo,utility_mobilenooo;
    TextView utility_amount;
    String mobilenoString,amountString,operatorString,rechargetypeString,stdCodeString,pincodeString,accountNoString,authString,latitudeString, circleCodeString,longitudeString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_utility);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Utility");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        utility_select_operator = findViewById(R.id.utility_select_operator);
        utility_select_Spinner =  findViewById(R.id.utility_select_Spinner);

        utility_mobileno = findViewById(R.id.utility_mobileno);
        utility_mobilenoo = findViewById(R.id.utility_mobilenoo);
        utility_mobilenooo = findViewById(R.id.utility_mobilenooo);
        utility_amount = findViewById(R.id.utility_amount);
        spinner_linear = findViewById(R.id.spinner_linear);

        utility_fetch_bill_button = findViewById(R.id.utility_fetch_bill_button);
        utility_button = findViewById(R.id.utility_button);

        utility_fetch_bill_button.setOnClickListener(this);
        utility_button.setOnClickListener(this);

        utility_mobileno.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 0){
                    mobilenoString = null;
                }else {
                    mobilenoString = utility_mobileno.getText().toString();
                }
            }
        });

        utility_mobilenoo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 0){
                    accountNoString = null;
                }else {
                    accountNoString = utility_mobilenoo.getText().toString();
                }
            }
        });

        utility_mobilenooo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 0){
                    authString = null;
                }else {
                    authString = utility_mobilenooo.getText().toString();
                }
            }
        });

        //Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,operatorUtility);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        utility_select_operator.setAdapter(aa);


        ArrayAdapter serviceTypeAdapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item,serviceType);
        serviceTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        utility_select_Spinner.setAdapter(serviceTypeAdapter);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.utility_fetch_bill_button:
                //rechargeUtilityFetchBill();
                break;
            case R.id.utility_button:
                // do your code
                //rechargeUtilitySubmit();
                //Toast.makeText(getApplicationContext(),"Success" , Toast.LENGTH_LONG).show();
                break;
            default:
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        utility_mobilenoo.setVisibility(View.GONE);
        utility_mobilenooo.setVisibility(View.GONE);
        spinner_linear.setVisibility(View.GONE);

        operatorString = operatorUtility[i];
        if (i == 0) {
            utility_mobilenoo.setVisibility(View.VISIBLE);
            utility_mobileno.setHint("Phone number");
            utility_mobilenoo.setHint("Customer Account Number");

        } else if (i == 1) {

            utility_mobilenoo.setVisibility(View.VISIBLE);
            spinner_linear.setVisibility(View.VISIBLE);

            utility_mobileno.setHint("Phone number");
            utility_mobilenoo.setHint("Account Number");
        } else if (i == 2) {
            utility_mobilenoo.setVisibility(View.VISIBLE);

            utility_mobileno.setHint("Customer number");
            utility_mobilenoo.setHint("Cycle Number");

        } else if (i == 3) {
            utility_mobilenoo.setVisibility(View.VISIBLE);
            utility_mobilenooo.setVisibility(View.VISIBLE);

            utility_mobileno.setHint("Customer number");
            utility_mobilenoo.setHint("Billing Unit");
            utility_mobilenooo.setHint("Processing Cycle");


        } else if (i == 4) {
            utility_mobilenoo.setVisibility(View.VISIBLE);

            utility_mobileno.setHint("Service Number");
            utility_mobilenoo.setHint("City");

        } else if (i == 5) {
            utility_mobilenoo.setVisibility(View.VISIBLE);

            utility_mobileno.setHint("Number");
            utility_mobilenoo.setHint("Account Number");

        } else if (i == 6 || i == 7) {
            utility_mobilenoo.setVisibility(View.VISIBLE);

            utility_mobileno.setHint("Policy Number");
            utility_mobilenoo.setHint("Date of Birth");

        } else if (i == 8 || i == 9 || i == 10 || i == 13 || i == 14 || i == 15 || i == 16 || i == 17 || i == 20 || i == 22 || i == 25 || i == 26) {
            utility_mobileno.setHint("Customer number");
        } else if (i == 11 || i == 23) {
            utility_mobileno.setHint("Service Number");
        } else if (i == 12) {
            utility_mobileno.setHint("K Number");
        } else if (i == 18) {
            utility_mobileno.setHint("Consumer ID");
        } else if (i == 19 || i == 21) {
            utility_mobileno.setHint("Business Partener Number");
        } else if (i == 24) {
            utility_mobileno.setHint("Customer ID");
        }


    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
