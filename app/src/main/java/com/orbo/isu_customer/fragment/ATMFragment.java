package com.orbo.isu_customer.fragment;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.finopaytech.finosdk.activity.DeviceSettingActivity;
import com.finopaytech.finosdk.activity.MainTransactionActivity;
import com.finopaytech.finosdk.encryption.AES_BC;
import com.finopaytech.finosdk.helpers.Utils;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.isu.dmt.R;
import com.orbo.isu_customer.activity.SessionManager;
import com.orbo.isu_customer.dashboard.UserInfoModel;
import com.orbo.isu_customer.matmtransaction.MATMTransactionStatusActivity;
import com.orbo.isu_customer.matmtransaction.MicroAtmReportActivity;
import com.orbo.isu_customer.microatm.MicroAtmContract;
import com.orbo.isu_customer.microatm.MicroAtmPresenter;
import com.orbo.isu_customer.microatm.MicroAtmRequestModel;
import com.orbo.isu_customer.microatm.MicroAtmResponse;
import com.orbo.isu_customer.microatm.MicroAtmTransactionModel;
import com.orbo.isu_customer.utility.Constants;
import com.orbo.isu_customer.utility.SharePreferenceClass;
import com.orbo.isu_customer.utility.Util;
import com.orbo.isu_customer.utills.Currency;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;


public class ATMFragment extends BottomSheetDialogFragment implements MicroAtmContract.View {

    String strTransType="";
    RadioGroup rgTransactionType;
    RadioButton rbCashWithdrawal;
    RadioButton rbBalanceEnquiry;
    Button proceedBtn;
    ProgressBar loadingView;
    EditText amount_txt;
    TextView balanceTxt;
    ImageView transactionReport;
    MicroAtmTransactionModel microAtmTransactionModel;
    MicroAtmPresenter microAtmPresenter;
    MicroAtmRequestModel microAtmRequestModel;
    SessionManager session;
    String token;
    Button pairBtn;

    String encData;
    String authentication;
    Activity activity;
    TextView amouttxt;
    SharedPreferences sharedpreferences;



    public ATMFragment(Activity activity) {
        // Required empty public constructor
        this.activity = activity;
    }


    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) dialogInterface;
                setupFullHeight(bottomSheetDialog);
            }
        });
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                System.out.println(">>>>>"+"Hello Hi...");
            }
        });

        return dialog;
    }


    private void setupFullHeight(BottomSheetDialog bottomSheetDialog) {
        FrameLayout bottomSheet = (FrameLayout) bottomSheetDialog.findViewById(R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        ViewGroup.LayoutParams layoutParams = bottomSheet.getLayoutParams();

        int windowHeight = getWindowHeight();

        if (layoutParams != null) {
            layoutParams.height = windowHeight;
        }

        bottomSheet.setLayoutParams(layoutParams);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    private int getWindowHeight() {
        // Calculate window height for fullscreen use
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) getContext()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        /*sharedpreferences = getActivity().getSharedPreferences("language", Context.MODE_PRIVATE);
        String lang  = sharedpreferences.getString("lang","en");
        Locale locale = new Locale(lang);

        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;

        getActivity().getBaseContext().getResources().updateConfiguration(config, getActivity().getBaseContext().getResources().getDisplayMetrics());
*/
        View view = inflater.inflate(R.layout.fragment_atm, container, false);

        ImageView imageView = view.findViewById(R.id.atm_nav);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        final LinearLayout amount_ll = view.findViewById(R.id.amount_ll);
         amount_txt = view.findViewById(R.id.amount_txt);

         rgTransactionType = view.findViewById(R.id.rg_trans_type);
         rbCashWithdrawal = view.findViewById(R.id.rb_cw);
         rbBalanceEnquiry = view.findViewById(R.id.rb_be);
         proceedBtn = view.findViewById(R.id.proceedBtn);
         loadingView = view.findViewById(R.id.loadingView);
        transactionReport = view.findViewById(R.id.transactionReport);
        pairBtn = view.findViewById(R.id.pairBtn);
        balanceTxt = view.findViewById(R.id.balanceTxt);
        amouttxt = view.findViewById(R.id.amouttxt);

        microAtmPresenter = new MicroAtmPresenter(this);


        session = new SessionManager(getActivity());
        HashMap<String, String> _user = session.getUserDetails();
        token = _user.get(SessionManager.KEY_TOKEN);

        HashMap<String, String> _userData = session.getUserSession();


        String bal = Util.amountFormat(_userData.get(SessionManager.userBalance));//_userBalance

        //amout ="₹ "+ bal;
        balanceTxt.setText("Balance: ₹ "+bal);
        //text_balance.setText("Balance: ₹  "+bal);

        transactionReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MicroAtmReportActivity.class));

                // startActivity(new Intent(getActivity(), MatmSettlementActivity.class));

            }
        });
        pairBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent settingIntent = new Intent(activity, DeviceSettingActivity.class);
                settingIntent.putExtra("IS_PAIR_DEVICE",true);
                activity.startActivityForResult(settingIntent,3);

            }
        });


        rgTransactionType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId)
                {

                    case R.id.rb_cw:
                        amount_txt.setClickable(true);
                        amount_txt.setHint("Amount");
                        amount_ll.setVisibility(View.VISIBLE);
                        strTransType = "Cash Withdrawal";
                        amount_txt.setText("");
                        amount_txt.setEnabled(true);
                        amount_txt.setInputType(InputType.TYPE_CLASS_NUMBER);
                        break;
                    case R.id.rb_be:
                        strTransType = "Balance Enquiry";
                        //amount_txt.setVisibility(View.GONE);
                        amount_ll.setVisibility(View.GONE);
                        break;
                }
            }
        });

        proceedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingView.setVisibility(View.VISIBLE);

                if (validate()) {
                    if (rbCashWithdrawal.isChecked()) {
                        apiCalling();
                    } else if (rbBalanceEnquiry.isChecked()) {
                        balanceEnquiryApiCalling();
                    }
                }
            else{
                loadingView.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "Validation Error", Toast.LENGTH_SHORT).show();
             }

            }
        });
        amount_txt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().isEmpty()){
                    amouttxt.setText("");

                }else {

                    String str = Currency.convertToIndianCurrency(s.toString());
                    amouttxt.setText(str);
                }


            }
        });


        return view;

    }

    @Override
    public void dismiss() {
        super.dismiss();
    }



    private boolean validate() {
        if ((!rbCashWithdrawal.isChecked()) && (!rbBalanceEnquiry.isChecked())) {
            showOneBtnDialog(getActivity(), "Info", "Please select Transaction Type!", false);
            return false;
        }
        if(rbCashWithdrawal.isChecked()) {
            if (amount_txt.getText().toString().equals("")) {
                String msg = "";
                if (rbCashWithdrawal.isChecked()) {
                    msg = "Amount";
                }

                showOneBtnDialog(getActivity(), "Info", "Please enter " + msg + " !", false);
                return false;
            }
        }
        return true;
    }

    //--------------------

    private void showOneBtnDialog(final Context mContext, String title, String msg, boolean cancelable) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(title);
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                hideKeyboard();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(cancelable);
        dialog.show();
    }

    public void hideKeyboard() {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public String getServiceID() {
        String clientRefID = "";

        if (rbCashWithdrawal.isChecked())
            clientRefID = Constants.SERVICE_MICRO_CW;
        if (rbBalanceEnquiry.isChecked())
            clientRefID = Constants.SERVICE_MICRO_BE;

        return clientRefID;
    }


    //-----------------------

    public void apiCalling()
    {
        microAtmRequestModel = new MicroAtmRequestModel(amount_txt.getText().toString(),getServiceID(),"mobile");
        microAtmPresenter.performRequestData(token, microAtmRequestModel);

    }

    public void balanceEnquiryApiCalling()
    {
        microAtmRequestModel = new MicroAtmRequestModel("0",getServiceID(),"mobile");
        microAtmPresenter.performRequestData(token, microAtmRequestModel);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(data != null & resultCode == RESULT_OK) {
            microAtmTransactionModel = new MicroAtmTransactionModel();
            if (requestCode == 1) {
                String response;
                if (data.hasExtra("ClientResponse")) {
                   // hideLoader();
                    response = data.getStringExtra("ClientResponse");
                    if (!response.equalsIgnoreCase("")) {
                        try {
                            String strDecryptResponse = AES_BC.getInstance().decryptDecode(Utils.replaceNewLine(response), Constants.CLIENT_REQUEST_ENCRYPTION_KEY);
                            if (rbCashWithdrawal.isChecked()) {
                                JSONObject jsonObject = new JSONObject(strDecryptResponse);
                                microAtmTransactionModel.setTxnStatus(jsonObject.getString("TxnStatus"));
                                microAtmTransactionModel.setTxnAmt(jsonObject.getString("TxnAmt"));
                                microAtmTransactionModel.setRnr(jsonObject.getString("RRN"));
                                microAtmTransactionModel.setCardNumber(jsonObject.getString("CardNumber"));
                                microAtmTransactionModel.setAvailableBalance(jsonObject.getString("AvailableBalance"));
                                microAtmTransactionModel.setTransactionDateandTime(jsonObject.getString("TransactionDatetime"));
                                microAtmTransactionModel.setTerminalId(jsonObject.getString("TerminalID"));
                                microAtmTransactionModel.setType(getServiceID());
                            } else if (rbBalanceEnquiry.isChecked()) {
                                JSONObject jsonObject = new JSONObject(strDecryptResponse);
                                microAtmTransactionModel.setBalanceEnquiryStatus(jsonObject.getString("BalanceEnquiryStatus"));
                                microAtmTransactionModel.setRnr(jsonObject.getString("RRN"));
                                microAtmTransactionModel.setCardNumber(jsonObject.getString("CardNumber"));
                                microAtmTransactionModel.setAvailableBalance(jsonObject.getString("AvailableBalance"));
                                microAtmTransactionModel.setTransactionDateandTime(jsonObject.getString("TransactionDatetime"));
                                microAtmTransactionModel.setAccountNo(jsonObject.getString("AccountNo"));
                                microAtmTransactionModel.setTerminalId(jsonObject.getString("TerminalID"));
                                microAtmTransactionModel.setType(getServiceID());
                            }

                        } catch (Exception e) {
                            microAtmTransactionModel = null;
                            Log.v("test", "error : " + e);
                        }
                    }
                } else if (data.hasExtra("ErrorDtls")) {
                    //hideLoader();
                    response = data.getStringExtra("ErrorDtls");
                    if (!response.equalsIgnoreCase("")) {
                        try {
                            String[] error_dtls = response.split("\\|");
                            String errorMsg = error_dtls[0];
                            microAtmTransactionModel.setErrormsg(errorMsg);
                        } catch (Exception exp) {
                            microAtmTransactionModel = null;
                            Log.v("test", "Error : " + exp.toString());
                        }
                    }
                } else {
                    microAtmTransactionModel = null;
                }
            }
            if (requestCode == 3) {
                String response;
                if (data.hasExtra("DeviceConnectionDtls")) {
                    response = data.getStringExtra("DeviceConnectionDtls");
                    String[] error_dtls = response.split("\\|");
                    String errorMsg = error_dtls[0];
                    String errorMsg2 = error_dtls[1];
                    microAtmTransactionModel.setErrormsg(errorMsg2);
                    microAtmTransactionModel.setStatusError(errorMsg);

                }
            }

            nextPage();
        }
    }


    public void nextPage(){


        Toast.makeText(getActivity(), "Go to Next Page", Toast.LENGTH_SHORT).show();

        Intent intentAtm = new Intent(getActivity(), MATMTransactionStatusActivity.class);
        intentAtm.putExtra(Constants.MICRO_ATM_TRANSACTION_STATUS_KEY,microAtmTransactionModel);
        startActivity(intentAtm);

    }

    @Override
    public void checkRequestCode(String status, String message, MicroAtmResponse microAtmResponse) {
        if(status!= null && !status.matches("")) {
            authentication = microAtmResponse.getAuthentication();
            encData = microAtmResponse.getEncData();

            Intent intent = new Intent(activity, MainTransactionActivity.class);
            intent.putExtra("RequestData", encData);
            intent.putExtra("HeaderData", authentication);
            intent.putExtra("ReturnTime", 5);// Application return time in second
            activity.startActivityForResult(intent, 1);
        }else{
           // Util.showAlert(this,getResources().getString(R.string.fail_error),message);
            Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();

        }

    }

    @Override
    public void showFeature(ArrayList<UserInfoModel.userFeature> userFeatures) {

    }

    @Override
    public void checkEmptyFields() {

    }

    @Override
    public void showLoader() {

    }

    @Override
    public void hideLoader() {

    }
}
