package com.orbo.isu_customer.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.snackbar.Snackbar;
import com.isu.dmt.R;
import com.moos.library.HorizontalProgressView;
import com.orbo.isu_customer.activity.SessionManager;
import com.orbo.isu_customer.activity.Settings.SettingsActivity;
import com.orbo.isu_customer.activity.report.ReportActivity;
import com.orbo.isu_customer.activity.transactionstatus.TransactionStatusActivity;
import com.orbo.isu_customer.activity.transactionstatus.TransactionStatusModel;
import com.orbo.isu_customer.balance.BalancePresenter;
import com.orbo.isu_customer.balanceenquiry.BalanceEnquiryContract;
import com.orbo.isu_customer.balanceenquiry.BalanceEnquiryPresenter;
import com.orbo.isu_customer.balanceenquiry.BalanceEnquiryRequestModel;
import com.orbo.isu_customer.balanceenquiry.BalanceEnquiryResponse;
import com.orbo.isu_customer.bankspinner.BankNameListActivity;
import com.orbo.isu_customer.bankspinner.BankNameModel;
import com.orbo.isu_customer.cashwithdrawal.CashWithDrawalContract;
import com.orbo.isu_customer.cashwithdrawal.CashWithdrawalPresenter;
import com.orbo.isu_customer.cashwithdrawal.CashWithdrawalRequestModel;
import com.orbo.isu_customer.cashwithdrawal.CashWithdrawalResponse;
import com.orbo.isu_customer.fingerprintmodel.DeviceInfo;
import com.orbo.isu_customer.fingerprintmodel.MorphoDeviceInfo;
import com.orbo.isu_customer.fingerprintmodel.MorphoPidData;
import com.orbo.isu_customer.fingerprintmodel.Opts;
import com.orbo.isu_customer.fingerprintmodel.PidData;
import com.orbo.isu_customer.fingerprintmodel.PidOptions;
import com.orbo.isu_customer.fingerprintmodel.uid.AuthReq;
import com.orbo.isu_customer.fingerprintmodel.uid.AuthRes;
import com.orbo.isu_customer.fingerprintmodel.uid.Meta;
import com.orbo.isu_customer.fingerprintmodel.uid.Uses;
import com.orbo.isu_customer.signer.XMLSigner;
import com.orbo.isu_customer.utility.Constants;
import com.orbo.isu_customer.utility.Util;
import com.orbo.isu_customer.utills.Currency;


import org.json.JSONArray;
import org.json.JSONObject;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class AEPSFragment extends BottomSheetDialogFragment implements BalanceEnquiryContract.View, CashWithDrawalContract.View {
    TextView text_balance,amouttxt;
    //TextView fingerprintStrengthBalance,withdraw_fingerprintStrengthBalance,balanceNote;
    Button cash_withdrawal_btn, balance_enquiry_btn;
    Button submit_cash_balance, submit_balance_enquiry;
    LinearLayout cash_withdrawal_layout, container_balance_enquiry_layout;
    SessionManager session;
    String _token, _admin;
    Spinner cash_withdrawal_select_bank, balance_enquiry_select_bank;
    ArrayList<String> bank_list = new ArrayList<>();
    ArrayList<String> bank_code = new ArrayList<>();
    ImageView imageView, bio_cash_scan_btn, bio_balance_scan_btn;
    RadioButton balance_enquiry_aadhar_number, balance_enquiry_virtual_id;

    RadioButton aadhar_number, virtual_id;
    ArrayList<String> positions = new ArrayList<>();

    EditText cash_withdrawal_bankspinner, balance_enquiry_bankspinner, aadhar_no, checkCashWithdrawalamount, balance_enquiry_aadhar_no, balance_enquiry_mobile_no, mobile_no;

    // LoadingView loadingView;

    String deviceSerialNumber = "0";
    String morphodeviceid = "SAGEM SA";
    String mantradeviceid = "MANTRA";
    String morphoe2device = "Morpho";

    ProgressDialog loadingView;
    ImageView transactionReport;
    Activity activity;

    private HorizontalProgressView withdraw_balanceEnqureyBar;
    //private HorizontalProgressView balanceEnqureyBar, depositBar;

    private PidData pidData;
    private MorphoPidData morphoPidData;
    private Serializer serializer = null;
    private Button settingBtn;
    private String balance_enquiry_nnid = "", cash_withdrawal_nnid = "";

    private BalanceEnquiryRequestModel balanceEnquiryRequestModel;
    private BalancePresenter mActionsListener;
    private BalanceEnquiryPresenter balanceEnquiryPresenter;

//private CashDepositPresenter cashDepositPresenter;

    private CashWithdrawalPresenter cashWithdrawalPresenter;
    private CashWithdrawalRequestModel cashWithdrawalRequestModel;

    SharedPreferences sharedpreferences;

    public AEPSFragment(Activity activity) {
        // Required empty public constructor
        this.activity = activity;
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) dialogInterface;
                setupFullHeight(bottomSheetDialog);
            }
        });

        return dialog;
    }

    private void setupFullHeight(BottomSheetDialog bottomSheetDialog) {
        FrameLayout bottomSheet = (FrameLayout) bottomSheetDialog.findViewById(R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        ViewGroup.LayoutParams layoutParams = bottomSheet.getLayoutParams();

        int windowHeight = getWindowHeight();

        if (layoutParams != null) {
            layoutParams.height = windowHeight;
        }

        bottomSheet.setLayoutParams(layoutParams);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    private int getWindowHeight() {
        // Calculate window height for fullscreen use
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) getContext()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        sharedpreferences = getActivity().getSharedPreferences("language", Context.MODE_PRIVATE);
        String lang  = sharedpreferences.getString("lang","en");
        Locale locale = new Locale(lang);

        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;

        getActivity().getBaseContext().getResources().updateConfiguration(config, getActivity().getBaseContext().getResources().getDisplayMetrics());

        View view = inflater.inflate(R.layout.fragment_ae, container, false);

        imageView = view.findViewById(R.id.aeps_nav);
        text_balance = view.findViewById(R.id.text_balance);
        amouttxt = view.findViewById(R.id.amouttxt);

       // cash_withdrawal_btn = view.findViewById(R.id.cash_withdrawal_btn);
        //balance_enquiry_btn = view.findViewById(R.id.balance_enquiry_btn);

        submit_cash_balance = view.findViewById(R.id.submit_cash_balance);
        submit_balance_enquiry = view.findViewById(R.id.submit_balance_enquiry);

        cash_withdrawal_layout = view.findViewById(R.id.cash_withdrawal_layout);
       // container_balance_enquiry_layout = view.findViewById(R.id.container_balance_enquiry_layout);

        cash_withdrawal_select_bank = view.findViewById(R.id.cash_withdrawal_select_bank);
       // balance_enquiry_select_bank = view.findViewById(R.id.balance_enquiry_select_bank);

       // balance_enquiry_bankspinner = view.findViewById(R.id.balance_enquiry_bankspinner);
        cash_withdrawal_bankspinner = view.findViewById(R.id.cash_withdrawal_bankspinner);

        bio_cash_scan_btn = view.findViewById(R.id.bio_cash_scan_btn);
       // bio_balance_scan_btn = view.findViewById(R.id.bio_balance_scan_btn);

       // balance_enquiry_aadhar_number = view.findViewById(R.id.balance_enquiry_aadhar_number);
       // balance_enquiry_virtual_id = view.findViewById(R.id.balance_enquiry_virtual_id);

        aadhar_number = view.findViewById(R.id.aadhar_number);
        virtual_id = view.findViewById(R.id.virtual_id);

        aadhar_number = view.findViewById(R.id.aadhar_number);
        virtual_id = view.findViewById(R.id.virtual_id);

        checkCashWithdrawalamount = view.findViewById(R.id.amount);
        aadhar_no = view.findViewById(R.id.aadhar_no);
       // balance_enquiry_aadhar_no = view.findViewById(R.id.balance_enquiry_aadhar_no);

       // balance_enquiry_mobile_no = view.findViewById(R.id.balance_enquiry_mobile_no);
        mobile_no = view.findViewById(R.id.mobile_no);

        //balanceEnqureyBar = view.findViewById(R.id.balanceEnqureyBar);
       // balanceEnqureyBar.setVisibility(View.GONE);

        withdraw_balanceEnqureyBar = view.findViewById(R.id.withdraw_balanceEnqureyBar);
        withdraw_balanceEnqureyBar.setVisibility(View.GONE);

        //fingerprintStrengthBalance = view.findViewById(R.id.fingerprintStrengthBalance);
        transactionReport = view.findViewById(R.id.transactionReport);

       // balanceNote = view.findViewById(R.id.balanceNote);
        //balanceNote.setVisibility(View.GONE);

        settingBtn = view.findViewById(R.id.settingBtn);


        serializer = new Persister();
        loadingView = new ProgressDialog(getActivity());
        loadingView.setCancelable(false);
        loadingView.setMessage("Loading...");

        session = new SessionManager(getActivity());
        HashMap<String, String> user = session.getUserSession();
        String _userName = user.get(session.userName);
        String _userType = user.get(session.userType);
        String _userBalance = user.get(session.userBalance);
        String _adminName = user.get(session.adminName);
        String _mposNumber = user.get(session.mposNumber);


        HashMap<String, String> _user = session.getUserDetails();
        _token = _user.get(SessionManager.KEY_TOKEN);
        _admin = _user.get(SessionManager.KEY_ADMIN);

        //mActionsListener = new BalancePresenter(DashboardActivity.this);
        //mActionsListener.loadBalance(session.getUserToken());

       /* cash_withdrawal_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                releaseDataBalanceEnquire();
                cash_withdrawal_layout.setVisibility(View.VISIBLE);
                container_balance_enquiry_layout.setVisibility(View.GONE);
            }
        });

        balance_enquiry_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                releaseDataCashWithdrawal();
                cash_withdrawal_layout.setVisibility(View.GONE);
                container_balance_enquiry_layout.setVisibility(View.VISIBLE);
            }
        });
*/

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        aadhar_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (aadhar_number.isChecked()) {
                    aadhar_no.setHint("Enter aadhar number");
                }
            }
        });

        virtual_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (aadhar_number.isChecked()) {
                    aadhar_no.setHint("Enter virtual id");
                }
            }
        });


       /* balance_enquiry_aadhar_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (balance_enquiry_aadhar_number.isChecked()) {
                    balance_enquiry_aadhar_no.setHint("Enter aadhar no");
                }
            }
        });


        balance_enquiry_virtual_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (balance_enquiry_virtual_id.isChecked()) {
                    balance_enquiry_aadhar_no.setHint("Enter adhar virtual id");
                }
            }
        });*/

        transactionReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, ReportActivity.class);
                activity.startActivity(intent);

            }
        });
        settingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, SettingsActivity.class);
                activity.startActivity(intent);
            }
        });


        bio_cash_scan_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HashMap<String, String> deviceInfo = session.getDeviceInfoSession();

                try {
                    if (deviceInfo.get("manufacturername").trim().equalsIgnoreCase(mantradeviceid)) {
                        capture();
                    } else if (deviceInfo.get("manufacturername").trim().equalsIgnoreCase(morphoe2device)) {
                        morophoCapture();
                    } else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.recommended_device), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception exc) {
                    exc.printStackTrace();
                    Toast.makeText(getActivity(), getResources().getString(R.string.recommended_device), Toast.LENGTH_SHORT).show();
                }
            }
        });

/*        bio_balance_scan_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, String> deviceInfo = session.getDeviceInfoSession();
                try {
                    if (deviceInfo.get("manufacturername").trim().equalsIgnoreCase(mantradeviceid)) {
                       // Toast.makeText(getActivity(), "bio_balance_scan_btn : " + deviceInfo.get("manufacturername").trim(), Toast.LENGTH_SHORT).show();
                        capture();
                    } else if (deviceInfo.get("manufacturername").trim().equalsIgnoreCase(morphoe2device)) {
                       // Toast.makeText(getActivity(), "bio_balance_scan_btn : " + deviceInfo.get("manufacturername").trim(), Toast.LENGTH_SHORT).show();
                        morophoCapture();
                    } else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.recommended_device), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception exc) {
                    exc.printStackTrace();
                    Toast.makeText(getActivity(), getResources().getString(R.string.recommended_device), Toast.LENGTH_SHORT).show();
                }
       *//* PackageManager pm = getActivity().getPackageManager();
        boolean isInstalled = isPackageInstalled("com.mantra.clientmanagement", pm);

        if (isInstalled) {
        //morophoCapture();
        capture();
        } else {
        //morphoMessage();
        mantraMessage();
        }*//*
            }
        });*/


       /* balance_enquiry_bankspinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLoader();
                Intent in = new Intent(getActivity(), BankNameListActivity.class);
                startActivityForResult(in, Constants.REQUEST_FOR_ACTIVITY_BALANCE_ENQUIRY_CODE);
            }
        });*/

        cash_withdrawal_bankspinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLoader();
                Intent in = new Intent(getActivity(), BankNameListActivity.class);
                startActivityForResult(in, Constants.REQUEST_FOR_ACTIVITY_CASH_WITHDRAWAL_CODE);
            }
        });

        try {

            JSONArray jsonArray = new JSONArray(Constants.bank_name_list);

            bank_code.clear();
            bank_list.clear();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String BANKNAME = jsonObject.getString("BANKNAME");
                String IIN = jsonObject.getString("IIN");

                bank_list.add(BANKNAME);
                bank_code.add(IIN);
            }

            ArrayAdapter<String> _adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, bank_list);

            cash_withdrawal_select_bank.setAdapter(_adapter);

            //balance_enquiry_select_bank.setAdapter(_adapter);

        } catch (Exception exc) {
            exc.printStackTrace();
        }

        String bal = Util.amountFormat(_userBalance);//_userBalance

        //amout ="₹ "+ bal;

        text_balance.setText("Balance: ₹  " + bal);


        cash_withdrawal_select_bank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cash_withdrawal_nnid = bank_code.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

       /* balance_enquiry_select_bank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                balance_enquiry_nnid = bank_code.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        submit_balance_enquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkCashWithdrawalamount.setVisibility(View.GONE);
                String withdrawalaadharNo = "";

                if (aadhar_number.isChecked()) {
                    withdrawalaadharNo = aadhar_no.getText().toString();
                    if (withdrawalaadharNo.contains("-")) {
                        withdrawalaadharNo = withdrawalaadharNo.replaceAll("-", "").trim();
                    }
                    if (Util.validateAadharNumber(withdrawalaadharNo) == false) {
                        aadhar_no.setError(getResources().getString(R.string.valid_aadhar_error));
                        return;
                    }
                }


                if (virtual_id.isChecked()) {
                    withdrawalaadharNo = aadhar_no.getText().toString();
                    if (withdrawalaadharNo.contains("-")) {
                        withdrawalaadharNo = withdrawalaadharNo.replaceAll("-", "").trim();
                    }
                    if (Util.validateAadharVID(withdrawalaadharNo) == false) {
                        aadhar_no.setError(getResources().getString(R.string.valid_aadhar_error));
                        return;
                    }
                }


                if (mobile_no.getText() == null || mobile_no.getText().toString().trim().matches("") || Util.isValidMobile(mobile_no.getText().toString().trim()) == false) {
                    mobile_no.setError(getResources().getString(R.string.mobileerror));
                    return;
                }


                String panaaadhaar = mobile_no.getText().toString().trim();
                if (!panaaadhaar.contains(" ") && panaaadhaar.length() == 10) {
                } else {
                    mobile_no.setError(getResources().getString(R.string.mobileerror));
                    return;
                }
                //---------------------------------------------------------


                /*if (checkCashWithdrawalamount.getText() == null || checkCashWithdrawalamount.getText().toString().trim().matches("")) {
                    checkCashWithdrawalamount.setError(getResources().getString(R.string.amount_error));
                    return;
                }*/


                if (cash_withdrawal_nnid == null || cash_withdrawal_nnid.trim().matches("")) {
                    Toast.makeText(getActivity(), "" + R.string.select_bank_error, Toast.LENGTH_SHORT).show();
                    return;
                }


                try {
                    HashMap<String, String> deviceInfo = session.getDeviceInfoSession();

                    deviceSerialNumber = deviceInfo.get("manufacturername").trim();

                    withdrawalaadharNo = aadhar_no.getText().toString();


                    if (deviceSerialNumber.trim().equalsIgnoreCase(mantradeviceid)) {
                        if (pidData == null) {
                            Toast.makeText(getActivity(), "Please Scan Your Finger", Toast.LENGTH_SHORT).show();
                        }
                        if (!pidData._Resp.errCode.equals("0")) {
                        } else {
                            hideKeyboard();
                             new AuthRequest(withdrawalaadharNo, pidData).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                        }
                    } else if (deviceSerialNumber.trim().equalsIgnoreCase(morphodeviceid) || deviceSerialNumber.trim().equalsIgnoreCase(morphoe2device)) {
                        if (morphoPidData == null) {
                            Toast.makeText(getActivity(), "Please Scan Your Finger", Toast.LENGTH_SHORT).show();
                        }
                        if (!morphoPidData._Resp.errCode.equals("0")) {
                        } else {
                            hideKeyboard();
                            new BalanceEnqAuthRequestMorpho(withdrawalaadharNo, morphoPidData).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                           // new CashWithdrawalAuthRequestMorpho(withdrawalaadharNo, morphoPidData).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                    }
                } catch (Exception exc) {
                    exc.printStackTrace();
                    Toast.makeText(getActivity(), "Please choose a recommended device to proceed!!!", Toast.LENGTH_SHORT).show();
                }

       /* withdrawalaadharNo = aadhar_no.getText().toString();

       try {
           if (deviceInfo.get("manufacturername").trim().equalsIgnoreCase(mantradeviceid)) {
               // Mantra device
               Toast.makeText(getActivity(), "withdrawala aadhar No :: " + withdrawalaadharNo, Toast.LENGTH_SHORT).show();
               Toast.makeText(getActivity(), "deviceInfo :: " + deviceInfo, Toast.LENGTH_SHORT).show();
               Toast.makeText(getActivity(), "pidData :: " + pidData, Toast.LENGTH_SHORT).show();
               new AuthCashRequest(withdrawalaadharNo, pidData).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
           } else if (deviceInfo.get("manufacturername").trim().equalsIgnoreCase(morphoe2device)) {
               // Moropho
               Toast.makeText(getActivity(), "withdrawala aadhar No :: " + withdrawalaadharNo, Toast.LENGTH_SHORT).show();
               Toast.makeText(getActivity(), "deviceInfo :: " + deviceInfo, Toast.LENGTH_SHORT).show();
               Toast.makeText(getActivity(), "morphoPidData :: " + morphoPidData, Toast.LENGTH_SHORT).show();
               new CashWithdrawalAuthRequestMorpho(withdrawalaadharNo, morphoPidData).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
           } else {
               Toast.makeText(getActivity(), getResources().getString(R.string.recommended_device), Toast.LENGTH_SHORT).show();
           }
       } catch (Exception exc) {
           exc.printStackTrace();
       }*/

                // Toast.makeText(getActivity(), "" + withdrawalaadharNo, Toast.LENGTH_SHORT).show();
                // Toast.makeText(getActivity(), "" + pidData, Toast.LENGTH_SHORT).show();
                // new AuthRequestMorpho(withdrawalaadharNo, morphoPidData).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);







                /*String balanceaadharNo = "";

                if (balance_enquiry_aadhar_number.isChecked()) {
                    balanceaadharNo = balance_enquiry_aadhar_no.getText().toString();
                    if (balanceaadharNo.contains("-")) {
                        balanceaadharNo = balanceaadharNo.replaceAll("-", "").trim();
                    }
                    if (Util.validateAadharNumber(balanceaadharNo) == false) {
                        balance_enquiry_aadhar_no.setError(getResources().getString(R.string.valid_aadhar_error));
                        return;
                    }
                }


                if (balance_enquiry_virtual_id.isChecked()) {
                    balanceaadharNo = balance_enquiry_aadhar_no.getText().toString();
                    if (balanceaadharNo.contains("-")) {
                        balanceaadharNo = balanceaadharNo.replaceAll("-", "").trim();
                    }
                    if (Util.validateAadharVID(balanceaadharNo) == false) {
                        balance_enquiry_aadhar_no.setError(getResources().getString(R.string.valid_aadhar__uid_error));
                        return;
                    }
                }

                if (balance_enquiry_mobile_no.getText() == null || balance_enquiry_mobile_no.getText().toString().trim().matches("") || Util.isValidMobile(balance_enquiry_mobile_no.getText().toString().trim()) == false) {
                    balance_enquiry_mobile_no.setError(getResources().getString(R.string.mobileerror));
                    return;
                } else {
                    String panaaadhaar = balance_enquiry_mobile_no.getText().toString().trim();
                    if (!panaaadhaar.contains(" ") && panaaadhaar.length() == 10) {
                    } else {
                        balance_enquiry_mobile_no.setError(getResources().getString(R.string.mobileerror));
                        return;
                    }
                }

                if (balance_enquiry_nnid == null || balance_enquiry_nnid.trim().matches("")) {
                    //balanceBankspinner.setError ( getResources ().getString ( R.string.select_bank_error ) );
                    Toast.makeText(getActivity(), R.string.select_bank_error, Toast.LENGTH_SHORT).show();
                    return;
                }


                try {

                    HashMap<String, String> deviceInfo = session.getDeviceInfoSession();
                    String deviceSerialNumber = deviceInfo.get("manufacturername").trim();

                    if (deviceSerialNumber.trim().equalsIgnoreCase(mantradeviceid)) {
                        if (pidData == null) {
                            Toast.makeText(getActivity(), "Please Scan Your Finger", Toast.LENGTH_SHORT).show();
                        }

                        if (!pidData._Resp.errCode.equals("0")) {
                        } else {
                            hideKeyboard();
                            new AuthRequest(balanceaadharNo, pidData).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                    } else if (deviceSerialNumber.trim().equalsIgnoreCase(morphodeviceid) || deviceSerialNumber.trim().equalsIgnoreCase(morphoe2device)) {
                        if (morphoPidData == null) {
                            Toast.makeText(getActivity(), "Please Scan Your Finger", Toast.LENGTH_SHORT).show();
                        }
                        if (!morphoPidData._Resp.errCode.equals("0")) {
                        } else {
                            hideKeyboard();
                            new BalanceEnqAuthRequestMorpho(balanceaadharNo, morphoPidData).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                    }
                } catch (Exception exc) {
                    exc.printStackTrace();

                    Toast.makeText(getActivity(), "Please choose a recommended device to proceed!!!", Toast.LENGTH_SHORT).show();

                }*/


                // String balanceaadharNo = balance_enquiry_aadhar_no.getText().toString();
                // HashMap<String, String> deviceInfo = session.getDeviceInfoSession();
//                try {
//                    if (deviceInfo.get("manufacturername").trim().equalsIgnoreCase(mantradeviceid)) {
//                        // Mantra device
//                        Toast.makeText(getActivity(), "withdrawala aadhar No :: "+balanceaadharNo, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(getActivity(), "deviceInfo :: "+deviceInfo, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(getActivity(), "mantra pidData :: "+pidData, Toast.LENGTH_SHORT).show();
//
//                        new AuthRequest(balanceaadharNo, pidData).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//                    } else if (deviceInfo.get("manufacturername").trim().equalsIgnoreCase(morphoe2device)) {
//                        // Moropho
//
//                        Toast.makeText(getActivity(), "withdrawala aadhar No :: "+balanceaadharNo, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(getActivity(), "deviceInfo :: "+deviceInfo, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(getActivity(), "Maopho PidData :: "+morphoPidData, Toast.LENGTH_SHORT).show();
//
//                        new BalanceEnqAuthRequestMorpho ( balanceaadharNo, morphoPidData ).executeOnExecutor (AsyncTask.THREAD_POOL_EXECUTOR );
//                    } else {
//                        Toast.makeText(getActivity(), getResources().getString(R.string.recommended_device), Toast.LENGTH_SHORT).show();
//                    }
//                } catch (Exception exc) {
//                    exc.printStackTrace();
//                }
            }
        });

        checkCashWithdrawalamount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if(s.toString().isEmpty()){
                    amouttxt.setText("");

                }else {

                    String str = Currency.convertToIndianCurrency(s.toString());
                    amouttxt.setText(str);
                }

            }
        });

        submit_cash_balance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkCashWithdrawalamount.setVisibility(View.VISIBLE);

                String withdrawalaadharNo = "";

                if (aadhar_number.isChecked()) {
                    withdrawalaadharNo = aadhar_no.getText().toString();
                    if (withdrawalaadharNo.contains("-")) {
                        withdrawalaadharNo = withdrawalaadharNo.replaceAll("-", "").trim();
                    }
                    if (Util.validateAadharNumber(withdrawalaadharNo) == false) {
                        aadhar_no.setError(getResources().getString(R.string.valid_aadhar_error));
                        return;
                    }
                }


                if (virtual_id.isChecked()) {
                    withdrawalaadharNo = aadhar_no.getText().toString();
                    if (withdrawalaadharNo.contains("-")) {
                        withdrawalaadharNo = withdrawalaadharNo.replaceAll("-", "").trim();
                    }
                    if (Util.validateAadharVID(withdrawalaadharNo) == false) {
                        aadhar_no.setError(getResources().getString(R.string.valid_aadhar_error));
                        return;
                    }
                }


                if (mobile_no.getText() == null || mobile_no.getText().toString().trim().matches("") || Util.isValidMobile(mobile_no.getText().toString().trim()) == false) {
                    mobile_no.setError(getResources().getString(R.string.mobileerror));
                    return;
                }


                String panaaadhaar = mobile_no.getText().toString().trim();
                if (!panaaadhaar.contains(" ") && panaaadhaar.length() == 10) {
                } else {
                    mobile_no.setError(getResources().getString(R.string.mobileerror));
                    return;
                }


                if (checkCashWithdrawalamount.getText() == null || checkCashWithdrawalamount.getText().toString().trim().matches("")) {
                    checkCashWithdrawalamount.setError(getResources().getString(R.string.amount_error));
                    return;
                }


                if (cash_withdrawal_nnid == null || cash_withdrawal_nnid.trim().matches("")) {
                    Toast.makeText(getActivity(), "" + R.string.select_bank_error, Toast.LENGTH_SHORT).show();
                    return;
                }


                try {
                    HashMap<String, String> deviceInfo = session.getDeviceInfoSession();

                    deviceSerialNumber = deviceInfo.get("manufacturername").trim();

                    withdrawalaadharNo = aadhar_no.getText().toString();


                    if (deviceSerialNumber.trim().equalsIgnoreCase(mantradeviceid)) {
                        if (pidData == null) {
                            Toast.makeText(getActivity(), "Please Scan Your Finger", Toast.LENGTH_SHORT).show();
                        }
                        if (!pidData._Resp.errCode.equals("0")) {
                        } else {
                            hideKeyboard();
                            new AuthCashRequest(withdrawalaadharNo, pidData).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                        }
                    } else if (deviceSerialNumber.trim().equalsIgnoreCase(morphodeviceid) || deviceSerialNumber.trim().equalsIgnoreCase(morphoe2device)) {
                        if (morphoPidData == null) {
                            Toast.makeText(getActivity(), "Please Scan Your Finger", Toast.LENGTH_SHORT).show();
                        }
                        if (!morphoPidData._Resp.errCode.equals("0")) {
                        } else {
                            hideKeyboard();
                            new CashWithdrawalAuthRequestMorpho(withdrawalaadharNo, morphoPidData).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                    }
                } catch (Exception exc) {
                    exc.printStackTrace();
                    Toast.makeText(getActivity(), "Please choose a recommended device to proceed!!!", Toast.LENGTH_SHORT).show();
                }

       /* withdrawalaadharNo = aadhar_no.getText().toString();

       try {
           if (deviceInfo.get("manufacturername").trim().equalsIgnoreCase(mantradeviceid)) {
               // Mantra device
               Toast.makeText(getActivity(), "withdrawala aadhar No :: " + withdrawalaadharNo, Toast.LENGTH_SHORT).show();
               Toast.makeText(getActivity(), "deviceInfo :: " + deviceInfo, Toast.LENGTH_SHORT).show();
               Toast.makeText(getActivity(), "pidData :: " + pidData, Toast.LENGTH_SHORT).show();
               new AuthCashRequest(withdrawalaadharNo, pidData).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
           } else if (deviceInfo.get("manufacturername").trim().equalsIgnoreCase(morphoe2device)) {
               // Moropho
               Toast.makeText(getActivity(), "withdrawala aadhar No :: " + withdrawalaadharNo, Toast.LENGTH_SHORT).show();
               Toast.makeText(getActivity(), "deviceInfo :: " + deviceInfo, Toast.LENGTH_SHORT).show();
               Toast.makeText(getActivity(), "morphoPidData :: " + morphoPidData, Toast.LENGTH_SHORT).show();
               new CashWithdrawalAuthRequestMorpho(withdrawalaadharNo, morphoPidData).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
           } else {
               Toast.makeText(getActivity(), getResources().getString(R.string.recommended_device), Toast.LENGTH_SHORT).show();
           }
       } catch (Exception exc) {
           exc.printStackTrace();
       }*/

                // Toast.makeText(getActivity(), "" + withdrawalaadharNo, Toast.LENGTH_SHORT).show();
                // Toast.makeText(getActivity(), "" + pidData, Toast.LENGTH_SHORT).show();
                // new AuthRequestMorpho(withdrawalaadharNo, morphoPidData).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });



        return view;
    }

    private boolean isPackageInstalled(String packagename, PackageManager packageManager) {
        try {
            packageManager.getPackageGids(packagename);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    private void morophoCapture() {
        try {
            String pidOption = getPIDOptions();
            if (pidOption != null) {
                Intent intent = new Intent("in.gov.uidai.rdservice.fp.CAPTURE");
                intent.setPackage("com.scl.rdservice");
                intent.putExtra("PID_OPTIONS", pidOption);
                startActivityForResult(intent, 4);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Util.showAlert(getActivity(), getResources().getString(R.string.alert_error), getResources().getString(R.string.setting_device));

        }

    }

    private void capture() { // mantra
        try {
            String pidOption = getPIDOptions();
            if (pidOption != null) {
                Intent intent2 = new Intent();
                intent2.setAction("in.gov.uidai.rdservice.fp.CAPTURE");
                intent2.setPackage("com.mantra.rdservice");
                intent2.putExtra("PID_OPTIONS", pidOption);
                startActivityForResult(intent2, 2);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Util.showAlert(getActivity(), getResources().getString(R.string.alert_error), getResources().getString(R.string.setting_device));
        }
    }

/*    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //Toast.makeText(getActivity(),"request Code :::::::"+requestCode,Toast.LENGTH_LONG).show();
        //Toast.makeText(getActivity(),"result Code ==== "+resultCode,Toast.LENGTH_LONG).show();


        if (requestCode == Constants.REQUEST_FOR_ACTIVITY_BALANCE_ENQUIRY_CODE) {
            hideLoader();

            if (resultCode == RESULT_OK) {
                BankNameModel bankIINValue = (BankNameModel) data.getSerializableExtra(Constants.IIN_KEY);
                balance_enquiry_bankspinner.setText(bankIINValue.getBankName());
                balance_enquiry_nnid = bankIINValue.getIin();
                cash_withdrawal_nnid = "";
                // checkBalanceEnquiryValidation();
            }

            // checkBalanceEnquiryValidation();

        } else if (requestCode == Constants.REQUEST_FOR_ACTIVITY_CASH_WITHDRAWAL_CODE) {
            hideLoader();
            if (resultCode == RESULT_OK) {
                BankNameModel bankIINValue = (BankNameModel) data.getSerializableExtra(Constants.IIN_KEY);
                cash_withdrawal_bankspinner.setText(bankIINValue.getBankName());
                cash_withdrawal_nnid = bankIINValue.getIin();
                balance_enquiry_nnid = "";
                //checkWithdrawalValidation();

            }
            //checkWithdrawalValidation();
        }


        if (resultCode == RESULT_OK) {
            //request code 4

            if (requestCode == 4) {
                try {
                    if (data != null) {
                        String result = data.getStringExtra("PID_DATA");
                        morphoPidData = serializer.read(MorphoPidData.class, result);


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            //request code 2
            if (requestCode == 2) {
                try {
                    String result = data.getStringExtra("PID_DATA");

                    if (result != null) {
                        pidData = serializer.read(PidData.class, result);

                        Snackbar _snackbar = Snackbar.make(cash_withdrawal_btn, "pidData " + pidData, Snackbar.LENGTH_LONG);
                        _snackbar.show();

                        if (Float.parseFloat(pidData._Resp.qScore) <= 60) {
//                            withdrawalBar.setVisibility ( View.VISIBLE );
//                            withdrawalBar.setProgress ( Float.parseFloat ( pidData._Resp.qScore ) );
//                            withdrawalBar.setProgressTextMoved ( true );
//                            withdrawalBar.setEndColor ( getResources ().getColor ( R.color.red ) );
//                            withdrawalBar.setStartColor ( getResources ().getColor ( R.color.red ) );
//                            withdrawalNote.setVisibility ( View.VISIBLE );
//                            fingerprintStrengthWithdrawal.setVisibility ( View.VISIBLE );

//                            balanceEnqureyBar.setVisibility ( View.VISIBLE );
//                            balanceEnqureyBar.setProgress ( Float.parseFloat ( pidData._Resp.qScore ) );
//                            balanceEnqureyBar.setProgressTextMoved ( true );
//                            balanceEnqureyBar.setEndColor ( getResources ().getColor ( R.color.red ) );
//                            balanceEnqureyBar.setStartColor ( getResources ().getColor ( R.color.red ) );
//
//                            balanceEnqureyBar.setVisibility ( View.VISIBLE );
//                            balanceNote.setVisibility ( View.VISIBLE );
//
//                            fingerprintStrengthBalance.setVisibility ( View.VISIBLE );

//                            depositBar.setVisibility ( View.VISIBLE );
//                            depositBar.setProgress ( Float.parseFloat ( pidData._Resp.qScore ) );
//                            depositBar.setProgressTextMoved ( true );
//                            depositBar.setEndColor ( getResources ().getColor ( R.color.red ) );
//                            depositBar.setStartColor ( getResources ().getColor ( R.color.red ) );
//                            depositNote.setVisibility ( View.VISIBLE );
//                            fingerprintStrengthDeposit.setVisibility ( View.V
                            //Snackbar snackbar = Snackbar.make(cash_withdrawal_btn, "Note : Recommended Fingerprint Strenght is 70% "+ "your Fingerprint strenght is :"+ Float.parseFloat ( pidData._Resp.qScore ), Snackbar.LENGTH_LONG);
                            //snackbar.show();

                            Toast.makeText(getActivity(), "Your Fingerprint Strenght is : " + Float.parseFloat(pidData._Resp.qScore), Toast.LENGTH_SHORT).show();

                        } else if (Float.parseFloat(pidData._Resp.qScore) >= 60 && Float.parseFloat(pidData._Resp.qScore) <= 70) {
//                            withdrawalBar.setVisibility ( View.VISIBLE );
//                            withdrawalBar.setProgress ( Float.parseFloat ( pidData._Resp.qScore ) );
//                            withdrawalBar.setProgressTextMoved ( true );
//                            withdrawalBar.setEndColor ( getResources ().getColor ( R.color.yellow ) );
//                            withdrawalBar.setStartColor ( getResources ().getColor ( R.color.yellow ) );
//                            withdrawalNote.setVisibility ( View.VISIBLE );
//                            fingerprintStrengthWithdrawal.setVisibility ( View.VISIBLE );

//                            balanceEnqureyBar.setVisibility ( View.VISIBLE );
//                            balanceEnqureyBar.setProgress ( Float.parseFloat ( pidData._Resp.qScore ) );
//                            balanceEnqureyBar.setProgressTextMoved ( true );
//                            balanceEnqureyBar.setEndColor ( getResources ().getColor ( R.color.yellow ) );
//                            balanceEnqureyBar.setStartColor ( getResources ().getColor ( R.color.yellow ) );
//                            balanceNote.setVisibility ( View.VISIBLE );
//                            fingerprintStrengthBalance.setVisibility ( View.VISIBLE );

//                            depositBar.setVisibility ( View.VISIBLE );
//                            depositBar.setProgress ( Float.parseFloat ( pidData._Resp.qScore ) );
//                            depositBar.setProgressTextMoved ( true );
//                            depositBar.setEndColor ( getResources ().getColor ( R.color.yellow ) );
//                            depositBar.setStartColor ( getResources ().getColor ( R.color.yellow ) );
//                            depositNote.setVisibility ( View.VISIBLE );
//                            fingerprintStrengthDeposit.setVisibility ( View.VISIBLE );
                            //Snackbar snackbar = Snackbar.make(cash_withdrawal_btn, "Note : Recommended Fingerprint Strenght is 70% "+ "your Fingerprint strenght is :"+ Float.parseFloat ( pidData._Resp.qScore ), Snackbar.LENGTH_LONG);
                            //snackbar.show();

                            Toast.makeText(getActivity(), "Your Fingerprint Strenght is : " + Float.parseFloat(pidData._Resp.qScore), Toast.LENGTH_SHORT).show();

                        } else {
//                            withdrawalBar.setVisibility ( View.VISIBLE );
//                            withdrawalBar.setProgress ( Float.parseFloat ( pidData._Resp.qScore ) );
//                            withdrawalBar.setProgressTextMoved ( true );
//                            withdrawalBar.setEndColor ( getResources ().getColor ( R.color.green ) );
//                            withdrawalBar.setStartColor ( getResources ().getColor ( R.color.green ) );
//                            withdrawalNote.setVisibility ( View.VISIBLE );
//                            fingerprintStrengthWithdrawal.setVisibility ( View.VISIBLE );

//                            balanceEnqureyBar.setVisibility ( View.VISIBLE );
//                            balanceEnqureyBar.setProgress ( Float.parseFloat ( pidData._Resp.qScore ) );
//                            balanceEnqureyBar.setProgressTextMoved ( true );
//                            balanceEnqureyBar.setEndColor ( getResources ().getColor ( R.color.green ) );
//                            balanceEnqureyBar.setStartColor ( getResources ().getColor ( R.color.green ) );
//                            balanceNote.setVisibility ( View.VISIBLE );
//                            fingerprintStrengthBalance.setVisibility ( View.VISIBLE );

//                            depositBar.setVisibility ( View.VISIBLE );
//                            depositBar.setProgress ( Float.parseFloat ( pidData._Resp.qScore ) );
//                            depositBar.setProgressTextMoved ( true );
//                            depositBar.setEndColor ( getResources ().getColor ( R.color.green ) );
//                            depositBar.setStartColor ( getResources ().getColor ( R.color.green ) );
//                            depositNote.setVisibility ( View.VISIBLE );
//                            fingerprintStrengthDeposit.setVisibility ( View.VISIBLE );
                            //Snackbar snackbar = Snackbar.make(cash_withdrawal_btn, "Note : Recommended Fingerprint Strenght is 70% "+ "your Fingerprint strenght is :"+ Float.parseFloat ( pidData._Resp.qScore ), Snackbar.LENGTH_LONG);
                            //snackbar.show();

                            Toast.makeText(getActivity(), "Your Fingerprint Strenght is : " + Float.parseFloat(pidData._Resp.qScore), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), result, Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Util.showAlert(getActivity(), getResources().getString(R.string.alert_error), getResources().getString(R.string.setting_device));
                }
            }
        } else {
            Toast.makeText(getActivity(), "Try Again!", Toast.LENGTH_LONG);
        }
    }*/



    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQUEST_FOR_ACTIVITY_BALANCE_ENQUIRY_CODE) {

            hideLoader();

            if (resultCode == RESULT_OK) {
                BankNameModel bankIINValue = (BankNameModel) data.getSerializableExtra(Constants.IIN_KEY);
                balance_enquiry_bankspinner.setText(bankIINValue.getBankName());
                balance_enquiry_nnid = bankIINValue.getIin();
                cash_withdrawal_nnid = "";
                // checkBalanceEnquiryValidation();


            }
            // checkBalanceEnquiryValidation();

        } else if (requestCode == Constants.REQUEST_FOR_ACTIVITY_CASH_WITHDRAWAL_CODE) {
            hideLoader();
            if (resultCode == RESULT_OK) {
                BankNameModel bankIINValue = (BankNameModel) data.getSerializableExtra(Constants.IIN_KEY);
                cash_withdrawal_bankspinner.setText(bankIINValue.getBankName());
                cash_withdrawal_nnid = bankIINValue.getIin();
                balance_enquiry_nnid = "";
                //checkWithdrawalValidation();
            }
            // checkWithdrawalValidation();
        }
        if (resultCode == RESULT_OK) {
            //request code 4
            if (requestCode == 4) {
                try {
                    if (data != null) {
                        String result = data.getStringExtra("PID_DATA");
                      //  Toast.makeText(getActivity(), "morphoPidData result is : " + result, Toast.LENGTH_SHORT).show();
                        morphoPidData = serializer.read(MorphoPidData.class, result);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Util.showAlert(getActivity(), getResources().getString(R.string.alert_error), getResources().getString(R.string.setting_device));
                }
            }

            //request code 2
            if (requestCode == 2) {
                try {
                    String result = data.getStringExtra("PID_DATA");
                    if (result != null) {
                        pidData = serializer.read(PidData.class, result);
                       // Toast.makeText(getActivity(), "pidData result is : " + result, Toast.LENGTH_SHORT).show();
                        //
//                        if (Float.parseFloat(pidData._Resp.qScore) <= 60) {
////                            withdrawalBar.setVisibility ( View.VISIBLE );
////                            withdrawalBar.setProgress ( Float.parseFloat ( pidData._Resp.qScore ) );
////                            withdrawalBar.setProgressTextMoved ( true );
////                            withdrawalBar.setEndColor ( getResources ().getColor ( R.color.red ) );
////                            withdrawalBar.setStartColor ( getResources ().getColor ( R.color.red ) );
////                            withdrawalNote.setVisibility ( View.VISIBLE );
////                            fingerprintStrengthWithdrawal.setVisibility ( View.VISIBLE );
//
////                            balanceEnqureyBar.setVisibility ( View.VISIBLE );
////                            balanceEnqureyBar.setProgress ( Float.parseFloat ( pidData._Resp.qScore ) );
////                            balanceEnqureyBar.setProgressTextMoved ( true );
////                            balanceEnqureyBar.setEndColor ( getResources ().getColor ( R.color.red ) );
////                            balanceEnqureyBar.setStartColor ( getResources ().getColor ( R.color.red ) );
////
////                            balanceEnqureyBar.setVisibility ( View.VISIBLE );
////                            balanceNote.setVisibility ( View.VISIBLE );
////
////                            fingerprintStrengthBalance.setVisibility ( View.VISIBLE );
//
////                            depositBar.setVisibility ( View.VISIBLE );
////                            depositBar.setProgress ( Float.parseFloat ( pidData._Resp.qScore ) );
////                            depositBar.setProgressTextMoved ( true );
////                            depositBar.setEndColor ( getResources ().getColor ( R.color.red ) );
////                            depositBar.setStartColor ( getResources ().getColor ( R.color.red ) );
////                            depositNote.setVisibility ( View.VISIBLE );
////                            fingerprintStrengthDeposit.setVisibility ( View.V
//                            //Snackbar snackbar = Snackbar.make(cash_withdrawal_btn, "Note : Recommended Fingerprint Strenght is 70% "+ "your Fingerprint strenght is :"+ Float.parseFloat ( pidData._Resp.qScore ), Snackbar.LENGTH_LONG);
//                            //snackbar.show();
//                            Toast.makeText(getActivity(), "Your Fingerprint Strenght is : " + Float.parseFloat(pidData._Resp.qScore), Toast.LENGTH_SHORT).show();
//                        } else if (Float.parseFloat(pidData._Resp.qScore) >= 60 && Float.parseFloat(pidData._Resp.qScore) <= 70) {
////                            withdrawalBar.setVisibility ( View.VISIBLE );
////                            withdrawalBar.setProgress ( Float.parseFloat ( pidData._Resp.qScore ) );
////                            withdrawalBar.setProgressTextMoved ( true );
////                            withdrawalBar.setEndColor ( getResources ().getColor ( R.color.yellow ) );
////                            withdrawalBar.setStartColor ( getResources ().getColor ( R.color.yellow ) );
////                            withdrawalNote.setVisibility ( View.VISIBLE );
////                            fingerprintStrengthWithdrawal.setVisibility ( View.VISIBLE );
//
////                            balanceEnqureyBar.setVisibility ( View.VISIBLE );
////                            balanceEnqureyBar.setProgress ( Float.parseFloat ( pidData._Resp.qScore ) );
////                            balanceEnqureyBar.setProgressTextMoved ( true );
////                            balanceEnqureyBar.setEndColor ( getResources ().getColor ( R.color.yellow ) );
////                            balanceEnqureyBar.setStartColor ( getResources ().getColor ( R.color.yellow ) );
////                            balanceNote.setVisibility ( View.VISIBLE );
////                            fingerprintStrengthBalance.setVisibility ( View.VISIBLE );
//
////                            depositBar.setVisibility ( View.VISIBLE );
////                            depositBar.setProgress ( Float.parseFloat ( pidData._Resp.qScore ) );
////                            depositBar.setProgressTextMoved ( true );
////                            depositBar.setEndColor ( getResources ().getColor ( R.color.yellow ) );
////                            depositBar.setStartColor ( getResources ().getColor ( R.color.yellow ) );
////                            depositNote.setVisibility ( View.VISIBLE );
////                            fingerprintStrengthDeposit.setVisibility ( View.VISIBLE );
//                            //Snackbar snackbar = Snackbar.make(cash_withdrawal_btn, "Note : Recommended Fingerprint Strenght is 70% "+ "your Fingerprint strenght is :"+ Float.parseFloat ( pidData._Resp.qScore ), Snackbar.LENGTH_LONG);
//                            //snackbar.show();
//                            Toast.makeText(getActivity(), "Your Fingerprint Strenght is : " + Float.parseFloat(pidData._Resp.qScore), Toast.LENGTH_SHORT).show();
//                        } else {
////                            withdrawalBar.setVisibility ( View.VISIBLE );
////                            withdrawalBar.setProgress ( Float.parseFloat ( pidData._Resp.qScore ) );
////                            withdrawalBar.setProgressTextMoved ( true );
////                            withdrawalBar.setEndColor ( getResources ().getColor ( R.color.green ) );
////                            withdrawalBar.setStartColor ( getResources ().getColor ( R.color.green ) );
////                            withdrawalNote.setVisibility ( View.VISIBLE );
////                            fingerprintStrengthWithdrawal.setVisibility ( View.VISIBLE );
//
////                            balanceEnqureyBar.setVisibility ( View.VISIBLE );
////                            balanceEnqureyBar.setProgress ( Float.parseFloat ( pidData._Resp.qScore ) );
////                            balanceEnqureyBar.setProgressTextMoved ( true );
////                            balanceEnqureyBar.setEndColor ( getResources ().getColor ( R.color.green ) );
////                            balanceEnqureyBar.setStartColor ( getResources ().getColor ( R.color.green ) );
////                            balanceNote.setVisibility ( View.VISIBLE );
////                            fingerprintStrengthBalance.setVisibility ( View.VISIBLE );
//
////                            depositBar.setVisibility ( View.VISIBLE );
////                            depositBar.setProgress ( Float.parseFloat ( pidData._Resp.qScore ) );
////                            depositBar.setProgressTextMoved ( true );
////                            depositBar.setEndColor ( getResources ().getColor ( R.color.green ) );
////                            depositBar.setStartColor ( getResources ().getColor ( R.color.green ) );
////                            depositNote.setVisibility ( View.VISIBLE );
////                            fingerprintStrengthDeposit.setVisibility ( View.VISIBLE );
//
//                            //Snackbar snackbar = Snackbar.make(cash_withdrawal_btn, "Note : Recommended Fingerprint Strenght is 70% "+ "your Fingerprint strenght is :"+ Float.parseFloat ( pidData._Resp.qScore ), Snackbar.LENGTH_LONG);
//                            //snackbar.show();
//                            Toast.makeText(getActivity(), "Your Fingerprint Strenght is : " + Float.parseFloat(pidData._Resp.qScore), Toast.LENGTH_SHORT).show();
//                        }
                    } else {
                        Toast.makeText(getActivity(), result, Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Util.showAlert(getActivity(), getResources().getString(R.string.alert_error), getResources().getString(R.string.setting_device));
                }
            }
        } else {
            Toast.makeText(getActivity(), "Try Again!", Toast.LENGTH_LONG);
        }
    }

    private String getPIDOptions() {
        try {
            String posh = "UNKNOWN";

            // getResources().getString(R.string.posh);

            if (positions.size() > 0) {
                posh = positions.toString().replace("[", "").replace("]", "").replaceAll("[\\s+]", "");
            }

            Opts opts = new Opts();
            opts.fCount = "1";
            opts.fType = "0";
            opts.iCount = "0";
            opts.iType = "0";
            opts.pCount = "0";
            opts.pType = "0";
            opts.format = "0";
            opts.pidVer = "2.0";
            opts.timeout = "10000";
            opts.posh = posh;
            opts.env = "P";

            PidOptions pidOptions = new PidOptions();
            pidOptions.ver = "1.0";
            pidOptions.Opts = opts;

            Serializer serializer = new Persister();
            StringWriter writer = new StringWriter();
            serializer.write(pidOptions, writer);
            return writer.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void morphoMessage() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(getActivity());
        }
        builder.setCancelable(false);
        builder.setTitle(getResources().getString(R.string.morpho))
                .setMessage(getResources().getString(R.string.install_morpho_message))
                .setPositiveButton(getResources().getString(R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        /*
                         * play store intent
                         */
                        final String appPackageName = "com.scl.rdservice"; // getPackageName() from Context or Activity object
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                })
                .show();
    }

    private void mantraMessage() {
        AlertDialog.Builder builder;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(getActivity());
        }

        builder.setCancelable(false);
        builder.setTitle(getResources().getString(R.string.mantra_client_management_install))
                .setMessage(getResources().getString(R.string.mantra))
                .setPositiveButton(getResources().getString(R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        final String appPackageName = "com.mantra.clientmanagement"; // getPackageName() from Context or Activity object
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                })
                .show();
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    @Override
    public void checkBalanceEnquiryStatus(String status, String message, BalanceEnquiryResponse balanceEnquiryResponse) {
      //  Toast.makeText(getActivity(), "status ::: " + status, Toast.LENGTH_SHORT).show();
     //   Toast.makeText(getActivity(), "message ::: " + message, Toast.LENGTH_SHORT).show();
        String aadhar = aadhar_no.getText().toString().trim();
        //releaseData ();
        TransactionStatusModel transactionStatusModel = new TransactionStatusModel();
        if (balanceEnquiryResponse != null) {
            transactionStatusModel.setAadharCard(aadhar);
            transactionStatusModel.setBankName(balanceEnquiryResponse.getBankName());
            transactionStatusModel.setBalanceAmount(balanceEnquiryResponse.getBalance());
            transactionStatusModel.setReferenceNo(balanceEnquiryResponse.getReferenceNo());
            transactionStatusModel.setTransactionType("Balance Enquery");
            transactionStatusModel.setStatus(balanceEnquiryResponse.getStatus());
            transactionStatusModel.setApiComment(balanceEnquiryResponse.getApiComment());
            transactionStatusModel.setStatusDesc(balanceEnquiryResponse.getStatusDesc());
        } else {
            transactionStatusModel = null;

        }
        Intent intent = new Intent(getActivity(), TransactionStatusActivity.class);
        intent.putExtra(Constants.TRANSACTION_STATUS_KEY, transactionStatusModel);
        startActivityForResult(intent, Constants.BALANCE_RELOAD);
    }

    @Override
    public void checkCashWithdrawalStatus(String status, String message, CashWithdrawalResponse cashWithdrawalResponse) {

    //    Toast.makeText(getActivity(), "status ::: " + status, Toast.LENGTH_SHORT).show();
   //     Toast.makeText(getActivity(), "message ::: " + message, Toast.LENGTH_SHORT).show();

        String aadhar = aadhar_no.getText().toString().trim();
        String amount = checkCashWithdrawalamount.getText().toString().trim();

        //releaseData ();
        TransactionStatusModel transactionStatusModel = new TransactionStatusModel();
        if (cashWithdrawalResponse != null) {
            transactionStatusModel.setAadharCard(aadhar);
            transactionStatusModel.setBankName(cashWithdrawalResponse.getBankName());
            transactionStatusModel.setBalanceAmount(cashWithdrawalResponse.getBalance());
            transactionStatusModel.setReferenceNo(cashWithdrawalResponse.getReferenceNo());
            transactionStatusModel.setTransactionAmount(amount);
            transactionStatusModel.setTransactionType("Cash Withdrawal");
            transactionStatusModel.setStatus(cashWithdrawalResponse.getStatus());
            transactionStatusModel.setApiComment(cashWithdrawalResponse.getApiComment());
            transactionStatusModel.setStatusDesc(cashWithdrawalResponse.getStatusDesc());
            //session.setFreshnessFactor ( cashWithdrawalResponse.getNextFreshnessFactor () );
        } else {
            transactionStatusModel = null;
            // session.setFreshnessFactor ( null );
        }
        Intent intent = new Intent(getActivity(), TransactionStatusActivity.class);
        intent.putExtra(Constants.TRANSACTION_STATUS_KEY, transactionStatusModel);
        startActivityForResult(intent, Constants.BALANCE_RELOAD);
    }

    @Override
    public void checkEmptyFields() {

    }

    @Override
    public void showLoader() {
       /* if (loadingView == null) {
        loadingView = showProgress(getActivity());
        }*/
        loadingView.show();
    }

    @Override
    public void hideLoader() {
        // if (loadingView != null) {
        loadingView.dismiss();
        // }
    }

    private String generateTXN() {
        try {
            Date tempDate = Calendar.getInstance().getTime();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.ENGLISH);
            String dTTXN = formatter.format(tempDate);
            return dTTXN;
        } catch (Exception e) {
            return "";
        }
    }

    private String getAuthURL(String UID) {
        String url = "http://developer.uidai.gov.in/auth/";
        url += "public/" + UID.charAt(0) + "/" + UID.charAt(1) + "/";
        url += "MG41KIrkk5moCkcO8w-2fc01-P7I5S-6X2-X7luVcDgZyOa2LXs3ELI"; //ASA
        return url;
    }

    private class AuthRequest extends AsyncTask<Void, Void, String> {

        private String uid;
        private PidData pidData;
        private ProgressDialog dialog;
        private int posFingerFormat = 0;
        Meta meta;
        AuthReq authReq;
        DeviceInfo info;

        private AuthRequest(String uid, PidData pidData) {
            this.uid = uid;
            this.pidData = pidData;
            dialog = new ProgressDialog(getActivity());
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                info = pidData._DeviceInfo;

                Uses uses = new Uses();
                uses.pi = "n";
                uses.pa = "n";
                uses.pfa = "n";
                uses.bio = "y";
                if (posFingerFormat == 1) {
                    uses.bt = "FIR";
                } else {
                    uses.bt = "FMR";
                }
                uses.pin = "n";
                uses.otp = "n";

                meta = new Meta();
                meta.udc = "MANT0";
                meta.rdsId = info.rdsId;
                meta.rdsVer = info.rdsVer;
                meta.dpId = info.dpId;
                meta.dc = info.dc;
                meta.mi = info.mi;
                meta.mc = info.mc;

                authReq = new AuthReq();
                authReq.uid = uid;
                authReq.rc = "Y";
                authReq.tid = "registered";
                authReq.ac = "public";
                authReq.sa = "public";
                authReq.ver = "2.0";
                authReq.txn = generateTXN();
                authReq.lk = "MEaMX8fkRa6PqsqK6wGMrEXcXFl_oXHA-YuknI2uf0gKgZ80HaZgG3A"; //AUA
                authReq.skey = pidData._Skey;
                authReq.Hmac = pidData._Hmac;
                authReq.data = pidData._Data;
                authReq.meta = meta;
                authReq.uses = uses;
                authReq.freshnessFactor = "NEXT_FRESHNESS_FACTOR";//session.getFreshnessFactor();

                StringWriter writer = new StringWriter();
                serializer.write(authReq, writer);
                String pass = "public";
                String reqXML = writer.toString();
                String signAuthXML = XMLSigner.generateSignXML(reqXML, getActivity().getAssets().open("staging_signature_privateKey.p12"), pass);
                URL url = new URL(getAuthURL(uid));
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(30000);
                conn.setConnectTimeout(30000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/xml");
                conn.setUseCaches(false);
                conn.setDefaultUseCaches(false);
                OutputStreamWriter writer2 = new OutputStreamWriter(conn.getOutputStream());
                writer2.write(signAuthXML);
                writer2.flush();
                conn.connect();

                StringBuilder sb = new StringBuilder();
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String response;
                while ((response = reader.readLine()) != null) {
                    sb.append(response).append("\n");
                }
                response = sb.toString();

                AuthRes authRes = serializer.read(AuthRes.class, response);
                String res;
                if (authRes.err != null) {
                    if (authRes.err.equals("0")) {
                        res = "Authentication Success" + "\n"
                                + "Auth Response: " + authRes.ret.toUpperCase() + "\n"
                                + "TXN: " + authRes.txn + "\n"
                                + "";
                    } else {
                        res = "Error Code: " + authRes.err + "\n"
                                + "Auth Response: " + authRes.ret.toUpperCase() + "\n"
                                + "TXN: " + authRes.txn + "\n"
                                + "";
                    }
                } else {
                    res = "Authentication Success" + "\n"
                            + "Auth Response: " + authRes.ret.toUpperCase() + "\n"
                            + "TXN: " + authRes.txn + "\n"
                            + "";
                }
                return res;
            } catch (Exception e) {
                return "Error: " + e.toString();
            }
        }

        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);
            if (res != null && authReq != null && meta != null && info != null) {

                String vid = null;
                String uid = null;

                if (aadhar_number.isChecked()) {
                    uid = aadhar_no.getText().toString();
                    if (uid.contains("-")) {
                        uid = uid.replaceAll("-", "").trim();
                    }
                }

                if (virtual_id.isChecked()) {
                    vid = aadhar_no.getText().toString();
                    if (vid.contains("-")) {
                        vid = vid.replaceAll("-", "").trim();
                    }
                }
                balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("", uid, vid, authReq.skey.ci, meta.dc, "", meta.dpId, authReq.data.value, authReq.freshnessFactor, authReq.Hmac, cash_withdrawal_nnid, meta.mc, meta.mi, mobile_no.getText().toString().trim(), "", meta.rdsId, meta.rdsVer, authReq.skey.value.toString());
                balanceEnquiryPresenter = new BalanceEnquiryPresenter(AEPSFragment.this);
                balanceEnquiryPresenter.performBalanceEnquiry(_token, balanceEnquiryRequestModel);
            } else {
                Util.showAlert(getActivity(),getResources().getString(R.string.alert_error),getResources().getString(R.string.scan_finger_alert_error));
            }
        }
    }

    private class AuthCashRequest extends AsyncTask<Void, Void, String> {

        private String uid;
        private PidData pidData;
        private ProgressDialog dialog;
        private int posFingerFormat = 0;
        Meta meta;
        AuthReq authReq;
        DeviceInfo info;

        private AuthCashRequest(String uid, PidData pidData) {
            this.uid = uid;
            this.pidData = pidData;
            dialog = new ProgressDialog(getActivity());
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                info = pidData._DeviceInfo;

                Uses uses = new Uses();
                uses.pi = "n";
                uses.pa = "n";
                uses.pfa = "n";
                uses.bio = "y";
                if (posFingerFormat == 1) {
                    uses.bt = "FIR";
                } else {
                    uses.bt = "FMR";
                }
                uses.pin = "n";
                uses.otp = "n";

                meta = new Meta();
                meta.udc = "MANT0";
                meta.rdsId = info.rdsId;
                meta.rdsVer = info.rdsVer;
                meta.dpId = info.dpId;
                meta.dc = info.dc;
                meta.mi = info.mi;
                meta.mc = info.mc;

                authReq = new AuthReq();
                authReq.uid = uid;
                authReq.rc = "Y";
                authReq.tid = "registered";
                authReq.ac = "public";
                authReq.sa = "public";
                authReq.ver = "2.0";
                authReq.txn = generateTXN();
                authReq.lk = "MEaMX8fkRa6PqsqK6wGMrEXcXFl_oXHA-YuknI2uf0gKgZ80HaZgG3A"; //AUA
                authReq.skey = pidData._Skey;
                authReq.Hmac = pidData._Hmac;
                authReq.data = pidData._Data;
                authReq.meta = meta;
                authReq.uses = uses;
                authReq.freshnessFactor = "NEXT_FRESHNESS_FACTOR";//session.getFreshnessFactor();

                StringWriter writer = new StringWriter();
                serializer.write(authReq, writer);
                String pass = "public";
                String reqXML = writer.toString();
                String signAuthXML = XMLSigner.generateSignXML(reqXML, getActivity().getAssets().open("staging_signature_privateKey.p12"), pass);
                URL url = new URL(getAuthURL(uid));
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(30000);
                conn.setConnectTimeout(30000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/xml");
                conn.setUseCaches(false);
                conn.setDefaultUseCaches(false);
                OutputStreamWriter writer2 = new OutputStreamWriter(conn.getOutputStream());
                writer2.write(signAuthXML);
                writer2.flush();
                conn.connect();

                StringBuilder sb = new StringBuilder();
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String response;
                while ((response = reader.readLine()) != null) {
                    sb.append(response).append("\n");
                }
                response = sb.toString();

                AuthRes authRes = serializer.read(AuthRes.class, response);
                String res;
                if (authRes.err != null) {
                    if (authRes.err.equals("0")) {
                        res = "Authentication Success" + "\n"
                                + "Auth Response: " + authRes.ret.toUpperCase() + "\n"
                                + "TXN: " + authRes.txn + "\n"
                                + "";
                    } else {
                        res = "Error Code: " + authRes.err + "\n"
                                + "Auth Response: " + authRes.ret.toUpperCase() + "\n"
                                + "TXN: " + authRes.txn + "\n"
                                + "";
                    }
                } else {
                    res = "Authentication Success" + "\n"
                            + "Auth Response: " + authRes.ret.toUpperCase() + "\n"
                            + "TXN: " + authRes.txn + "\n"
                            + "";
                }
                return res;
            } catch (Exception e) {
                return "Error: " + e.toString();
            }
        }

        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);
            if (res != null && authReq != null && meta != null && info != null) {

                String vid = null;
                String uid = null;

                if (aadhar_number.isChecked()) {
                    uid = aadhar_no.getText().toString();
                    if (uid.contains("-")) {
                        uid = uid.replaceAll("-", "").trim();
                    }
                }

                if (virtual_id.isChecked()) {
                    vid = aadhar_no.getText().toString();
                    if (vid.contains("-")) {
                        vid = vid.replaceAll("-", "").trim();
                    }
                }

                cashWithdrawalRequestModel = new CashWithdrawalRequestModel(checkCashWithdrawalamount.getText().toString().trim(), uid, vid, authReq.skey.ci, meta.dc, "", meta.dpId, authReq.data.value, authReq.freshnessFactor, authReq.Hmac, cash_withdrawal_nnid, meta.mc, meta.mi, mobile_no.getText().toString().trim(), "WITHDRAW", meta.rdsId, meta.rdsVer, authReq.skey.value.toString());
                cashWithdrawalPresenter = new CashWithdrawalPresenter(AEPSFragment.this);
                cashWithdrawalPresenter.performCashWithdrawal(_token, cashWithdrawalRequestModel);

            } else {
                Util.showAlert(getActivity(),getResources().getString(R.string.alert_error),getResources().getString(R.string.scan_finger_alert_error));
            }
        }
    }

    private class BalanceEnqAuthRequestMorpho extends AsyncTask<Void, Void, String> {

/*        private String uid;
        private MorphoPidData morphoPidData;
        private ProgressDialog dialog;
        private int posFingerFormat = 0;
        Meta meta;
        AuthReq authReq;
        MorphoDeviceInfo morphoDeviceInfo ;*/

        private String uid;
        private MorphoPidData morphoPidData;
        private ProgressDialog dialog;
        private int posFingerFormat = 0;
        Meta meta;
        AuthReq authReq;
        MorphoDeviceInfo morphoDeviceInfo ;


        private BalanceEnqAuthRequestMorpho(String uid, MorphoPidData morphoPidData) {
            this.uid = uid;
            this.morphoPidData = morphoPidData;
            dialog = new ProgressDialog(getActivity());
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {

            try {
                morphoDeviceInfo = morphoPidData._DeviceInfo;

                Uses uses = new Uses();
                uses.pi = "n";
                uses.pa = "n";
                uses.pfa = "n";
                uses.bio = "y";
                if (posFingerFormat == 1) {
                    uses.bt = "FIR";
                } else {
                    uses.bt = "FMR";
                }
                uses.pin = "n";
                uses.otp = "n";

                meta = new Meta();
                meta.udc = "MANT0";
                meta.rdsId = morphoDeviceInfo.rdsId;
                meta.rdsVer = morphoDeviceInfo.rdsVer;
                meta.dpId = morphoDeviceInfo.dpId;
                meta.dc = morphoDeviceInfo.dc;
                meta.mi = morphoDeviceInfo.mi;
                meta.mc = morphoDeviceInfo.mc;


                authReq = new AuthReq();
                authReq.uid = uid;
                authReq.rc = "Y";
                authReq.tid = "registered";
                authReq.ac = "public";
                authReq.sa = "public";
                authReq.ver = "2.0";
                authReq.txn = generateTXN();
                authReq.lk = "MEaMX8fkRa6PqsqK6wGMrEXcXFl_oXHA-YuknI2uf0gKgZ80HaZgG3A"; //AUA
                authReq.skey = morphoPidData._Skey;
                authReq.Hmac = morphoPidData._Hmac;
                authReq.data = morphoPidData._Data;
                authReq.meta = meta;
                authReq.uses = uses;
                authReq.freshnessFactor = "NEXT_FRESHNESS_FACTOR";

                StringWriter writer = new StringWriter();
                serializer.write(authReq, writer);
                String pass = "public";
                String reqXML = writer.toString();
                String signAuthXML = XMLSigner.generateSignXML(reqXML, getActivity().getAssets().open("staging_signature_privateKey.p12"), pass);
                URL url = new URL(getAuthURL(uid));
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(30000);
                conn.setConnectTimeout(30000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/xml");
                conn.setUseCaches(false);
                conn.setDefaultUseCaches(false);
                OutputStreamWriter writer2 = new OutputStreamWriter(conn.getOutputStream());
                writer2.write(signAuthXML);
                writer2.flush();
                conn.connect();

                StringBuilder sb = new StringBuilder();
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String response;
                while ((response = reader.readLine()) != null) {
                    sb.append(response).append("\n");
                }
                response = sb.toString();

                AuthRes authRes = serializer.read(AuthRes.class, response);
                String res;
                if (authRes.err != null) {
                    if (authRes.err.equals("0")) {
                        res = "Authentication Success" + "\n"
                                + "Auth Response: " + authRes.ret.toUpperCase() + "\n"
                                + "TXN: " + authRes.txn + "\n"
                                + "";
                    } else {
                        res = "Error Code: " + authRes.err + "\n"
                                + "Auth Response: " + authRes.ret.toUpperCase() + "\n"
                                + "TXN: " + authRes.txn + "\n"
                                + "";
                    }
                } else {
                    res = "Authentication Success" + "\n"
                            + "Auth Response: " + authRes.ret.toUpperCase() + "\n"
                            + "TXN: " + authRes.txn + "\n"
                            + "";
                }
                return res;
            } catch (Exception e) {
                return "Error: " + e.toString();
            }


    }

        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);
            if (res != null && authReq != null && meta != null && morphoDeviceInfo != null) {

                String vid = null;
                String uid = null;

                if (aadhar_number.isChecked()) {
                    uid = aadhar_no.getText().toString();
                    if (uid.contains("-")) {
                        uid = uid.replaceAll("-", "").trim();
                    }
                }

                if (virtual_id.isChecked()) {
                    vid = aadhar_no.getText().toString();
                    if (vid.contains("-")) {
                        vid = vid.replaceAll("-", "").trim();
                    }
                }

                balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("", uid,vid, authReq.skey.ci, meta.dc, "", meta.dpId, authReq.data.value, authReq.freshnessFactor, authReq.Hmac, cash_withdrawal_nnid, meta.mc, meta.mi, mobile_no.getText().toString().trim(), "", meta.rdsId, meta.rdsVer, authReq.skey.value.toString());
                balanceEnquiryPresenter = new BalanceEnquiryPresenter(AEPSFragment.this);
                balanceEnquiryPresenter.performBalanceEnquiry(_token, balanceEnquiryRequestModel);

            }
        }
    }

    private class CashWithdrawalAuthRequestMorpho extends AsyncTask<Void, Void, String> {

        private String uid;
        private MorphoPidData morphoPidData;
        private ProgressDialog dialog;
        private int posFingerFormat = 0;
        Meta meta;
        AuthReq authReq;
        MorphoDeviceInfo morphoDeviceInfo ;

        private CashWithdrawalAuthRequestMorpho(String uid, MorphoPidData morphoPidData) {
            this.uid = uid;
            this.morphoPidData = morphoPidData;
            dialog = new ProgressDialog(getActivity());
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {

            try {
                morphoDeviceInfo = morphoPidData._DeviceInfo;

                Uses uses = new Uses();
                uses.pi = "n";
                uses.pa = "n";
                uses.pfa = "n";
                uses.bio = "y";
                if (posFingerFormat == 1) {
                    uses.bt = "FIR";
                } else {
                    uses.bt = "FMR";
                }
                uses.pin = "n";
                uses.otp = "n";

                meta = new Meta();
                meta.udc = "MANT0";
                meta.rdsId = morphoDeviceInfo.rdsId;
                meta.rdsVer = morphoDeviceInfo.rdsVer;
                meta.dpId = morphoDeviceInfo.dpId;
                meta.dc = morphoDeviceInfo.dc;
                meta.mi = morphoDeviceInfo.mi;
                meta.mc = morphoDeviceInfo.mc;


                authReq = new AuthReq();
                authReq.uid = uid;
                authReq.rc = "Y";
                authReq.tid = "registered";
                authReq.ac = "public";
                authReq.sa = "public";
                authReq.ver = "2.0";
                authReq.txn = generateTXN();
                authReq.lk = "MEaMX8fkRa6PqsqK6wGMrEXcXFl_oXHA-YuknI2uf0gKgZ80HaZgG3A"; //AUA
                authReq.skey = morphoPidData._Skey;
                authReq.Hmac = morphoPidData._Hmac;
                authReq.data = morphoPidData._Data;
                authReq.meta = meta;
                authReq.uses = uses;
                authReq.freshnessFactor = "NEXT_FRESHNESS_FACTOR";

                StringWriter writer = new StringWriter();
                serializer.write(authReq, writer);
                String pass = "public";
                String reqXML = writer.toString();
                String signAuthXML = XMLSigner.generateSignXML(reqXML, getActivity().getAssets().open("staging_signature_privateKey.p12"), pass);
                URL url = new URL(getAuthURL(uid));
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(30000);
                conn.setConnectTimeout(30000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/xml");
                conn.setUseCaches(false);
                conn.setDefaultUseCaches(false);
                OutputStreamWriter writer2 = new OutputStreamWriter(conn.getOutputStream());
                writer2.write(signAuthXML);
                writer2.flush();
                conn.connect();

                StringBuilder sb = new StringBuilder();
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String response;
                while ((response = reader.readLine()) != null) {
                    sb.append(response).append("\n");
                }
                response = sb.toString();

                AuthRes authRes = serializer.read(AuthRes.class, response);
                String res;
                if (authRes.err != null) {
                    if (authRes.err.equals("0")) {
                        res = "Authentication Success" + "\n"
                                + "Auth Response: " + authRes.ret.toUpperCase() + "\n"
                                + "TXN: " + authRes.txn + "\n"
                                + "";
                    } else {
                        res = "Error Code: " + authRes.err + "\n"
                                + "Auth Response: " + authRes.ret.toUpperCase() + "\n"
                                + "TXN: " + authRes.txn + "\n"
                                + "";
                    }
                } else {
                    res = "Authentication Success" + "\n"
                            + "Auth Response: " + authRes.ret.toUpperCase() + "\n"
                            + "TXN: " + authRes.txn + "\n"
                            + "";
                }
                return res;
            } catch (Exception e) {
                return "Error: " + e.toString();
            }
        }

        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);
            if (res != null && authReq != null && meta != null && morphoDeviceInfo != null) {

                String vid = null;
                String uid = null;

                if (aadhar_number.isChecked()) {
                    uid = aadhar_no.getText().toString();
                    if (uid.contains("-")) {
                        uid = uid.replaceAll("-", "").trim();
                    }
                }

                if (virtual_id.isChecked()) {
                    vid = aadhar_no.getText().toString();
                    if (vid.contains("-")) {
                        vid = vid.replaceAll("-", "").trim();
                    }
                }

                cashWithdrawalRequestModel = new CashWithdrawalRequestModel(checkCashWithdrawalamount.getText().toString().trim(), uid, vid, authReq.skey.ci, meta.dc, "", meta.dpId, authReq.data.value, authReq.freshnessFactor, authReq.Hmac, cash_withdrawal_nnid, meta.mc, meta.mi, mobile_no.getText().toString().trim(), "WITHDRAW", meta.rdsId, meta.rdsVer, authReq.skey.value.toString());
                cashWithdrawalPresenter = new CashWithdrawalPresenter(AEPSFragment.this);
                cashWithdrawalPresenter.performCashWithdrawal(_token, cashWithdrawalRequestModel);

            }
        }
    }



    @Override
    public void onResume() {
        super.onResume();
        hideKeyboard();
    }

    public void hideKeyboard() {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public void releaseDataCashWithdrawal() {

        aadhar_number.setChecked(false);
        virtual_id.setChecked(false);

        mobile_no.setText(null);
        mobile_no.setError(null);

        aadhar_no.setText(null);
        aadhar_no.setError(null);

        checkCashWithdrawalamount.setText(null);
        checkCashWithdrawalamount.setError(null);


        balance_enquiry_nnid = "";
        cash_withdrawal_nnid = "";
        pidData = null;
        morphoPidData = null;

        //        amountEnter.setText(null);
//        amountEnter.setError(null);
//        aadharNumber.setText(null);
//        aadharNumber.setError(null);
//
//        mobileNumber.setText(null);
//        mobileNumber.setError(null);
//
//        bankspinner.setText(null);
//        bankspinner.setError(null);
//
//        withdrawalAmountEnter.setText(null);
//        withdrawalAmountEnter.setError(null);
//
//        withdrawalAadharNumber.setText(null);
//        withdrawalAadharNumber.setError(null);
//
//        withdrawalMobileNumber.setText(null);
//        withdrawalMobileNumber.setError(null);
//
//        withdrawalBankspinner.setText(null);
//        withdrawalBankspinner.setError(null);
//
//
//
//        balanceAadharNumber.setText(null);
//        balanceAadharNumber.setError(null);
//
//        balanceMobileNumber.setText(null);
//        balanceMobileNumber.setError(null);
//
//        balanceBankspinner.setText(null);
//        balanceBankspinner.setError(null);
//
//
//        bankIINNumber = "";
//
//        apiTidNumber.setText(null);
//        apiTidNumber.setError(null);
//
//        tag = "";
//        pidData=null;
//        morphoPidData=null;
//        balanceEnquiryRequestModel = null;
//        cashDepositRequestModel = null;
//        cashWithdrawalRequestModel = null;
//        refundRequestModel = null;
//        withdrawalBar.setVisibility ( View.GONE );
//        balanceEnqureyBar.setVisibility ( View.GONE );
//        withdrawalNote.setVisibility ( View.GONE );
//        fingerprintStrengthWithdrawal.setVisibility ( View.GONE );
//        fingerprintStrengthBalance.setVisibility ( View.GONE );
//        balanceNote.setVisibility ( View.GONE );
//        depositBar.setVisibility ( View.GONE );
//        depositNote.setVisibility ( View.GONE );
//        fingerprintStrengthDeposit.setVisibility ( View.GONE );
    }

    public void releaseDataBalanceEnquire() {

        balance_enquiry_aadhar_number.setChecked(false);
        balance_enquiry_virtual_id.setChecked(false);

        balance_enquiry_mobile_no.setText(null);
        balance_enquiry_mobile_no.setError(null);

        balance_enquiry_aadhar_no.setText(null);
        balance_enquiry_aadhar_no.setError(null);

        balance_enquiry_nnid = "";
        cash_withdrawal_nnid = "";
        pidData = null;
        morphoPidData = null;

        //        amountEnter.setText(null);
//        amountEnter.setError(null);
//        aadharNumber.setText(null);
//        aadharNumber.setError(null);
//
//        mobileNumber.setText(null);
//        mobileNumber.setError(null);
//
//        bankspinner.setText(null);
//        bankspinner.setError(null);
//
//        withdrawalAmountEnter.setText(null);
//        withdrawalAmountEnter.setError(null);
//
//        withdrawalAadharNumber.setText(null);
//        withdrawalAadharNumber.setError(null);
//
//        withdrawalMobileNumber.setText(null);
//        withdrawalMobileNumber.setError(null);
//
//        withdrawalBankspinner.setText(null);
//        withdrawalBankspinner.setError(null);
//
//
//
//        balanceAadharNumber.setText(null);
//        balanceAadharNumber.setError(null);
//
//        balanceMobileNumber.setText(null);
//        balanceMobileNumber.setError(null);
//
//        balanceBankspinner.setText(null);
//        balanceBankspinner.setError(null);
//
//
//        bankIINNumber = "";
//
//        apiTidNumber.setText(null);
//        apiTidNumber.setError(null);
//
//        tag = "";
//        pidData=null;
//        morphoPidData=null;
//        balanceEnquiryRequestModel = null;
//        cashDepositRequestModel = null;
//        cashWithdrawalRequestModel = null;
//        refundRequestModel = null;
//        withdrawalBar.setVisibility ( View.GONE );
//        balanceEnqureyBar.setVisibility ( View.GONE );
//        withdrawalNote.setVisibility ( View.GONE );
//        fingerprintStrengthWithdrawal.setVisibility ( View.GONE );
//        fingerprintStrengthBalance.setVisibility ( View.GONE );
//        balanceNote.setVisibility ( View.GONE );
//        depositBar.setVisibility ( View.GONE );
//        depositNote.setVisibility ( View.GONE );
//        fingerprintStrengthDeposit.setVisibility ( View.GONE );

    }

}
