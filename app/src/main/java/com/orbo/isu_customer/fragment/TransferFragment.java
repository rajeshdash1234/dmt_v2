package com.orbo.isu_customer.fragment;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.isu.dmt.R;
import com.orbo.isu_customer.activity.SessionManager;
import com.orbo.isu_customer.bankspinner.DMTBankNameActivity;
import com.orbo.isu_customer.utility.CommonUtil;
import com.orbo.isu_customer.utility.Constants;
import com.orbo.isu_customer.utills.Currency;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.app.Activity.RESULT_OK;
import static com.orbo.isu_customer.utility.AppController.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class TransferFragment extends BottomSheetDialogFragment {
    ImageView imageView, addbene;
    CardView nav_view;
    LinearLayout bank, loadmore, wallet_content_one, wallet_content_two, wallet_content_three, main_container, new_customer, verify_number, add_new_bene, single_bene_bank,bene_ll;
    Button load_more, enroll_number, resend_otp, verify, add_bene, create_add_bene,verify_bene, addAndPayBtn;
    EditText mobile_number, amount_transfer, ifc_code, customer_name, otp;
    EditText  acc_no, ifccode, bene_name;
    RecyclerView benelistData;
   // AutoCompleteTextView bank_name_autocomplete;
    TextView bank_name_autocomplete;
    SessionManager session;
    TextView bankname,wallet_balance_one, wallet_balance_two, wallet_balance_three, wallet_one, wallet_two, wallet_three,amount_to_transfer;
    String _token;
    ArrayList<HashMap<String, String>> beny_list;
    int wallet_selected = 1;
    String pipeNo = "1",beneId,customerId,_amount_transfer,uid = "f0oo",transactionMode = "NEFT";

    ProgressBar progressBar, progressBar_main;
    private ArrayList<String>listOfBank = new ArrayList<>();
    final HashMap<String, List<BankListModel>> bankDataMap = new HashMap<>();
    ArrayList<BankListModel> bankDetailList  = new ArrayList<>();
    String selectedBank="",getAccountNo="";
    Activity activity;

    ArrayList<String> mobile_number_list = new ArrayList<>();

    SharedPreferences sharedpreferences;

    //ArrayAdapter<String> _adapter;
    //AutoCompleteTextView actv;

    ArrayList<HashMap<String, String>> mList = new ArrayList<>();
    BeneAdapter adapter;

    BeneRecycleAdapter bebeAdapter;
    ArrayList<BeneModel> beneList = new ArrayList<BeneModel>();
    TransferFragment tfrag;

    Button show_recept;
    TextView selectedBeneName,amouttxt;
    ProgressBar progress_bene;
    ProgressDialog pd;
    RadioGroup rg_add_bene,rg_select_bene;
    RadioButton rb_select_neft,rb_select_imps,rb_add_imps,rb_add_neft;


    public TransferFragment(Activity activty) {
        // Required empty public constructor
        this.activity = activty;
        tfrag = this;
      //  FirebaseApp.initializeApp(activity);
    }


    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) dialogInterface;
                setupFullHeight(bottomSheetDialog);
            }
        });

        return dialog;
    }

    private void setupFullHeight(BottomSheetDialog bottomSheetDialog) {
        FrameLayout bottomSheet = (FrameLayout) bottomSheetDialog.findViewById(R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        ViewGroup.LayoutParams layoutParams = bottomSheet.getLayoutParams();

        int windowHeight = getWindowHeight();

        if (layoutParams != null) {
            layoutParams.height = windowHeight;
        }

        bottomSheet.setLayoutParams(layoutParams);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    private int getWindowHeight() {
        // Calculate window height for fullscreen use
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) getContext()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

       /* sharedpreferences = getActivity().getSharedPreferences("language", Context.MODE_PRIVATE);
        String lang  = sharedpreferences.getString("lang","en");
        Locale locale = new Locale(lang);

        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;

        getActivity().getBaseContext().getResources().updateConfiguration(config, getActivity().getBaseContext().getResources().getDisplayMetrics());
*/
        View view = inflater.inflate(R.layout.fragment_transfer, container, false);

        bankname = view.findViewById(R.id.bankname);

        imageView = view.findViewById(R.id.transfer_nav);
        addbene = view.findViewById(R.id.addbene);

        bank = view.findViewById(R.id.bank);

        loadmore = view.findViewById(R.id.loadmore);
        load_more = view.findViewById(R.id.submit);

        mobile_number = view.findViewById(R.id.mobile_number);
        amount_transfer = view.findViewById(R.id.amount_transfer);
        ifc_code = view.findViewById(R.id.ifc_code);
        benelistData = view.findViewById(R.id.benelistData);
        benelistData.setLayoutManager(new LinearLayoutManager(getActivity()));


        wallet_content_one = view.findViewById(R.id.wallet_content_one);
        wallet_content_two = view.findViewById(R.id.wallet_content_two);
        wallet_content_three = view.findViewById(R.id.wallet_content_three);

        wallet_balance_one = view.findViewById(R.id.wallet_balance_one);
        wallet_balance_two = view.findViewById(R.id.wallet_balance_two);
        wallet_balance_three = view.findViewById(R.id.wallet_balance_three);

        wallet_one = view.findViewById(R.id.wallet_one);
        wallet_two = view.findViewById(R.id.wallet_two);
        wallet_three = view.findViewById(R.id.wallet_three);

        main_container = view.findViewById(R.id.main_container);

        progressBar = view.findViewById(R.id.progressBar);

        progressBar_main = view.findViewById(R.id.progressBar_main);


        new_customer = view.findViewById(R.id.new_customer);

        customer_name = view.findViewById(R.id.customer_name);
        enroll_number = view.findViewById(R.id.enroll_number);

        otp = view.findViewById(R.id.otp);
        verify_number = view.findViewById(R.id.verify_number);
        resend_otp = view.findViewById(R.id.resend_otp);
        verify = view.findViewById(R.id.verify);

        add_bene = view.findViewById(R.id.add_bene);
        add_new_bene = view.findViewById(R.id.add_new_bene);
        single_bene_bank = view.findViewById(R.id.single_bene_bank);

        create_add_bene = view.findViewById(R.id.create_add_bene);

        bank_name_autocomplete = view.findViewById(R.id.bank_name_autocomplete);

        acc_no = view.findViewById(R.id.acc_no);
        ifccode = view.findViewById(R.id.ifccode);
        bene_name = view.findViewById(R.id.bene_name);
        amount_to_transfer = view.findViewById(R.id.amount_to_transfer);

        bene_ll = view.findViewById(R.id.bene_ll);
        show_recept = view.findViewById(R.id.show_recept);

        selectedBeneName = view.findViewById(R.id.selectedBeneName);
        verify_bene = view.findViewById(R.id.verify_bene);

        progress_bene = view.findViewById(R.id.progress_bene);
        addAndPayBtn = view.findViewById(R.id.addAndPayBtn);


        rg_select_bene = view.findViewById(R.id.rg_select_bene);
        rb_select_neft = view.findViewById(R.id.rb_select_neft);
        rb_select_imps = view.findViewById(R.id.rb_select_imps);

        rg_add_bene = view.findViewById(R.id.rg_add_bene);
        rb_add_imps = view.findViewById(R.id.rb_add_imps);
        rb_add_neft = view.findViewById(R.id.rb_add_neft);

        amouttxt = view.findViewById(R.id.amouttxt);



        beny_list = new ArrayList<>();


        rg_add_bene.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId)
                {

                    case R.id.rb_add_imps:
                        transactionMode = "IMPS";
                        break;
                    case R.id.rb_add_neft:
                        transactionMode = "NEFT";
                        break;
                }
            }
        });

        rg_select_bene.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId)
                {

                    case R.id.rb_select_neft:
                        transactionMode = "IMPS";
                        break;
                    case R.id.rb_select_imps:
                        transactionMode = "NEFT";
                        break;
                }
            }
        });



        //-----Firestore Data base access bank list
        //retriveBankList();


        beny_list = new ArrayList<>();

        session = new SessionManager(getActivity());
        HashMap<String, String> user = session.getUserSession();
        String _userName = user.get(session.userName);
        String _userType = user.get(session.userType);
        String _userBalance = user.get(session.userBalance);
        String _adminName = user.get(session.adminName);
        String _mposNumber = user.get(session.mposNumber);

        HashMap<String, String> _user = session.getUserDetails();
        _token = _user.get(SessionManager.KEY_TOKEN);

        bank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadmore.setVisibility(View.VISIBLE);
                load_more.setVisibility(View.VISIBLE);
            }
        });

        create_add_bene.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String _bank_name = bank_name_autocomplete.getText().toString();
                String _ifccode = acc_no.getText().toString();
                String _acc_no = ifccode.getText().toString();
                String _bene_name = bene_name.getText().toString();

                if (_bank_name != null && _acc_no != null && _ifccode != null && _bene_name != null) {

                    createBene(_bank_name, _ifccode, _acc_no, _bene_name, mobile_number.getText().toString());
                   // retriveBankList();

                } else {
                    Toast.makeText(getActivity(), "All field required", Toast.LENGTH_SHORT).show();
                }
            }
        });

        verify_bene.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Rajesh
                if(!customerId.equalsIgnoreCase("") || (customerId != null)){
                    doVerifyBene();
                }else{

                }
            }
        });
        addAndPayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Rajesh
                String amountStr = amount_transfer.getText().toString();
                int amount =0;
                if(!amountStr.isEmpty()){
                     amount = Integer.parseInt(amountStr);
                }


                if (amount <= 5000) {
                   // addNPayRequest = new AddNPayRequest(amount, mob, transactionMode, beneNameEnter, ifscCode, bankName, getAccountNo, mob, pipeNo);
                    doAddNPay();
                } else {
                    Toast.makeText(getActivity(), "The amount limit should be maximum Rs.5000 for the new beneficiary account.", Toast.LENGTH_SHORT).show();
                }

            }
        });


        show_recept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bene_ll.setVisibility(View.VISIBLE);
                add_new_bene.setVisibility(View.GONE);
                loadmore.setVisibility(View.GONE);
                load_more.setVisibility(View.GONE);

            }
        });

        add_bene.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_new_bene.setVisibility(View.VISIBLE);
                single_bene_bank.setVisibility(View.GONE);
                bene_ll.setVisibility(View.GONE);
                //retriveBankList();
            }
        });

       /* addbene.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (beny_list.size() > 0) {
                    final Dialog dialog = new Dialog(getActivity());
                    dialog.setContentView(R.layout.custom);
                    dialog.setTitle("BENEFICIARY");

                    ListView listView = dialog.findViewById(R.id.listview);
                    MySimpleArrayAdapter adapter = new MySimpleArrayAdapter(getActivity(), beny_list);
                    listView.setAdapter(adapter);

                    bebeAdapter = new BeneRecycleAdapter(getActivity(), beny_list);
                    benelistData.setAdapter(bebeAdapter);

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            single_bene_bank.setVisibility(View.VISIBLE);

                            String bene_id = beny_list.get(position).get("bene_id");
                            String customerNumber = beny_list.get(position).get("customerNumber");
                            String beneMobileNo = beny_list.get(position).get("beneMobileNo");
                            String beneName = beny_list.get(position).get("beneName");
                            String accountNo = beny_list.get(position).get("accountNo");
                            String bankName = beny_list.get(position).get("bankName");
                            String ifscCode = beny_list.get(position).get("ifscCode");
                            String bene_status = beny_list.get(position).get("bene_status");

                             bankname.setText(bankName);
                             ifc_code.setText(ifscCode);
                             beneId = bene_id;

                             dialog.dismiss();

                        }
                    });

                    dialog.show();
                }

            }
        });*/


        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        load_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String _otp = otp.getText().toString();
                String _mobile_number = mobile_number.getText().toString();

                verifyotp(_otp, _mobile_number);
            }
        });

        resend_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String _mobile_number = mobile_number.getText().toString();

                resendotp(_mobile_number);
            }
        });


        //        Log.i("_userName",_userName);
//        Log.i("_userType",_userType);
//        Log.i("_userBalance","_userBalance");
//        Log.i("_adminName","_adminName");
//        Log.i("_mposNumber","_mposNumber");

        mobile_number.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {

                try {
                    if (count != 10) {
                        main_container.setVisibility(View.GONE);
                        new_customer.setVisibility(View.GONE);
                        verify_number.setVisibility(View.GONE);
                        add_new_bene.setVisibility(View.GONE);
                        single_bene_bank.setVisibility(View.GONE);
                        bene_ll.setVisibility(View.GONE);
                    }
                } catch (Exception exc) {
                    exc.printStackTrace();
                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                Log.i("Mobile Number ::", "" + s.toString());
            }

            public void afterTextChanged(Editable s) {
                String mobile = s.toString();

                if (mobile.length() == 10) {
                    CommonUtil.hideKeyboard(getActivity(),mobile_number);
                    progressBar.setVisibility(View.VISIBLE);
                    loadCustomerDetail(_token, mobile_number.getText().toString());
                   // retriveBankList();
                }
            }
        });

        amount_to_transfer.setText("Amount to transfer Wallet 1");

        wallet_content_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wallet_selected = 1;
                wallet_balance_one.setTextColor(Color.WHITE);
                wallet_balance_two.setTextColor(Color.GRAY);
                wallet_balance_three.setTextColor(Color.GRAY);

                wallet_one.setTextColor(Color.WHITE);
                wallet_two.setTextColor(Color.GRAY);
                wallet_three.setTextColor(Color.GRAY);

                amount_to_transfer.setText("Amount to transfer Wallet 1 ");
            }
        });

        wallet_content_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wallet_selected = 2;
                wallet_balance_two.setTextColor(Color.WHITE);
                wallet_balance_one.setTextColor(Color.GRAY);
                wallet_balance_three.setTextColor(Color.GRAY);

                wallet_one.setTextColor(Color.GRAY);
                wallet_two.setTextColor(Color.WHITE);
                wallet_three.setTextColor(Color.GRAY);
                amount_to_transfer.setText("Amount to transfer Wallet 2 ");

            }
        });

        wallet_content_three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wallet_selected = 3;
                wallet_balance_three.setTextColor(Color.WHITE);
                wallet_balance_two.setTextColor(Color.GRAY);
                wallet_balance_one.setTextColor(Color.GRAY);

                wallet_one.setTextColor(Color.GRAY);
                wallet_two.setTextColor(Color.GRAY);
                wallet_three.setTextColor(Color.WHITE);

                amount_to_transfer.setText("Amount to transfer Wallet 3 ");

            }
        });


        enroll_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verify_number.setVisibility(View.VISIBLE);

                String _customer_name = customer_name.getText().toString();
                String _mobile_number = mobile_number.getText().toString();

                addcustomer(_customer_name, _mobile_number);
            }
        });


        load_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session = new SessionManager(getActivity());
                //String _mobile_no = mobile_number.getText().toString();

                progressBar_main.setVisibility(View.VISIBLE);

                _amount_transfer = amount_transfer.getText().toString();

                if (wallet_selected == 1) {
                    pipeNo = "1";
                } else if (wallet_selected == 2) {
                    pipeNo = "2";
                } else {
                    pipeNo = "3";
                }

                if (_amount_transfer.length() == 0) {
                    Toast.makeText(getActivity(), "Enter Transfer Amount", Toast.LENGTH_SHORT).show();
                } else {
                    HashMap<String, String> params = new HashMap<String, String>();
                    params.put("beneId", beneId);
                    params.put("coustomerId", customerId);
                    params.put("amount", _amount_transfer);
                    params.put("pipeNo", pipeNo);
                    params.put("uid", uid);
                    params.put("transactionMode", transactionMode);

                    RequestQueue mQueue = Volley.newRequestQueue(getActivity());
                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest("http://34.93.39.198:8080/dmt/transferreq", new JSONObject(params),
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    try {
                                        progressBar_main.setVisibility(View.GONE);
                                        Toast.makeText(getActivity(), response.getString("statusDesc"), Toast.LENGTH_SHORT).show();
                                        dismiss();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        progressBar_main.setVisibility(View.GONE);
                                    }
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("TAG", error.getMessage(), error);
                            progressBar_main.setVisibility(View.GONE);
                        }
                    }) { //no semicolon or coma
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("Content-Type", "application/json");
                            params.put("Authorization", _token);
                            return params;
                        }
                    };

                    mQueue.add(jsonObjectRequest);
                }
            }
        });

        amount_transfer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().isEmpty()){
                    amouttxt.setText("");

                }else {

                    String str = Currency.convertToIndianCurrency(s.toString());
                    amouttxt.setText(str);
                }

            }
        });



        //-----------Rajesh
        bank_name_autocomplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DMTBankNameActivity.class);
                intent.putExtra("BANKDATA", listOfBank);
                getActivity().startActivity(intent);
            }
        });





        //-----------Rajesh
/*        bank_name_autocomplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String str = (String) parent.getAdapter().getItem(position);
                selectedBank = str;


                int pos =  listOfBank.indexOf(str);
                String strr =  bankDetailList.get(pos).getBANKCODE();
                Toast.makeText(getActivity(),strr, Toast.LENGTH_SHORT).show();

            }
        });*/
        //------------------Rajesh
        acc_no.addTextChangedListener(new TextWatcher() {
            int length_before = 0;
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                length_before = charSequence.length();
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                getAccountNo = acc_no.getText().toString();

                if(charSequence.toString().contains("-")){
                    getAccountNo = charSequence.toString().replace("-","");
                    System.out.println(">>---->>>>"+getAccountNo);
                }
                //====================================================================================================================================================================//
               // if(activity.getCurrentFocus()==acc_no) {
                    if (charSequence.length() == 0) {
                        ifccode.setText("");
                        bene_name.setText("");
                    }
                        if (!isValidAccount(getAccountNo)) {
                            acc_no.setError("Invalid AccountNo");
                            ifccode.setText("");
                        } else {
                            ifcsFind();
                        }

               // }
            }

            @Override
            public void afterTextChanged(Editable editable) {

               // if(activity.getCurrentFocus()==acc_no) {
                    if (editable.toString().length() == 0) {
                        ifccode.setText("");
                        bene_name.setText("");
                    }
               // }

                if (length_before < editable.length()) {
                    if (editable.length() == 4 || editable.length() == 9 ||editable.length() == 14 || editable.length() == 19)
                        editable.append("-");
                    if (editable.length() > 4) {
                        if (Character.isDigit(editable.charAt(4)))
                            editable.insert(4, "-");
                    }
                    if (editable.length() > 9) {
                        if (Character.isDigit(editable.charAt(9)))
                            editable.insert(9, "-");
                    }
                    if (editable.length() > 14) {
                        if (Character.isDigit(editable.charAt(14)))
                            editable.insert(14, "-");
                    }
                    if (editable.length() > 19) {
                        if (Character.isDigit(editable.charAt(19)))
                            editable.insert(19, "-");
                    }
                }
                if(editable.toString().contains("-")){
                    getAccountNo = editable.toString().replace("-","");
                    System.out.println(">>---->>>>"+getAccountNo);
                }


            }
        });
//-------------------------------------

        //adapter = new BeneAdapter(getActivity(), R.layout.rowlayout, R.id.beneMobileNo, mList);
        //actv = view.findViewById(R.id.autoCompleteTextView);
        //actv.setThreshold(1);
        //actv.setAdapter(adapter);
        //actv.setTextColor(Color.BLACK);

       /* actv.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    if (s.length() != 10) {
                        main_container.setVisibility(View.GONE);
                        new_customer.setVisibility(View.GONE);
                        verify_number.setVisibility(View.GONE);
                        add_new_bene.setVisibility(View.GONE);
                        single_bene_bank.setVisibility(View.GONE);
                    } else {
                        progressBar.setVisibility(View.VISIBLE);
                        loadCustomerDetail(_token, actv.getText().toString());
                    }
                } catch (Exception exc) {
                    exc.printStackTrace();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Log.i("Mobile Number ::", "" + s.toString());
            }

            public void afterTextChanged(Editable s) {
                *//*
               try {
                    if (s.length() != 10) {
                        main_container.setVisibility(View.GONE);
                        new_customer.setVisibility(View.GONE);
                        verify_number.setVisibility(View.GONE);
                        add_new_bene.setVisibility(View.GONE);
                        single_bene_bank.setVisibility(View.GONE);
                    } else {
                        String mobile = actv.getText().toString();

                        if (mobile_number_list.contains(mobile)) {
                            try {
                                single_bene_bank.setVisibility(View.VISIBLE);
                                int position = mobile_number_list.indexOf(mobile);
                                Log.i("position :::", "" + position);
                                 String bene_id = beny_list.get(position).get("bene_id");
                                 String customerNumber = beny_list.get(position).get("customerNumber");
                                String beneMobileNo = beny_list.get(position).get("beneMobileNo");
                                String beneName = beny_list.get(position).get("beneName");
                                String accountNo = beny_list.get(position).get("accountNo");
                                String bankName = beny_list.get(position).get("bankName");
                                String ifscCode = beny_list.get(position).get("ifscCode");
                                String bene_status = beny_list.get(position).get("bene_status");

                                bankname.setText(bankName);
                                ifc_code.setText(ifscCode);
                                beneId = bene_id;
                            } catch (Exception exc) {
                                exc.printStackTrace();
                            }

                        } else {
                            progressBar.setVisibility(View.VISIBLE);
                            loadCustomerDetail(_token, actv.getText().toString());
                        }
                    }
                } catch (Exception exc) {
                    exc.printStackTrace();
                }
                *//*
            }
        });*/

/*        actv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                try {
                    single_bene_bank.setVisibility(View.VISIBLE);
                    String bene_id = beny_list.get(position).get("bene_id");
                    String customerNumber = beny_list.get(position).get("customerNumber");
                    String beneMobileNo = beny_list.get(position).get("beneMobileNo");
                    String beneName = beny_list.get(position).get("beneName");
                    String accountNo = beny_list.get(position).get("accountNo");
                    String bankName = beny_list.get(position).get("bankName");
                    String ifscCode = beny_list.get(position).get("ifscCode");
                    String bene_status = beny_list.get(position).get("bene_status");

                    bankname.setText(bankName);
                    ifc_code.setText(ifscCode);
                    beneId = bene_id;
                } catch (Exception exc) {
                    exc.printStackTrace();
                }
            }
        });*/




        return view;
    }




    @Override
    public void onResume() {
        super.onResume();
        selectedBank = Constants.BANK_FLAG;
        System.out.println(">>>>>>>:::"+selectedBank);
        bank_name_autocomplete.setText(selectedBank);

        /*if(Constants.BANK_FLAG.equalsIgnoreCase("")){

        }else{

        }*/


    }



  /*  @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == 3) {
            String bankName = data.getDataString();
            System.out.println(">>>>>>>:::"+bankName);

        }


    }*/

    private void retriveBankList() {

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        //asynchronously retrieve all documents
        db.collection("BankList")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                List<BankListModel> downloadInfoList = task.getResult().toObjects(BankListModel.class);
                                bankDataMap.put(document.getId(),downloadInfoList);
                                listOfBank.add(document.getId());

                            }
                            for (Map.Entry<String, List<BankListModel>> entry : bankDataMap.entrySet()) {
                                System.out.println("DATA@@@@-->>> "+entry.getKey()+" : "+entry.getValue());
                                //bankDetailList = entry.getValue();
                                bankDetailList = new ArrayList<BankListModel>(entry.getValue());

                            }
                            progressBar.setVisibility(View.GONE);
                            main_container.setVisibility(View.VISIBLE);
                            benelistData.setVisibility(View.VISIBLE);
                            bene_ll.setVisibility(View.GONE);


                        } else {
                            Log.w(TAG, "Error getting documents.", task.getException());
                        }
                    }
                });
    }

    private void createBene(String _bank_name, String _ifccode, String _acc_no, String _bene_name, String _mobile) {
        HashMap<String, String> map = new HashMap<>();
        map.put("customerNumber", _mobile);
        map.put("beneMobileNo", _mobile);
        map.put("beneName", _bene_name);
        map.put("bankName", _bank_name);
        map.put("ifscCode", _ifccode);
        map.put("accountNo", _acc_no);

        RequestQueue mQueue = Volley.newRequestQueue(getActivity());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest("http://34.93.39.198:8080/dmt/addbene", new JSONObject(map),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.i("response ::::", "" + response);
                            beneList = new ArrayList<>();

                            int status = response.optInt("status");
                            String statusDesc = response.optString("statusDesc");

                            if (status == 0) {

                                JSONArray jsonArray = response.getJSONArray("beneList");

                                Toast.makeText(getActivity(), statusDesc, Toast.LENGTH_SHORT).show();
                                bank_name_autocomplete.setText("");
                                acc_no.setText("");
                                ifccode.setText("");
                                bene_name.setText("");

                                if (jsonArray.length() > 0) {
                                    // beny_list.clear();
                                    // mobile_number_list.clear();
                                    mList.clear();

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        HashMap<String, String> map = new HashMap<>();
                                        JSONObject _JsonObject = jsonArray.getJSONObject(i);
                                        String bene_id = _JsonObject.getString("id");
                                        String customerNumber = _JsonObject.getString("customerNumber");
                                        String beneMobileNo = _JsonObject.getString("beneMobileNo");
                                        String beneName = _JsonObject.getString("beneName");
                                        String accountNo = _JsonObject.getString("accountNo");
                                        String bankName = _JsonObject.getString("bankName");
                                        String ifscCode = _JsonObject.getString("ifscCode");
                                        String bene_status = _JsonObject.getString("status");

                                        map.put("bene_id", bene_id);
                                        map.put("customerNumber", customerNumber);
                                        map.put("beneMobileNo", beneMobileNo);
                                        map.put("beneName", beneName);
                                        map.put("accountNo", accountNo);
                                        map.put("bankName", bankName);
                                        map.put("ifscCode", ifscCode);
                                        map.put("bene_status", bene_status);

                                        BeneModel BM = new BeneModel();
                                        BM.setBeneId(bene_id);
                                        BM.setCustomerNumber(customerNumber);
                                        BM.setBeneMobileNo(beneMobileNo);
                                        BM.setBeneName(beneName);
                                        BM.setAccountNo(accountNo);
                                        BM.setBankName(bankName);
                                        BM.setIfscCode(ifscCode);
                                        BM.setBeneStatus(bene_status);
                                        beneList.add(BM);

                                        // mobile_number_list.add(beneMobileNo);
                                        // beny_list.add(map);

                                       // mList.add(map);
                                    }

                                    bebeAdapter = new BeneRecycleAdapter(tfrag,beneList);
                                    benelistData.setAdapter(bebeAdapter);


                                    // _adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.select_dialog_item, mobile_number_list);
                                    //adapter = new BeneAdapter(getActivity(), R.layout.rowlayout, R.id.beneMobileNo, mList);

                                    //actv.setThreshold(1);
                                    //actv.setAdapter(bebeAdapter);
                                    //actv.setTextColor(Color.BLACK);

                                }


                            } else {
                                Toast.makeText(getActivity(), statusDesc, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("TAG", error.getMessage(), error);
            }
        }) { //no semicolon or coma
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", _token);
                return params;
            }
        };

        mQueue.add(jsonObjectRequest);

    }

    private void resendotp(String mobile_number) {

        StringRequest request = new StringRequest(Request.Method.POST, "http://34.93.39.198:8080/dmt/resendotp/" + mobile_number, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                JSONObject jsonObject = null;

                try {
                    jsonObject = new JSONObject(response);
                    Log.i("jsonObject ::::: ", "" + jsonObject);

                    int status = jsonObject.optInt("status");
                    String statusDesc = jsonObject.optString("statusDesc");

                    if (status == 0) {
                        Toast.makeText(getActivity(), "" + statusDesc, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), "" + statusDesc, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error is ", "" + error);
            }
        }) {
            //This is for Headers If You Needed
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", _token);
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        queue.add(request);
    }

    private void verifyotp(String otp, final String mobile_number) {
        HashMap<String, String> map = new HashMap<>();
        map.put("otp", otp);
        map.put("mobileNumber", mobile_number);

        RequestQueue mQueue = Volley.newRequestQueue(getActivity());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest("http://34.93.39.198:8080/dmt/verifyotp", new JSONObject(map),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.i("response ::::", "" + response);

                            int status = response.optInt("status");
                            String statusDesc = response.optString("statusDesc");

                            if (status == 0) {
                                new_customer.setVisibility(View.GONE);
                                verify_number.setVisibility(View.GONE);
                                main_container.setVisibility(View.VISIBLE);

                                Toast.makeText(getActivity(), statusDesc, Toast.LENGTH_SHORT).show();

                                loadCustomerDetail(_token, mobile_number);

                            } else if (status == -1) {
                                new_customer.setVisibility(View.GONE);
                                verify_number.setVisibility(View.VISIBLE);
                                Toast.makeText(getActivity(), statusDesc, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), statusDesc, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("TAG", error.getMessage(), error);
            }
        }) { //no semicolon or coma
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", _token);
                return params;
            }
        };
        mQueue.add(jsonObjectRequest);
    }

    private void addcustomer(String customer_name, String mobile_number) {

        HashMap<String, String> map = new HashMap<>();
        map.put("customerNo", mobile_number);
        map.put("customerName", customer_name);

        RequestQueue mQueue = Volley.newRequestQueue(getActivity());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest("http://34.93.39.198:8080/dmt/addcustomer", new JSONObject(map),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.i("response ::::", "" + response);

                            int status = response.optInt("status");
                            String statusDesc = response.optString("statusDesc");

                            if (status == 0) {
                                new_customer.setVisibility(View.GONE);
                                verify_number.setVisibility(View.VISIBLE);

                                Toast.makeText(getActivity(), statusDesc, Toast.LENGTH_SHORT).show();
                            } else if (status == -1) {
                                new_customer.setVisibility(View.GONE);
                                verify_number.setVisibility(View.VISIBLE);
                                Toast.makeText(getActivity(), statusDesc, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), statusDesc, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("TAG", error.getMessage(), error);
            }
        }) { //no semicolon or coma
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", _token);
                return params;
            }
        };
        mQueue.add(jsonObjectRequest);


    }

    private void loadCustomerDetail(final String token, final String mobile) {
        StringRequest request = new StringRequest(Request.Method.POST, "http://34.93.39.198:8080/dmt/getcustomer/" + mobile, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                JSONObject jsonObject = null;

                try {
                    jsonObject = new JSONObject(response);

                    int status = jsonObject.optInt("status");

                    if (status == 0) {

                        CommonUtil.hideKeyboard(getActivity(),mobile_number);
                       // progressBar.setVisibility(View.GONE);
                       /* main_container.setVisibility(View.VISIBLE);
                        benelistData.setVisibility(View.VISIBLE);
                        bene_ll.setVisibility(View.GONE);*/

                        customerId = jsonObject.getString("customerId");
                        String mobileNumber = jsonObject.getString("mobileNumber");
                        String name = jsonObject.getString("name");
                        String statusDesc = jsonObject.getString("statusDesc");

                        String balanceLimitPipe1 = jsonObject.getString("balanceLimitPipe1");
                        String balanceLimitPipe2 = jsonObject.getString("balanceLimitPipe2");
                        String balanceLimitPipe3 = jsonObject.getString("balanceLimitPipe3");

                        if (!balanceLimitPipe1.equals("0.0")) {
                            wallet_balance_one.setText("₹ " + balanceLimitPipe1);
                        } else {
                            wallet_content_one.setVisibility(View.GONE);
                        }


                        if (!balanceLimitPipe2.equals("0.0")) {
                            wallet_balance_two.setText("₹ " + balanceLimitPipe2);
                        } else {
                            wallet_balance_two.setVisibility(View.GONE);
                        }


                        if (!balanceLimitPipe3.equals("0.0")) {
                            wallet_balance_three.setText("₹ " + balanceLimitPipe3);
                        } else {
                            wallet_balance_three.setVisibility(View.GONE);
                        }

                        JSONArray jsonArray = jsonObject.getJSONArray("beneList");

                        if (jsonArray.length() > 0) {
                            beny_list.clear();
                            mobile_number_list.clear();
                            mList.clear();



                            for (int i = 0; i < jsonArray.length(); i++) {
                                HashMap<String, String> map = new HashMap<>();
                                JSONObject _JsonObject = jsonArray.getJSONObject(i);
                                String bene_id = _JsonObject.getString("id");
                                String customerNumber = _JsonObject.getString("customerNumber");
                                String beneMobileNo = _JsonObject.getString("beneMobileNo");
                                String beneName = _JsonObject.getString("beneName");
                                String accountNo = _JsonObject.getString("accountNo");
                                String bankName = _JsonObject.getString("bankName");
                                String ifscCode = _JsonObject.getString("ifscCode");
                                String bene_status = _JsonObject.getString("status");

                                BeneModel BM = new BeneModel();
                                BM.setBeneId(bene_id);
                                BM.setCustomerNumber(customerNumber);
                                BM.setBeneMobileNo(beneMobileNo);
                                BM.setBeneName(beneName);
                                BM.setAccountNo(accountNo);
                                BM.setBankName(bankName);
                                BM.setIfscCode(ifscCode);
                                BM.setBeneStatus(bene_status);
                                beneList.add(BM);

                            }
                            bebeAdapter = new BeneRecycleAdapter(tfrag,beneList);
                            benelistData.setAdapter(bebeAdapter);
                            //Retribve bank list fron firebase

                            retriveBankList();

                        }

                    } else if (status == 11) {
                        main_container.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        new_customer.setVisibility(View.VISIBLE);
                        verify_number.setVisibility(View.GONE);
                        //benelistData.setVisibility(View.GONE);
                        bene_ll.setVisibility(View.GONE);


                    } else if (status == 12) {
                        main_container.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        new_customer.setVisibility(View.GONE);
                        verify_number.setVisibility(View.VISIBLE);
                       // benelistData.setVisibility(View.GONE);
                        bene_ll.setVisibility(View.GONE);

                    } else {
                        main_container.setVisibility(View.GONE);
                        benelistData.setVisibility(View.GONE);
                        bene_ll.setVisibility(View.GONE);
                    }

                    Log.i("jsonObject ::::: ", "" + jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error is ", "" + error);
            }
        }) {
            //This is for Headers If You Needed
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", token);
                return params;
            }

            //Pass Your Parameters here
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                int statusCode = response.statusCode;
                Log.i("statusCode ::::::", "" + statusCode);
                return super.parseNetworkResponse(response);
            }
        };

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        queue.add(request);

    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    public class MySimpleArrayAdapter extends ArrayAdapter {
        private final Context context;
        private final ArrayList<HashMap<String, String>> values;

        public MySimpleArrayAdapter(Context context, ArrayList<HashMap<String, String>> values) {
            super(context, -1, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("ViewHolder") View rowView = inflater.inflate(R.layout.rowlayout, parent, false);
            TextView name = rowView.findViewById(R.id.name);
            TextView beneMobileNo = rowView.findViewById(R.id.beneMobileNo);

            // TextView accountNo =  rowView.findViewById(R.id.accountNo);
            // TextView bankName =  rowView.findViewById(R.id.bankName);

            name.setText("Name: " + values.get(position).get("beneName"));
            beneMobileNo.setText("Mobile No: " + values.get(position).get("beneMobileNo"));

            // accountNo.setText("account No"+values.get(position).get("accountNo"));
            // bankName.setText("Bank Name"+values.get(position).get("bankName"));

            return rowView;
        }
    }

    //----------------------------
    public void itemOncliclickView(BeneModel bModel){


        try {
            single_bene_bank.setVisibility(View.VISIBLE);
           // benelistData.setVisibility(View.GONE);
            bene_ll.setVisibility(View.GONE);

            loadmore.setVisibility(View.VISIBLE);
            load_more.setVisibility(View.VISIBLE);
            selectedBeneName.setText(bModel.beneName);


            bankname.setText(bModel.bankName);
            ifc_code.setText(bModel.ifscCode);
            beneId = bModel.beneId;
        } catch (Exception exc) {
            exc.printStackTrace();
        }





    }

    //--------------------
   private boolean isValidAccount(String accountPattern) {
        String patternAccountNo="";
        for(String bankl:listOfBank){

            if(bankl.equalsIgnoreCase(selectedBank)){

                int pos =  listOfBank.indexOf(selectedBank);
                String strr =  bankDetailList.get(pos).getPATTERN();

                if (strr!=null){
                    patternAccountNo=strr;
                }}}
        Pattern pattern = Pattern.compile(patternAccountNo);
        Matcher matcher = pattern.matcher(accountPattern);
        return matcher.matches();
    }
    //------------------------------
    private void ifcsFind() {

        for(String bankl:listOfBank){

            if(bankl.equalsIgnoreCase(selectedBank)){
                int pos =  listOfBank.indexOf(selectedBank);
                String flag =  bankDetailList.get(pos).getFLAG();
                String bankCode = bankDetailList.get(pos).getBANKCODE();
                if (flag!=null){
                    if (flag.equalsIgnoreCase("u")){
                        ifccode.setText(bankCode);
                        ifccode.setEnabled(false);
                    }else if (flag.equalsIgnoreCase("4")){
                        String first4 = "";
                        try {

                            first4 = getAccountNo.substring(0,4);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        ifccode.setText(bankCode+first4);
                        ifccode.setEnabled(false);
                    }else if (flag.equalsIgnoreCase("3")){

                        String first3 = "";
                        try {

                            first3 = getAccountNo.substring(0,3);
                            //first3 = enterAccountNo.getText().toString().substring(0,3);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        ifccode.setText(bankCode+first3);
                        ifccode.setEnabled(false);
                    }else if (flag.equalsIgnoreCase("6")){
                        String first6 = "";
                        try {
                            first6 = getAccountNo.substring(0,6);
                            // first6 = enterAccountNo.getText().toString().substring(0,6);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        ifccode.setText(bankCode+first6);
                        ifccode.setEnabled(false);
                    }}else /*if(bankl.getFLAG().equalsIgnoreCase(null))*/{
                    ifccode.setText("no IFSC");
                    ifccode.setEnabled(true);
                }
            }
        }
    }


    private void doVerifyBene() {
        progress_bene.setVisibility(View.VISIBLE);

        HashMap<String, String> map = new HashMap<>();
        map.put("beneName", bene_name.getText().toString());
        map.put("coustomerId", customerId);
        map.put("toAccount",acc_no.getText().toString());
        map.put("ifscCode",ifccode.getText().toString());
        map.put("bankName",bank_name_autocomplete.getText().toString());

        RequestQueue mQueue = Volley.newRequestQueue(getActivity());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest("http://34.93.39.198:8080/dmt/verifybene", new JSONObject(map),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            progress_bene.setVisibility(View.GONE);
                            Log.i("response ::::", "" + response);

                            int status = response.optInt("status");
                            String statusDesc = response.optString("statusDesc");

                            if (status == 0) {
                                String beneName = response.optString("beneName");
                                bene_name.setText(beneName);


                             } else if (status == -1) {

                                Toast.makeText(getActivity(), statusDesc, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), statusDesc, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("TAG", error.getMessage(), error);
                progress_bene.setVisibility(View.GONE);

            }
        }) { //no semicolon or coma
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", _token);
                return params;
            }
        };
        mQueue.add(jsonObjectRequest);
    }

    private void doAddNPay() {

        pd = new ProgressDialog(getActivity());
        pd.setMessage("Loading...");
        pd.show();
        pd.setCancelable(false);


        if (wallet_selected == 1) {
            pipeNo = "1";
        } else if (wallet_selected == 2) {
            pipeNo = "2";
        } else {
            pipeNo = "3";
        }


        HashMap<String, String> map = new HashMap<>();
        map.put("customerNumber", bene_name.getText().toString());
        map.put("recipientMobileNo", mobile_number.getText().toString());
        map.put("recipientName",bene_name.getText().toString());
        map.put("accountNo",acc_no.getText().toString());
        map.put("bankName",bank_name_autocomplete.getText().toString());
        map.put("accIfsc",ifccode.getText().toString());
        map.put("transactionType","imps");
        map.put("amount",amount_transfer.getText().toString());
        map.put("pipeNo",pipeNo);
        map.put("uid",uid);


        RequestQueue mQueue = Volley.newRequestQueue(getActivity());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest("http://34.93.39.198:8080/fnaddndpay", new JSONObject(map),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            //progress_bene.setVisibility(View.GONE);
                            pd.dismiss();
                            Log.i("response ::::", "" + response);

                            int status = response.optInt("status");
                            String statusDesc = response.optString("statusDesc");

                            if (status == 0) {
                                Toast.makeText(getActivity(), statusDesc, Toast.LENGTH_SHORT).show();
                                dismiss();


                            } else if (status == -1) {

                                Toast.makeText(getActivity(), statusDesc, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), statusDesc, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("TAG", error.getMessage(), error);
                //progress_bene.setVisibility(View.GONE);
                pd.dismiss();

            }
        }) { //no semicolon or coma
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", _token);
                return params;
            }
        };
        mQueue.add(jsonObjectRequest);


    }
}