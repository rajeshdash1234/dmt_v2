package com.orbo.isu_customer.special;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.isu.dmt.R;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

public class SpecialActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    SearchableSpinner special_select_operator;
    EditText special_mobileno,special_amount;

    String[] operatorSpecial = { "AIRCEL stv","AIRTEL stv","BSNL stv","IDEA stv","RELIANCE stv","TATA DOCOMO stv","TELENOR stv",
            "VODAFONE stv","T24(Special)","BSNL VALIDITY","MTNL Validity","VIRGIN GSM SPECIAL","VIDEOCON SPECIAL"};

    Button special_button;

    String mobilenoString,amountString,operatorString,rechargetypeString,stdCodeString,pincodeString,accountNoString,latitudeString,
            circleCodeString,longitudeString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_special);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.special));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        special_mobileno = findViewById(R.id.special_mobileno);
        special_amount = findViewById(R.id.special_amount);
        special_button = findViewById(R.id.special_button);

        special_select_operator =  findViewById(R.id.special_select_operator);
        special_select_operator.setOnItemSelectedListener(this);


        special_mobileno.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 10){
                    mobilenoString = special_mobileno.getText().toString();
                }else {
                    mobilenoString = null;
                }
            }
        });

        special_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                amountString = special_amount.getText().toString();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 0){
                    amountString = null;
                }else {
                    amountString = special_amount.getText().toString();
                }
            }
        });
        special_select_operator = findViewById(R.id.special_select_operator);
        special_select_operator.setOnItemSelectedListener(this);

        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,operatorSpecial);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        special_select_operator.setAdapter(aa);

        special_button.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (amountString==null|| mobilenoString==null){
            Toast.makeText(getApplicationContext(),"Enter 10 Digit MobileNo OR Enter Amount" , Toast.LENGTH_LONG).show();
        }else {
            rechargeSpecial();
        }


    }

    private void rechargeSpecial() {
        Toast.makeText(this, "Pending .....", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        operatorString = operatorSpecial[position];

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
