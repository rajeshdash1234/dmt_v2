package com.orbo.isu_customer.bankspinner;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.isu.dmt.R;
import com.orbo.isu_customer.utility.Constants;

import java.util.ArrayList;

public class DMTBankNameActivity extends AppCompatActivity {

    private RecyclerView bankNameRecyclerView;
    private DMTBankNameListAdapter bankNameListAdapter;
    SearchView searchView;
    ProgressDialog loadingView;
    String bankLogoImage = "https://firebasestorage.googleapis.com/v0/b/fir-banklogostore.appspot.com/o/BankList%2Fandhrabank.png?alt=media&token=5610f400-4213-4fa2-a108-e54ed4cabc53";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_name_list);

          bankNameRecyclerView = (RecyclerView) findViewById(R.id.bankNameRecyclerView);
          RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
          bankNameRecyclerView.setLayoutManager(mLayoutManager);
          bankNameRecyclerView.setItemAnimator(new DefaultItemAnimator());


        ArrayList<String> bankList = getIntent().getStringArrayListExtra("BANKDATA");


        searchView = findViewById(R.id.searchView);
        searchView.setIconifiedByDefault(true);
        searchView.setQueryHint(getResources().getString(R.string.search_hint));
        searchView.setFocusable(true);
        searchView.setIconified(false);
        searchView.clearFocus();
        searchView.requestFocusFromTouch();

        setToolbar();

        System.out.println("::::>>>>:::::"+bankList.size());


        if (bankList != null && bankList.size() > 0) {
            bankNameListAdapter = new DMTBankNameListAdapter(DMTBankNameActivity.this,bankList, new DMTBankNameListAdapter.RecyclerViewClickListener() {
                @Override
                public void recyclerViewListClicked(View v, int position) {
                    Constants.BANK_FLAG = bankNameListAdapter.getItem(position);

                   /* Intent intent = new Intent();
                    intent.putExtra("BankName", bankNameListAdapter.getItem(position));
                    setResult(3, intent);*/
                    finish();


                }
            });
            bankNameRecyclerView.setAdapter(bankNameListAdapter);
        }


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                // Toast.makeText(getBaseContext(), query, Toast.LENGTH_LONG).show();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                bankNameListAdapter.getFilter().filter(newText);

                //Toast.makeText(getBaseContext(), newText, Toast.LENGTH_LONG).show();
                return true;
            }
        });


    }
    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.banklist));
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
