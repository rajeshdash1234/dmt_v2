package com.orbo.isu_customer.bankspinner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;


import com.isu.dmt.R;
import com.orbo.isu_customer.utility.Constants;
import com.orbo.isu_customer.utility.Util;

import java.util.ArrayList;
import java.util.List;

public class BankNameListActivity extends AppCompatActivity implements BankNameContract.View {

    private List<BankNameModel> bankNameModelList = new ArrayList<>();
    private RecyclerView bankNameRecyclerView;
    private BankNameListAdapter bankNameListAdapter;
    private BankNameListPresenter bankNameListPresenter;
    SearchView searchView;
   // LoadingView loadingView;
    ProgressDialog loadingView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_name_list);

        loadingView = new ProgressDialog(BankNameListActivity.this);
        loadingView.setMessage("Loading..");
        loadingView.setCancelable(false);

        bankNameRecyclerView = (RecyclerView) findViewById(R.id.bankNameRecyclerView);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        bankNameRecyclerView.setLayoutManager(mLayoutManager);
        bankNameRecyclerView.setItemAnimator(new DefaultItemAnimator());
        //  bankNameRecyclerView.addItemDecoration(new DividerItemDecoration(this));

        bankNameListPresenter = new BankNameListPresenter(BankNameListActivity.this);
        bankNameListPresenter.loadBankNamesList(BankNameListActivity.this);

        searchView = findViewById(R.id.searchView);
        searchView.setIconifiedByDefault(true);
        searchView.setQueryHint(getResources().getString(R.string.search_hint));
        searchView.setFocusable(true);
        searchView.setIconified(false);
        searchView.clearFocus();
        searchView.requestFocusFromTouch();

        setToolbar();


       /* bankNameRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(BankNameListActivity.this,
                bankNameRecyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {



            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));*/

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                // Toast.makeText(getBaseContext(), query, Toast.LENGTH_LONG).show();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                bankNameListAdapter.getFilter().filter(newText);

                //Toast.makeText(getBaseContext(), newText, Toast.LENGTH_LONG).show();
                return true;
            }
        });
        // prepareMovieData ();

    }


    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.banklist));
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    @Override
    public void bankNameListReady(ArrayList<BankNameModel> bankNameModelArrayList) {
        if (bankNameModelArrayList != null && bankNameModelArrayList.size() > 0) {
            bankNameModelList = bankNameModelArrayList;
        }
    }

    @Override
    public void showBankNames() {
        if (bankNameModelList != null && bankNameModelList.size() > 0) {
            bankNameListAdapter = new BankNameListAdapter(bankNameModelList, new BankNameListAdapter.RecyclerViewClickListener() {
                @Override
                public void recyclerViewListClicked(View v, int position) {
                    Intent intent = new Intent();
                    intent.putExtra(Constants.IIN_KEY, bankNameListAdapter.getItem(position));
                    setResult(RESULT_OK, intent);
                    finish();
                }
            });
            bankNameRecyclerView.setAdapter(bankNameListAdapter);
        }
    }

    @Override
    public void showLoader() {

        /*if (loadingView == null) {
            loadingView = Util.showProgress(BankNameListActivity.this);
        }*/

        loadingView.show();
    }

    @Override
    public void hideLoader() {
        /*if (loadingView != null) {
            loadingView.hide();
        }*/
        loadingView.dismiss();
    }

    @Override
    public void emptyBanks() {

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK)
            return false;
        return false;
    }

/*
    private void setToolbar() {

        Toolbar mToolbar = findViewById ( R.id.toolbar );
        mToolbar.setTitle (getResources().getString(R.string.aeps_bank_list_title) );
        mToolbar.inflateMenu ( R.menu.bank_menu );

        mToolbar.setOnMenuItemClickListener ( new Toolbar.OnMenuItemClickListener () {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                if(item.getItemId()==R.id.action_close)
                {
                    finish ();
                }

                return false;
            }
        } );
    }
*/
}
