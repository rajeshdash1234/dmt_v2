package com.orbo.isu_customer.prepaid;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.isu.dmt.R;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

public class PrepaidActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    SearchableSpinner prepaid_select_operator;
    String mobilenoString,amountString,operatorString,rechargetypeString,stdCodeString,pincodeString,accountNoString,latitudeString,circleCodeString,longitudeString;

    String[] operator = { "AIRCEL","AIRTEL","BSNL","IDEA","JIO","LOOP","MTNL DELHI","MTNL MUMBAI","MTS",
            "RELIANCE CDMA","RELIANCE GSM","TATA DOCOMO","TELENOR","VIDEOCON","VIRGIN CDMA","VIRGIN GSM","VODAFONE",
            "TataIndicom","T24(Flexi)"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prepaid);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.prepaid));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        prepaid_select_operator = (SearchableSpinner) findViewById(R.id.prepaid_select_operator);
        prepaid_select_operator.setOnItemSelectedListener(this);

        //Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,operator);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        prepaid_select_operator.setAdapter(aa);

    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        operatorString = operator[i];

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    @Override
    public void onClick(View view) {

    }
}
