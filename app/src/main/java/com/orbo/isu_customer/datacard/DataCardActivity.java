package com.orbo.isu_customer.datacard;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.isu.dmt.R;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

public class DataCardActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    SearchableSpinner datacard_select_operator;
    String operatorString;

    String[] operatorDatacard = {"VODAFONE DATACARD","Airtel DATACARD","TATA PHOTON PLUS DATACARD","RELIANCE DATACARD","RELIANCE BROADBAND DATACARD",
            "MTS DATACARD","BSNL DATACARD","MTNL MUMBAI DATACARD","IDEA DATACARD","TataIndicom DATACARD","TATADocomo DATACARD","AIRCEL DATACARD"};

    Button datacard_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_card);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.datacard));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        datacard_button = findViewById(R.id.datacard_button);
        datacard_select_operator = findViewById(R.id.datacard_select_operator);
        datacard_select_operator.setOnItemSelectedListener(this);

        //Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa = new ArrayAdapter(DataCardActivity.this,android.R.layout.simple_spinner_item,operatorDatacard);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        datacard_select_operator.setAdapter(aa);

        datacard_button.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        operatorString = operatorDatacard[i];
        Toast.makeText(getApplicationContext(),operatorString , Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
