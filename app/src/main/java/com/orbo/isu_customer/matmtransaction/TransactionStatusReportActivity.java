package com.orbo.isu_customer.matmtransaction;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.isu.dmt.R;
import com.orbo.isu_customer.utility.Constants;

public class TransactionStatusReportActivity extends AppCompatActivity {

    LinearLayout successLayout, failureLayout, pendingLayout;
    Button okButton, okSuccessButton, okPendingButton;
    TextView detailsTextView, failureDetailTextView, failureTitleTextView, detailsPendingTextView, failureTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_status_report);

        failureTextView = findViewById(R.id.failureTextView);
        successLayout = findViewById(R.id.successLayout);
        pendingLayout = findViewById(R.id.pendingLayout);
        failureLayout = findViewById(R.id.failureLayout);
        okButton = findViewById(R.id.okButton);
        okPendingButton = findViewById(R.id.okPendingButton);
        okSuccessButton = findViewById(R.id.okSuccessButton);
        detailsTextView = findViewById(R.id.detailsTextView);
        failureTitleTextView = findViewById(R.id.failureTitleTextView);
        failureDetailTextView = findViewById(R.id.failureDetailTextView);


        if (getIntent().getSerializableExtra(Constants.TRANSACTION_STATUS_KEY_SDK) == null) {
            failureLayout.setVisibility(View.VISIBLE);
            successLayout.setVisibility(View.GONE);
            failureDetailTextView.setText("Some Exception occured");
        } else {
            TransactionStatusModel transactionStatusModel = (TransactionStatusModel) getIntent().getSerializableExtra(Constants.TRANSACTION_STATUS_KEY_SDK);

            if (transactionStatusModel.getStatus() != null && transactionStatusModel.getTransactionType() != null) {

                if (transactionStatusModel.getStatus().equalsIgnoreCase("Success")) {
                    failureLayout.setVisibility(View.GONE);
                    successLayout.setVisibility(View.VISIBLE);
                    String status = "NA";
                    if (transactionStatusModel.getStatus() != null && !transactionStatusModel.getStatus().matches("")) {
                        status = transactionStatusModel.getStatus();
                    }

                    String amount = "NA";
                    if (transactionStatusModel.getAmount() != null && !transactionStatusModel.getAmount().matches("")) {
                        amount = transactionStatusModel.getAmount();
                    }

                    String localdate = "NA";
                    if (transactionStatusModel.getLocaldate() != null && !transactionStatusModel.getLocaldate().matches("")) {
                        localdate = transactionStatusModel.getLocaldate();
                    }
                    String rferencesnumber = "NA";
                    if (transactionStatusModel.getRRN() != null && !transactionStatusModel.getRRN().matches("")) {
                        rferencesnumber = transactionStatusModel.getRRN();
                    }

                    String clientId = "NA";
                    if (transactionStatusModel.getClientId() != null && !transactionStatusModel.getClientId().matches("")) {
                        clientId = transactionStatusModel.getClientId();
                    }

                    String transactionType = "NA";
                    if (transactionStatusModel.getTransactionType() != null && !transactionStatusModel.getTransactionType().matches("")) {
                        transactionType = transactionStatusModel.getTransactionType();
                    }
                    if (transactionStatusModel.getStatus() != null && !transactionStatusModel.getStatus().matches("")) {
                        detailsTextView.setText("Transaction Status for " + clientId + " is " + status + ". \n \nAmount : " + amount + " \nReference No : " + rferencesnumber + "\nTransaction Type : " + transactionType + "\nTransaction Amount : " + amount + " \nTransaction Date and Time : " + localdate);
                    }
                } else if (transactionStatusModel.getStatus().equalsIgnoreCase("Failed")) {
                    failureLayout.setVisibility(View.VISIBLE);
                    successLayout.setVisibility(View.GONE);
                    String status = "NA";
                    if (transactionStatusModel.getStatus() != null && !transactionStatusModel.getStatus().matches("")) {
                        status = transactionStatusModel.getStatus();
                    }

                    String amount = "NA";
                    if (transactionStatusModel.getAmount() != null && !transactionStatusModel.getAmount().matches("")) {
                        amount = transactionStatusModel.getAmount();
                    }

                    String localdate = "NA";
                    if (transactionStatusModel.getLocaldate() != null && !transactionStatusModel.getLocaldate().matches("")) {
                        localdate = transactionStatusModel.getLocaldate();
                    }
                    String rferencesnumber = "NA";
                    if (transactionStatusModel.getRRN() != null && !transactionStatusModel.getRRN().matches("")) {
                        rferencesnumber = transactionStatusModel.getRRN();
                    }

                    String clientId = "NA";
                    if (transactionStatusModel.getClientId() != null && !transactionStatusModel.getClientId().matches("")) {
                        clientId = transactionStatusModel.getClientId();
                    }

                    String transactionType = "NA";
                    if (transactionStatusModel.getTransactionType() != null && !transactionStatusModel.getTransactionType().matches("")) {
                        transactionType = transactionStatusModel.getTransactionType();
                    }
                    if (transactionStatusModel.getStatus() != null && !transactionStatusModel.getStatus().matches("")) {
                        failureTextView.setText("Transaction Status for " + clientId + " is " + status + ". \n \nAmount : " + amount + " \nReference No : " + rferencesnumber + "\nTransaction Type : " + transactionType + "\nTransaction Amount : " + amount + " \nTransaction Date and Time : " + localdate);
                        failureDetailTextView.setVisibility(View.GONE);
                    }
                }

            } else {
                failureLayout.setVisibility(View.VISIBLE);
                successLayout.setVisibility(View.GONE);

                if (transactionStatusModel.getErrorMsg() != null && !transactionStatusModel.getErrorMsg().matches("")) {
                    failureTextView.setVisibility(View.GONE);
                    failureDetailTextView.setText(transactionStatusModel.getErrorMsg());
                } else {
                    failureTextView.setVisibility(View.GONE);
                    failureDetailTextView.setText("Some Exception occured, Please try again after some time.");
                }
            }

        }
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        okPendingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        okSuccessButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}