package com.orbo.isu_customer.matmtransaction;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MicroReportResponse {

    @SerializedName("mATMTransactionReport")
    private ArrayList<MicroReportModel> mATMTransactionReport;

    public ArrayList<MicroReportModel> getmATMTransactionReport() {
        return mATMTransactionReport;
    }

    public void setmATMTransactionReport(ArrayList<MicroReportModel> mATMTransactionReport) {
        this.mATMTransactionReport = mATMTransactionReport;
    }

    public MicroReportResponse() {
    }

}
