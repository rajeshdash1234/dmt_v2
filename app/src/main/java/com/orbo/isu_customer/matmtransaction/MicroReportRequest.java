package com.orbo.isu_customer.matmtransaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MicroReportRequest {

    @SerializedName("fromDate")
    @Expose
    private String fromDate;

    @SerializedName("toDate")
    @Expose
    private String toDate;

    @SerializedName("transactionType")
    @Expose
    private String transactionType;

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public MicroReportRequest(String fromDate, String toDate, String transactionType) {
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.transactionType = transactionType;
    }
}
