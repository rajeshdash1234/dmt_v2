package com.orbo.isu_customer.dth;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.isu.dmt.R;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

public class DthActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    Button dth_button;
    SearchableSpinner dth_select_operator;
    EditText dth_customerid, dth_amount;
    String customeridString, amountString, operatorString, rechargetypeString, stdCodeString, pincodeString, accountNoString, latitudeString, circleCodeString, longitudeString;

    String[] operatorDth = {"BIGTV DTH", "AIRTEL DTH", "TATASKY DTH", "SUN DIRECT DTH", "VIDEOCON DTH", "DISH TV DTH", "TATASKY B2B DTH"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dth);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.dth));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        dth_customerid = findViewById(R.id.dth_customerid);
        dth_amount = findViewById(R.id.dth_amount);
        dth_button = findViewById(R.id.dth_button);

        dth_customerid.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 0) {
                    customeridString = null;
                } else {
                    customeridString = dth_customerid.getText().toString();
                }
            }
        });

        dth_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 0) {
                    amountString = null;
                } else {
                    amountString = dth_amount.getText().toString();
                }
            }
        });

        dth_select_operator = findViewById(R.id.dth_select_operator);
        dth_select_operator.setOnItemSelectedListener(this);

        //Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, operatorDth);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        dth_select_operator.setAdapter(aa);

        dth_button.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (amountString == null || customeridString == null) {
            Toast.makeText(getApplicationContext(), "Enter CustomerId OR Enter Amount", Toast.LENGTH_LONG).show();
        } else {
            //    rechargeDth();
            Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        operatorString = operatorDth[i];
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
