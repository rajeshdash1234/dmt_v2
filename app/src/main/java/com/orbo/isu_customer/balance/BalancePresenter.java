package com.orbo.isu_customer.balance;

import android.util.Base64;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.orbo.isu_customer.utility.AEPSAPIService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BalancePresenter implements BalanceContract.UserActionsListener {
    /**
     * Initialize LoginView
     */
    private BalanceContract.View balanceView;
    private AEPSAPIService aepsapiService;
    public static final String GET_LOADED_FEATURE_URL = "https://itpl.iserveu.tech/generate/v2";

    /**
     * Initialize LoginPresenter
     */
    public BalancePresenter(BalanceContract.View balanceView) {
        this.balanceView = balanceView;
    }

    @Override
    public void loadBalance(final String token) {
        if (token!=null) {

            if (this.aepsapiService == null) {
                this.aepsapiService = new AEPSAPIService();
            }
            balanceView.showLoader ();

            AndroidNetworking.get(GET_LOADED_FEATURE_URL)
                    //.setPriority(Request.Priority.LOW)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----"+key);
                                byte[] data = Base64.decode(key, Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");

                                encriptedLoadBalance(aepsapiService,token,encodedUrl);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });
        }
    }
    //Edited By Rajesh
    public void encriptedLoadBalance(AEPSAPIService aepsapiService, String token, String encodedUrl){

        BalanceApi balanceApi = this.aepsapiService.getClient().create(BalanceApi.class);
        Call<BalanceResponse> respuesta = balanceApi.getBalanceDetails(token,encodedUrl);
        respuesta.enqueue(new Callback<BalanceResponse>() {
            @Override
            public void onResponse(Call<BalanceResponse> call, Response<BalanceResponse> response) {
                if (response.isSuccessful()){
                    String json = response.body().getUserInfoModel().getUserBrand();

                    try {

                        JSONObject obj = new JSONObject(json);

                        if (obj.has("brand")){
                            response.body().getUserInfoModel().setUserBrand(obj.getString("brand"));
                        }

                        //balanceView.showBalance(response.body().getUserInfoModel().getUserBrand());

                    } catch (Throwable t) {
                        t.printStackTrace();
                    }

                    balanceView.hideLoader ();
                    balanceView.showBalance(response.body().getUserInfoModel().getUserBalance());
                    balanceView.showFeature(response.body().getUserInfoModel().getFeatureIdList());
                }

            }

            @Override
            public void onFailure(Call<BalanceResponse> call, Throwable t) {
                balanceView.hideLoader ();
            }


        });

    }
}
