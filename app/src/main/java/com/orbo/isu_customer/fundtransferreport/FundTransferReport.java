package com.orbo.isu_customer.fundtransferreport;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.isu.dmt.R;
import com.orbo.isu_customer.activity.SessionManager;
import com.orbo.isu_customer.utility.AEPSAPIService;
import com.orbo.isu_customer.utility.Util;
import com.rw.loadingdialog.LoadingView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FundTransferReport extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    LoadingView loadingView;
    TextView textView;
    TextView noData;

    private RecyclerView reportRecyclerView;
    CustomReportAdapter mAdapter;

    RecyclerView.LayoutManager layoutManager;

    String transactionType = "ISU_FT";

    private AEPSAPIService apiService;
    //Session session;
    AutoCompleteTextView transaction_spinner;
    ReportFragmentRequest reportFragmentRequest;
    ArrayList<FinoTransactionReports> finoTransactionReports;

    String _token = "", _admin = "";
    SessionManager session;
    private ShimmerFrameLayout mm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fund_transfer_report);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.fund_transfer_report));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        textView = findViewById(R.id.text_date_picker);
        transaction_spinner = findViewById(R.id.transaction_spinner);
        reportRecyclerView = findViewById(R.id.reportRecyclerView);
        reportRecyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(FundTransferReport.this);

        reportRecyclerView.setLayoutManager(layoutManager);

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(FundTransferReport.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.show(getFragmentManager(), "jitu");
                dpd.setMaxDate(Calendar.getInstance());
                dpd.setAutoHighlight(true);

            }
        });

        noData = findViewById(R.id.noData);

        transaction_spinner.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // mAdapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                mAdapter.getFilter().filter(editable.toString());
                if (mAdapter.getItemCount() == 0) {
                    noData.setVisibility(View.VISIBLE);
                } else {
                    noData.setVisibility(View.GONE);
                }
            }
        });

        session = new SessionManager(getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        _token = user.get(SessionManager.KEY_TOKEN);
        _admin = user.get(SessionManager.KEY_ADMIN);

        mm = findViewById(R.id.shimmer_view_container1);
        mm.setVisibility(View.GONE);

    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {

        String fromdate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
        String todate = yearEnd + "-" + (monthOfYearEnd + 1) + "-" + (dayOfMonthEnd);
        String api_todate = yearEnd + "-" + (monthOfYearEnd + 1) + "-" + (dayOfMonthEnd + 1);

        String date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year + " To " + (dayOfMonthEnd) + "/" + (monthOfYearEnd + 1) + "/" + yearEnd;
        textView.setText(date);
        Log.d("Report", "fromdate: " + fromdate);
        Log.d("Report", "fromdate todate: " + todate);

        /*ReportFragmentRequest*/
        reportFragmentRequest = new ReportFragmentRequest(transactionType, fromdate, api_todate);

        Date from_date = null;
        Date to_date = null;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        try {
            from_date = formatter.parse(fromdate);
            to_date = formatter.parse(todate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (from_date.before(to_date) || from_date.equals(to_date)) {

            reportCall();

            mm.startShimmerAnimation();
            mm.setVisibility(View.VISIBLE);

        } else {
            Toast.makeText(this, "From date should be less than To date.", Toast.LENGTH_LONG).show();
        }

    }

    private void reportCall() {
        if (this.apiService == null) {
            this.apiService = new AEPSAPIService();
        }
        AndroidNetworking.get("https://itpl.iserveu.tech/generate/v7")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            encriptedReportCall(encodedUrl);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }

    private void showLoader() {
        if (loadingView == null) {
            loadingView = Util.showProgress(FundTransferReport.this);
        }

        loadingView.show();
    }

    private void hideLoader() {
        if (loadingView != null) {
            loadingView.hide();
        }
    }

    //Edited Rajesh
    private void encriptedReportCall(String encodedUrl) {
        final ReportFragmentApi reportFragmentApi = this.apiService.getClient().create(ReportFragmentApi.class);
        reportFragmentApi.getReport(_token, reportFragmentRequest, encodedUrl).enqueue(new Callback<ReportFragmentResponse>() {
            @Override
            public void onResponse(Call<ReportFragmentResponse> call, Response<ReportFragmentResponse> response) {
                if (response.isSuccessful()) {

                    // Log.d("STATUS", "TransactionSuccessfulReport" + response.body());

                    finoTransactionReports = response.body().getFinoTransactionReports();

                    // for( int i=0;i<finoTransactionReports.size();i++) {

                    transaction_spinner.setVisibility(View.VISIBLE);
                    Collections.reverse(finoTransactionReports);
                    mAdapter = new CustomReportAdapter(FundTransferReport.this, finoTransactionReports);
                    reportRecyclerView.setAdapter(mAdapter);

                    mm.stopShimmerAnimation();
                    mm.setVisibility(View.GONE);

                    if (finoTransactionReports.size() <= 0) {
                        noData.setVisibility(View.VISIBLE);
                    } else {
                        noData.setVisibility(View.GONE);
                    }
                } else {
                    transaction_spinner.setVisibility(View.GONE);

                    mm.stopShimmerAnimation();
                    mm.setVisibility(View.GONE);

                    if (finoTransactionReports.size() <= 0) {
                        noData.setVisibility(View.VISIBLE);
                    } else {
                        noData.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ReportFragmentResponse> call, Throwable t) {
                Log.d("STATUS", "TransactionFailedReport" + t);
                transaction_spinner.setVisibility(View.GONE);

                mm.stopShimmerAnimation();
                mm.setVisibility(View.GONE);

                if (finoTransactionReports.size() <= 0) {
                    noData.setVisibility(View.VISIBLE);
                } else {
                    noData.setVisibility(View.GONE);
                }

            }
        });
    }
}
