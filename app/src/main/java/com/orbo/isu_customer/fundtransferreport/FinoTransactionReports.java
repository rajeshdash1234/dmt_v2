package com.orbo.isu_customer.fundtransferreport;

import androidx.annotation.NonNull;

import java.util.Date;

public class FinoTransactionReports implements Comparable<FinoTransactionReports> {

    @Override
    public String toString() {
        return "FinoTransactionReports{" +
                "apiComment='" + apiComment + '\'' +
                ", status='" + status + '\'' +
                ", updatedDate='" + updatedDate + '\'' +
                ", bankName='" + bankName + '\'' +
                ", routeID='" + routeID + '\'' +
                ", beniMobile='" + beniMobile + '\'' +
                ", transactionMode='" + transactionMode + '\'' +
                ", amountTransacted='" + amountTransacted + '\'' +
                ", id='" + id + '\'' +
                ", transactionType='" + transactionType + '\'' +
                ", benificiaryName='" + benificiaryName + '\'' +
                ", balanceAmount='" + balanceAmount + '\'' +
                ", toAccount='" + toAccount + '\'' +
                ", api='" + api + '\'' +
                ", userName='" + userName + '\'' +
                ", previousAmount='" + previousAmount + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", apiTid='" + apiTid + '\'' +
                '}';
    }

//    public FinoTransactionReports(String apiComment, String status, String updatedDate, String bankName, String routeID, String beniMobile, String transactionMode, String amountTransacted, String id, String transactionType, String benificiaryName, String balanceAmount, String toAccount, String api, String userName, String previousAmount, String createdDate, String apiTid) {
//        this.apiComment = apiComment;
//        this.status = status;
//        this.updatedDate = updatedDate;
//        this.bankName = bankName;
//        this.routeID = routeID;
//        this.beniMobile = beniMobile;
//        this.transactionMode = transactionMode;
//        this.amountTransacted = amountTransacted;
//        this.id = id;
//        this.transactionType = transactionType;
//        this.benificiaryName = benificiaryName;
//        this.balanceAmount = balanceAmount;
//        this.toAccount = toAccount;
//        this.api = api;
//        this.userName = userName;
//        this.previousAmount = previousAmount;
//        this.createdDate = createdDate;
//        this.apiTid = apiTid;
//    }

    private String apiComment;

    private String status;

    private String updatedDate;

    private String bankName;

    private String routeID;

    private String beniMobile;

    private String transactionMode;

    private String amountTransacted;

    private String id;

    private String transactionType;

    private String benificiaryName;

    private String balanceAmount;

    private String toAccount;

    private String api;

    private String userName;

    private String previousAmount;

    private String createdDate;

    private String apiTid;

    private Date dateTime;


    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date datetime) {
        this.dateTime = datetime;
    }




    public String getApiComment ()
    {
        return apiComment;
    }

    public void setApiComment (String apiComment)
    {
        this.apiComment = apiComment;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getUpdatedDate ()
    {
        return updatedDate;
    }

    public void setUpdatedDate (String updatedDate)
    {
        this.updatedDate = updatedDate;
    }

    public String getBankName ()
    {
        return bankName;
    }

    public void setBankName (String bankName)
    {
        this.bankName = bankName;
    }

    public String getRouteID ()
    {
        return routeID;
    }

    public void setRouteID (String routeID)
    {
        this.routeID = routeID;
    }

    public String getBeniMobile ()
    {
        return beniMobile;
    }

    public void setBeniMobile (String beniMobile)
    {
        this.beniMobile = beniMobile;
    }

    public String getTransactionMode ()
    {
        return transactionMode;
    }

    public void setTransactionMode (String transactionMode)
    {
        this.transactionMode = transactionMode;
    }

    public String getAmountTransacted ()
    {
        return amountTransacted;
    }

    public void setAmountTransacted (String amountTransacted)
    {
        this.amountTransacted = amountTransacted;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getTransactionType ()
    {
        return transactionType;
    }

    public void setTransactionType (String transactionType)
    {
        this.transactionType = transactionType;
    }

    public String getBenificiaryName ()
    {
        return benificiaryName;
    }

    public void setBenificiaryName (String benificiaryName)
    {
        this.benificiaryName = benificiaryName;
    }

    public String getBalanceAmount ()
    {
        return balanceAmount;
    }

    public void setBalanceAmount (String balanceAmount)
    {
        this.balanceAmount = balanceAmount;
    }

    public String getToAccount ()
    {
        return toAccount;
    }

    public void setToAccount (String toAccount)
    {
        this.toAccount = toAccount;
    }

    public String getApi ()
    {
        return api;
    }

    public void setApi (String api)
    {
        this.api = api;
    }

    public String getUserName ()
    {
        return userName;
    }

    public void setUserName (String userName)
    {
        this.userName = userName;
    }

    public String getPreviousAmount ()
    {
        return previousAmount;
    }

    public void setPreviousAmount (String previousAmount)
    {
        this.previousAmount = previousAmount;
    }

    public String getCreatedDate ()
    {
        return createdDate;
    }

    public void setCreatedDate (String createdDate)
    {
        this.createdDate = createdDate;
    }

    public String getApiTid ()
    {
        return apiTid;
    }

    public void setApiTid (String apiTid)
    {
        this.apiTid = apiTid;
    }


    @Override
    public int compareTo(@NonNull FinoTransactionReports o) {
        if (getDateTime() == null || o.getDateTime() == null)
            return 0;
        return getDateTime().compareTo(o.getDateTime());
    }
}
