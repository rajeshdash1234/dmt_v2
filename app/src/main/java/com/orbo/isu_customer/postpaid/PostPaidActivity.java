package com.orbo.isu_customer.postpaid;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.isu.dmt.R;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

public class PostPaidActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    SearchableSpinner postpaid_select_operator,postpaid_select_operator_circle;

    LinearLayout operator_circlecode_layout;

    String[] operatorPostPaid = { "AIRCEL POSTPAID","BSNL POSTPAID","IDEA POSTPAID","RELIANCE POSTPAID","TATA DOCOMO POSTPAID",
            "VODAFONE POSTPAID","AIRTEL POSTPAID","RELIANCE CDMA POSTPAID","TATA INDICOM LANDLINE","LOOP POSTPAID","BSNL LANDLINE",
            "AIRTEL BROADBAND(DSL)","AIRTEL LANDLINE","IDEA LANDLINE","MTS POSTPAID"};

    String[] operatorCirclePostPaid = { "AndamanNicrobar","AndhraPradesh","ArunachalPradesh","Assam","Bangalore","Bihar","Chennai",
            "Chhattisgarh","DadraandNagarHaveli","DamanandDiu","Delhi","Goa","Gujarat","Haryana","HimachalPradesh","JammuAndKashmir",
            "Jharkhand","Karnataka","Kerala","Kolkata","MadhyaPradesh","Maharashtra","Manipur","Meghalaya","Mizoram","Mumbai","Nagaland",
            "Orissa","Pondicherry","Punjab","Rajasthan","Sikkim","TamilNadu","Tripura","Uttar Pradesh East","Uttar Pradesh West","Uttrakhand",
            "West Bengal"};

    String mobilenoString,amountString,operatorString,rechargetypeString,stdCodeString,pincodeString,accountNoString,latitudeString,circleCodeString,longitudeString;

    EditText postpaid_mobileno,postpaid_amount,postpaid_stdcode,postpaid_accountno;

    Button postpaid_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_paid);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.postpaid));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        operator_circlecode_layout = findViewById(R.id.operator_circlecode_layout);
        postpaid_select_operator = findViewById(R.id.postpaid_select_operator);
        postpaid_select_operator_circle = findViewById(R.id.postpaid_select_operator_circle);

        postpaid_select_operator.setOnItemSelectedListener(this);
        postpaid_select_operator_circle.setOnItemSelectedListener(this);


       /* postpaid_mobileno.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 10){
                    mobilenoString = postpaid_mobileno.getText().toString();
                }else {
                    mobilenoString = null;
                }
            }
        });

        postpaid_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 0){
                    amountString = null;
                }else {
                    amountString = postpaid_amount.getText().toString();
                }
            }
        });

        postpaid_stdcode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                stdCodeString = postpaid_stdcode.getText().toString();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        postpaid_accountno.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                accountNoString = postpaid_accountno.getText().toString();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });*/

        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,operatorPostPaid);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        postpaid_select_operator.setAdapter(aa);

        ArrayAdapter aaa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,operatorCirclePostPaid);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        postpaid_select_operator_circle.setAdapter(aaa);

    }

    @Override
    public void onClick(View view) {
        if (amountString==null|| mobilenoString==null){
            Toast.makeText(getApplicationContext(),"Enter 10 Digit MobileNo OR Enter Amount" , Toast.LENGTH_LONG).show();
        }else {
            rechargePostpaid();
        }

    }

    private void rechargePostpaid() {
        Toast.makeText(this, "pending .....", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        switch (adapterView.getId())
        {
            case R.id.postpaid_select_operator:
                operatorString = operatorPostPaid[i];
                if (operatorString.equals(operatorPostPaid[1])){
                    operator_circlecode_layout.setVisibility(View.VISIBLE);

                }else {
                    operator_circlecode_layout.setVisibility(View.GONE);
                    circleCodeString="";
                    stdCodeString="";
                    accountNoString="";
                }
                // Toast.makeText(getApplicationContext(),operatorString , Toast.LENGTH_LONG).show();

                break;

            case R.id.postpaid_select_operator_circle:
                circleCodeString = operatorCirclePostPaid[i];
                Toast.makeText(getApplicationContext(),circleCodeString , Toast.LENGTH_LONG).show();
                break;
        }

            //        operatorString = operatorPostPaid[i];
            //        circleCodeString = operatorCirclePostPaid[i];
            //        Toast.makeText(getApplicationContext(),operatorString , Toast.LENGTH_LONG).show();
            //        Toast.makeText(getApplicationContext(),circleCodeString , Toast.LENGTH_LONG).show();

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
